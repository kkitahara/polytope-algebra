import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { AnotherPolytopeAlgebra as PolytopeAlgebra } from '../src/index.mjs'
// set a reasonable tolerance for equality
let r = new RealAlgebra(1e-8)
let l = new LinearAlgebra(r)
let dim = 3
let p3d = new PolytopeAlgebra(dim, l)

let tau = r.iadd(r.$(1, 2), r.$(1, 2, 5))
let c1 = l.$(
  1, 0, 0,
  0, 1, 0,
  0, 0, 1).setDim(3, 3)
let c2x = l.$(
  1, 0, 0,
  0, -1, 0,
  0, 0, -1).setDim(3, 3)
let c2y = l.$(
  -1, 0, 0,
  0, 1, 0,
  0, 0, -1).setDim(3, 3)
let c2z = l.$(
  -1, 0, 0,
  0, -1, 0,
  0, 0, 1).setDim(3, 3)
let c3 = l.$(
  0, 0, 1,
  1, 0, 0,
  0, 1, 0).setDim(3, 3)
let c5 = l.ismul(l.$(
  r.sub(tau, 1), r.neg(tau), 1,
  tau, 1, r.sub(tau, 1),
  -1, r.sub(tau, 1), tau), r.$(1, 2)).setDim(3, 3)
let c2list = [c1, c2x, c2y, c2z]
let c3list = [c1, c3, l.mmul(c3, c3)]
let c5list = [c1, c5]
for (let i = 2; i < 5; i += 1) {
  c5list.push(l.mmul(c5list[i - 1], c5))
}

// vectors of length 1 along 2-fold axes (not incl. inversion pair)
let v0 = l.$(1, 0, 0)
let v2list = []
for (let i = 0; i < c3list.length; i += 1) {
  for (let j = 0; j < c5list.length; j += 1) {
    let a = l.mmul(c5list[j], c3list[i])
    v2list.push(l.mmul(a, v0))
  }
}
// vectors of length sqrt(t + 2) along 5-fold axes (incl. inversion)
v0 = l.$(0, 1, tau)
let v5list = []
for (let i = 0; i < c2list.length; i += 1) {
  for (let j = 0; j < c3list.length; j += 1) {
    let a = l.mmul(c2list[i], c3list[j])
    v5list.push(l.mmul(a, v0))
  }
}

// construct triacontahedron
// - huge cube
let tr = p3d.hypercube(10000)
let tau2 = r.mul(tau, tau)
for (let i = 0; i < v2list.length; i += 1) {
  p3d.iaddFacet(tr, p3d.facet(v2list[i], tau2, true))
  p3d.iaddFacet(tr, p3d.facet(v2list[i], r.neg(tau2), false))
}
console.log(p3d.volume(tr).toString())

let b0 = p3d.scale(tr, r.div(1, tau))
console.log(p3d.volume(b0).toString())
let m0 = p3d.copy(b0)
for (let i = v5list.length - 1; i >= 0; i -= 1) {
  m0 = p3d.sub(m0, p3d.translate(b0, l.smul(v5list[i], tau)))
  console.log(i, m0.length)
}
console.log(p3d.volume(m0).toString())

let m5 = p3d.translate(m0, v5list[0])
for (let i = 1; i < v5list.length; i += 1) {
  m5 = p3d.add(m5, p3d.translate(m0, v5list[i]))
  console.log(i, m5.length)
}
console.log(p3d.volume(m5).toString())
