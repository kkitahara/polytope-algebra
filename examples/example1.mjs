import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { PolytopeAlgebra } from '../src/index.mjs'
// set a reasonable tolerance for equality
let r = new RealAlgebra(1e-8)
let l = new LinearAlgebra(r)
let dim = 3
let p3d = new PolytopeAlgebra(dim, l)

let tau = r.iadd(r.$(1, 2), r.$(1, 2, 5))
let c1 = l.$(
  1, 0, 0,
  0, 1, 0,
  0, 0, 1).setDim(3, 3)
let ci = l.$(
  -1, 0, 0,
  0, -1, 0,
  0, 0, -1).setDim(3, 3)
let c2x = l.$(
  1, 0, 0,
  0, -1, 0,
  0, 0, -1).setDim(3, 3)
let c2y = l.$(
  -1, 0, 0,
  0, 1, 0,
  0, 0, -1).setDim(3, 3)
let c2z = l.$(
  -1, 0, 0,
  0, -1, 0,
  0, 0, 1).setDim(3, 3)
let c3 = l.$(
  0, 0, 1,
  1, 0, 0,
  0, 1, 0).setDim(3, 3)
let c5 = l.ismul(l.$(
  r.sub(tau, 1), r.neg(tau), 1,
  tau, 1, r.sub(tau, 1),
  -1, r.sub(tau, 1), tau), r.$(1, 2)).setDim(3, 3)
let c2list = [c1, c2x, c2y, c2z]
let c3list = [c1, c3, l.mmul(c3, c3)]
let c5list = [c1, c5]
for (let i = 2; i < 5; i += 1) {
  c5list.push(l.mmul(c5list[i - 1], c5))
}
let ih = c5list.slice()
for (let i = 1; i < 3; i += 1) {
  for (let j = 0; j < 5; j += 1) {
    ih.push(l.mmul(c3list[i], ih[j]))
  }
}
for (let i = 0; i < 15; i += 1) {
  ih.push(l.mmul(c2x, ih[i]))
}
for (let i = 0; i < 30; i += 1) {
  ih.push(l.mmul(c2y, ih[i]))
}
for (let i = 0; i < 60; i += 1) {
  ih.push(l.mmul(ci, ih[i]))
}

// vectors of length 1 along 2-fold axes (not incl. inversion pair)
let v0 = l.$(1, 0, 0)
let v2list = []
for (let i = 0; i < c3list.length; i += 1) {
  for (let j = 0; j < c5list.length; j += 1) {
    let a = l.mmul(c5list[j], c3list[i])
    v2list.push(l.mmul(a, v0))
  }
}
// vectors of length sqrt(t + 2) along 5-fold axes (incl. inversion)
v0 = l.$(0, 1, tau)
let v5list = []
for (let i = 0; i < c2list.length; i += 1) {
  for (let j = 0; j < c3list.length; j += 1) {
    let a = l.mmul(c2list[i], c3list[j])
    v5list.push(l.mmul(a, v0))
  }
}

// construct triacontahedron
// huge cube
let tr = p3d.hypercube(10000)
let tau2 = r.mul(tau, tau)
for (let i = 0; i < v2list.length; i += 1) {
  p3d.iaddFacet(tr, p3d.facet(v2list[i], tau2, true))
  p3d.iaddFacet(tr, p3d.facet(v2list[i], r.neg(tau2), false))
}
console.log(p3d.volume(tr).toString())

// irreducible part
let ir = p3d.hypercube(10000)
p3d.iaddFacet(ir, p3d.facet(l.$(1, 0, 0), 0, true))
p3d.iaddFacet(ir, p3d.facet(l.$(0, 1, 0), 0, false))
p3d.iaddFacet(ir, p3d.facet(
  l.$(r.neg(tau), 1, r.sub(1, tau)), 0, true))
let trir = p3d.mul(tr, ir)
console.log(p3d.volume(trir).toString(),
  r.div(p3d.volume(tr), p3d.volume(trir)).toString())

let b0 = p3d.scale(tr, r.div(1, tau))
let b0ir = p3d.mul(b0, ir)
console.log(r.mul(p3d.volume(b0ir), 120).toString())
let m0ir = p3d.copy(b0ir)
for (let i = v5list.length - 1; i >= 0; i -= 1) {
  m0ir = p3d.sub(m0ir, p3d.translate(b0, l.smul(v5list[i], tau)))
  console.log(i, m0ir.length)
}
console.log(r.mul(p3d.volume(m0ir), 120).toString())

// empty polytope
let m5ir = p3d.hypercube(0)
for (let j = 0; j < ih.length; j += 1) {
  let p = p3d.rotate(m0ir, ih[j])
  for (let i = 0; i < v5list.length; i += 1) {
    let p2 = p3d.translate(p, v5list[i])
    p2 = p3d.mul(p2, ir)
    m5ir = p3d.add(m5ir, p2)
  }
  console.log(j, m5ir.length)
}
console.log(r.mul(p3d.volume(m5ir), 120).toString())
