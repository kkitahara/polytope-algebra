/**
 * @source: https://www.npmjs.com/package/@kkitahara/polytope-algebra
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

/**
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Matrix } from '@kkitahara/linear-algebra'

/**
 * @desc
 * The Edge class is a class for edges of convex polytopes.
 *
 * @version 1.0.0
 * @since 1.0.0
 */
export class Edge {
  /**
   * @desc
   * The constructor function of the {@link Edge} class.
   *
   * @param {Matrix} v0
   * a {@link Matrix} representing a vertex of the new {@link Edge}.
   *
   * @param {Matrix} v1
   * a {@link Matrix} representing the other vertex of the new {@link Edge}.
   *
   * @throws {Error}
   * if `v0` or `v1` is not an adaptive {@link Matrix}.
   *
   * @throws {Error}
   * if the lengths (dimension) of `v0` and `v1` are not the same.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Matrix as M } from '@kkitahara/linear-algebra'
   * import { Edge } from '@kkitahara/polytope-algebra'
   *
   * const m1 = new M(1, 1, 1)
   * const m2 = new M(2, 1, 1)
   * const e = new Edge(m1, m2)
   *
   * e instanceof Edge // true
   * e.v0 === m1 // true
   * e.v1 === m2 // true
   *
   * @example
   * import { Matrix as M } from '@kkitahara/linear-algebra'
   * import { Edge } from '@kkitahara/polytope-algebra'
   *
   * const m1 = new M(1, 1, 1)
   * const m2 = new M(2, 1, 1)
   *
   * new Edge(new M(1, 1, 1).setDim(1, 3), m2) // Error
   * new Edge(m1, new M(2, 1, 1).setDim(1, 3)) // Error
   *
   * new Edge(new M(1, 1, 1, 1), m2) // Error
   * new Edge(m1, new M(2, 1, 1, 1)) // Error
   *
   * new Edge(new M(), m2) // Error
   * new Edge(m1, new M()) // Error
   */
  constructor (v0, v1) {
    if (!(v0 instanceof Matrix && v0.length > 0 && v0.isAdaptive())) {
      throw Error('`v0` must be an adaptive matrix of at least one element.')
    }
    if (!(v1 instanceof Matrix && v1.length > 0 && v1.isAdaptive())) {
      throw Error('`v1` must be an adaptive matrix of at least one element.')
    }
    const dim = v0.length
    if (dim !== v1.length) {
      throw Error('the lengths of `v0` and `v1` must be the same.')
    }
    /**
     * @desc
     * Edge#v0 stores the reference to the first vertex `v0`.
     *
     * @type {Matrix}
     *
     * @version 1.0.0
     * @since 1.0.0
     */
    this.v0 = v0
    /**
     * @desc
     * Edge#v1 stores the reference to the second vertex `v1`.
     *
     * @type {Matrix}
     *
     * @version 1.0.0
     * @since 1.0.0
     */
    this.v1 = v1
  }

  /**
   * @desc
   * The Edge#dim method returns the dimension of `this`.
   * The dimension of an edge is equal to that of the first vertex `v0`.
   *
   * @type {number}
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Matrix as M } from '@kkitahara/linear-algebra'
   * import { Edge } from '@kkitahara/polytope-algebra'
   *
   * const m1 = new M(1, 1, 1)
   * const m2 = new M(2, 1, 1)
   * const e = new Edge(m1, m2)
   *
   * e.dim // 3
   */
  get dim () {
    return this.v0.length
  }

  /**
   * @desc
   * The Edge#copy method returns a new instance of {@link Edge}
   * which has references to `this.v0` and `this.v1`.
   *
   * @return {Edge}
   * a new instance of {@link Edge}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { Edge } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   *
   * const m1 = l.$(1, 1, 1)
   * const m2 = l.$(2, 1, 1)
   * const e = new Edge(m1, m2)
   * const e2 = e.copy()
   *
   * e2 instanceof Edge // true
   * e2.v0 === m1 // true
   * e2.v1 === m2 // true
   */
  copy () {
    return new Edge(this.v0, this.v1)
  }

  /**
   * @desc
   * The Edge#replaceVertices method
   * replaces vertices according to the given map.
   *
   * @param {WeakMap} vMap
   * a {@link WeakMap} object for replacing.
   *
   * @return {Edge}
   * `this`.
   *
   * @throws {Error}
   * if `vMap` is not a `WeakMap` object.
   *
   * @throws {Error}
   * if `vMap` does not have a vertex of `this` facet as a key.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { Edge } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   *
   * const m1 = l.$(1, 1, 1)
   * const m2 = l.$(2, 1, 1)
   * const m3 = l.$(3, 1, 1)
   * const m4 = l.$(4, 1, 1)
   * const e = new Edge(m1, m2)
   * const vMap = new WeakMap()
   * vMap.set(m1, m3)
   * vMap.set(m2, m4)
   * e.replaceVertices(vMap)
   * e.v0 === m3 // true
   * e.v1 === m4 // true
   *
   * e.replaceVertices(vMap) // Error
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { Edge } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   *
   * const m1 = l.$(1, 1, 1)
   * const m2 = l.$(2, 1, 1)
   * const m3 = l.$(3, 1, 1)
   * const e = new Edge(m1, m2)
   * const vMap = new WeakMap()
   * e.replaceVertices(vMap) // Error
   * vMap.set(m1, m3)
   * e.replaceVertices(vMap) // Error
   * e.replaceVertices(null) // Error
   */
  replaceVertices (vMap) {
    if (!(vMap instanceof WeakMap)) {
      throw Error('`vMap` must be an instance of `WeakMap`.')
    }
    let v = vMap.get(this.v0)
    if (v === undefined) {
      throw Error('`vMap` does not have a vertex of `this` as a key.')
    }
    this.v0 = v
    v = vMap.get(this.v1)
    if (v === undefined) {
      throw Error('`vMap` does not have a vertex of `this` as a key.')
    }
    this.v1 = v
    return this
  }

  /**
   * @desc
   * The Edge#isIdentical method checks if
   * `this` is identical to `another`.
   * Two edges are identical if the two associated
   * vectors are the same objects irrespective of the order.
   *
   * @param {Edge} another
   * another {@link Edge}.
   *
   * @return {boolean}
   * `true` if `this` is identical to `another`.
   *
   * @throws {Error}
   * if `another` is not an {@link Edge}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Matrix as M } from '@kkitahara/linear-algebra'
   * import { Edge } from '@kkitahara/polytope-algebra'
   *
   * const m1 = new M(1, 1, 1)
   * const m2 = new M(2, 1, 1)
   * const e1 = new Edge(m1, m2)
   * const e2 = new Edge(m1, m2)
   * const e3 = new Edge(m2, m1)
   * const e4 = new Edge(m1, new M(2, 1, 1))
   * const e5 = new Edge(m2, new M(1, 1, 1))
   *
   * e1.isIdentical(e1) // true
   * e1.isIdentical(e2) // true
   * e1.isIdentical(e3) // true
   * e1.isIdentical(e4) // false
   * e1.isIdentical(e5) // false
   * e1.isIdentical(null) // Error
   */
  isIdentical (another) {
    if (!(another instanceof Edge)) {
      throw Error('`another` must be an `Edge`.')
    }
    return (this.v0 === another.v0 && this.v1 === another.v1) ||
        (this.v0 === another.v1 && this.v1 === another.v0)
  }
}

/* @license-end */
