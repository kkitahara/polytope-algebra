/**
 * @source: https://www.npmjs.com/package/@kkitahara/polytope-algebra
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

/**
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Matrix, LinearAlgebra } from '@kkitahara/linear-algebra'

/**
 * @typedef {Object|number}
 * RealAlgebraicElement
 *
 * @desc
 * A RealAlgebraicElement denotes a number supported by @kkitahara/real-algebra.
 */

/**
 * @desc
 * `OUTSIDE_OF_FACET = 1` is returned by
 * {@link Facet#position} method if a vector is outside of the facet.
 *
 * @type {number}
 *
 * @version 1.0.0
 * @since 1.0.0
 */
export const OUTSIDE_OF_FACET = 1
/**
 * @desc
 * `ON_FACET = 0` is returned by
 * {@link Facet#position} method if a vector is on the facet.
 *
 * @type {number}
 *
 * @version 1.0.0
 * @since 1.0.0
 */
export const ON_FACET = 0
/**
 * @desc
 * `INSIDE_OF_FACET = -1` is returned by
 * {@link Facet#position} method if a vector is inside of the facet.
 *
 * @type {number}
 *
 * @version 1.0.0
 * @since 1.0.0
 */
export const INSIDE_OF_FACET = -1

/**
 * @desc
 * The Facet class
 * is a class for facets of comvex polytopes.
 *
 * @version 1.0.0
 * @since 1.0.0
 */
export class Facet {
  /**
   * @desc
   * The constructor function of the {@link Facet} class.
   *
   * CAUTION: this constructor function does not check if `d`
   * is a valid {@link RealAlgebraicElement}.
   *
   * @param {Matrix} nvec
   * a {@link Matrix} representing the normal vector of the new {@link Facet}.
   *
   * @param {RealAlgebraicElement} d
   * a {@link RealAlgebraicElement} which is equal to
   * the dot product of `nvec` and a vertex on the new {@link Facet}.
   * If `nvec.length === 0`, `d` is set at `0`.
   *
   * @param {boolean} faceOutside
   * `nvec` is considered to face toward outside/inside
   * of the new {@link Facet} if `faceOutside` is `true`/`false`.
   *
   * @throws {Error}
   * if `nvec` is not an adaptive {@link Matrix}.
   *
   * @version 1.1.4
   * @since 1.0.0
   *
   * @example
   * import { Matrix as M } from '@kkitahara/linear-algebra'
   * import { Facet } from '@kkitahara/polytope-algebra'
   *
   * const m = new M(1, 2, 3)
   * const d = 2
   * const f = new Facet(m, d, true)
   *
   * f instanceof Facet // true
   * f.nvec === m // true
   * f.faceOutside // true
   * f.d === d // true
   * f.vertices instanceof Array // true
   * f.vertices.length === 0 // true
   *
   * const f2 = new Facet(m, d, false)
   *
   * f2 instanceof Facet // true
   * f2.faceOutside // false
   *
   * @example
   * import { Matrix as M } from '@kkitahara/linear-algebra'
   * import { Facet } from '@kkitahara/polytope-algebra'
   *
   * const m = new M(1, 2, 3, 4).setDim(1, 4)
   * const d = 2
   * // `m` is not a vector
   * new Facet(m, d, true) // Error
   *
   * @example
   * import { Matrix as M } from '@kkitahara/linear-algebra'
   * import { Facet } from '@kkitahara/polytope-algebra'
   *
   * // no facet in 0-dim
   * new Facet(new M(), 1, true) // Error
   */
  constructor (nvec, d, faceOutside) {
    if (!(nvec instanceof Matrix && nvec.length > 0 && nvec.isAdaptive())) {
      throw Error('`nvec` must be an adaptive matrix of at least one element.')
    }
    /**
     * @desc
     * Facet#nvec represents the normal vector of `this`.
     * No copy is generated on construction, i.e. the reference is stored.
     *
     * @type {Matrix}
     *
     * @version 1.0.0
     * @since 1.0.0
     */
    this.nvec = nvec
    /**
     * @desc
     * Facet#d is equal to the dot product of `nvec` and a vertex on this facet.
     * No copy is generated on construction, i.e. the reference is stored.
     *
     * @type {RealAlgebraicElement}
     *
     * @version 1.0.0
     * @since 1.0.0
     */
    this.d = d
    /**
     * @desc
     * Facet#nvec is considered to face toward outside/inside
     * of this facet if `faceOutside` is `true`/`false`.
     *
     * @type {boolean}
     *
     * @version 1.0.0
     * @since 1.0.0
     */
    this.faceOutside = faceOutside
    /**
     * @desc
     * Facet#vertices sotres the references to the vertices on `this` facet.
     *
     * @type {Array}
     *
     * @version 1.0.0
     * @since 1.0.0
     */
    this.vertices = []
  }

  /**
   * @desc
   * The Facet#dim method returns the dimension of `this`.
   * The dimension of a facet is equal to that of the normal vector.
   *
   * @type {number}
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Matrix as M } from '@kkitahara/linear-algebra'
   * import { Facet } from '@kkitahara/polytope-algebra'
   *
   * const m = new M(1, 2, 3, 4)
   * const d = 2
   * const f = new Facet(m, d, true)
   *
   * f.dim // 4
   */
  get dim () {
    return this.nvec.length
  }

  /**
   * @desc
   * The Facet#copy method returns a new instance of {@link Facet}
   * which has copies of `this.d` and `this.faceOutside`,
   * a shallow copy of `this.vertices` and a reference to `this.nvec`.
   *
   * @param {LinearAlgebra} lalg
   * an instance of {@link LinearAlgebra}.
   *
   * @return {Facet}
   * a new instance of {@link Facet}.
   *
   * @throws {Error}
   * if `lalg` is not an instance of `LinearAlgebra`.
   *
   * @version 1.1.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { Facet } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   *
   * const m = l.$(1, 2, 3)
   * const d = 2
   * const f = new Facet(m, d, true)
   * // ANTI-PATTERN
   * // this is just for a test, vertices must be on the facet
   * const v = l.$(0, 0, 0)
   * f.vertices.push(v)
   * const f2 = f.copy(l)
   *
   * f2 instanceof Facet // true
   * f2.nvec === m // true
   * f2.faceOutside // true
   * f2.d === d // false
   * r.eq(f2.d, d) // true
   * f2.vertices instanceof Array // true
   * f2.vertices.length === 1 // true
   * f2.vertices[0] === v // true
   *
   * // invalid parameter
   * f.copy(null) // Error
   */
  copy (lalg) {
    if (!(lalg instanceof LinearAlgebra)) {
      throw Error('`lalg` must be an instance of `LinearAlgebra`.')
    }
    const salg = lalg.salg
    const f = new Facet(this.nvec, salg.copy(this.d), this.faceOutside)
    f.vertices.push(...this.vertices)
    return f
  }

  /**
   * @desc
   * The Facet#replaceVertices method
   * replaces vertices according to the given map.
   *
   * @param {WeakMap} vMap
   * a {@link WeakMap} object for replacing.
   *
   * @return {Facet}
   * `this`.
   *
   * @throws {Error}
   * if `vMap` is not a `WeakMap` object.
   *
   * @throws {Error}
   * if `vMap` does not have a vertex of `this` facet as a key.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { Facet } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   *
   * const m = l.$(1, 2, 3)
   * const d = 2
   * const f = new Facet(m, d, true)
   * // ANTI-PATTERN
   * // this is just for a test, vertices must be on the facet
   * const v = l.$(0, 0, 0)
   * const v2 = l.$(1, 0, 0)
   * f.vertices.push(v)
   * const vMap = new WeakMap()
   *
   * vMap.set(v, v2)
   * f.vertices[0] === v // true
   * f.replaceVertices(vMap)
   * f.vertices[0] === v2 // true
   *
   * f.replaceVertices(vMap) // Error
   * f.replaceVertices(null) // Error
   */
  replaceVertices (vMap) {
    if (!(vMap instanceof WeakMap)) {
      throw Error('`vMap` must be an instance of `WeakMap`.')
    }
    for (let i = this.vertices.length - 1; i >= 0; i -= 1) {
      const v = vMap.get(this.vertices[i])
      if (v === undefined) {
        throw Error('`vMap` does not have a vertex of `this` as a key.')
      }
      this.vertices[i] = v
    }
    return this
  }

  /**
   * @desc
   * The Facet#position method compares the position `v` to `this.d`.
   *
   * @param {Matrix} v
   * a {@link Matrix} representing a vector.
   *
   * @param {LinearAlgebra} lalg
   * an instance of {@link LinearAlgebra}.
   *
   * @return {number}
   * `-1` if `v` is inside of of `this`.
   *
   * `0` if `v` is on `this`.
   *
   * `1` if `v` is outside of `this`.
   *
   * @throws {Error}
   * if `lalg` is not an instance of `LinearAlgebra`.
   *
   * @throws {Error}
   * if `lalg` is not an instance of real algebra.
   *
   * @version 1.1.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * import { Facet, OUTSIDE_OF_FACET, ON_FACET, INSIDE_OF_FACET }
   *   from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const r2 = new RealAlgebra(0.1)
   * const l = new LinearAlgebra(r)
   * const l2 = new LinearAlgebra(r2)
   *
   * const m = new M(1, 1, 1)
   * const d = 3
   * const f = new Facet(m, d, true)
   * const v1 = new M(0.99, 1, 1)
   * const v2 = new M(1, 1, 1)
   * const v3 = new M(1, 1, 1.01)
   *
   * f.position(v1, l) // INSIDE_OF_FACET
   * f.position(v2, l) // ON_FACET
   * f.position(v3, l) // OUTSIDE_OF_FACET
   *
   * f.position(v1, l2) // ON_FACET
   * f.position(v2, l2) // ON_FACET
   * f.position(v3, l2) // ON_FACET
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * import { Facet, OUTSIDE_OF_FACET, ON_FACET, INSIDE_OF_FACET }
   *   from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const r2 = new RealAlgebra(0.1)
   * const l = new LinearAlgebra(r)
   * const l2 = new LinearAlgebra(r2)
   *
   * const m = new M(1, 1, 1)
   * const d = 3
   * const f = new Facet(m, d, false)
   * const v1 = new M(0.99, 1, 1)
   * const v2 = new M(1, 1, 1)
   * const v3 = new M(1, 1, 1.01)
   *
   * f.position(v1, l) // OUTSIDE_OF_FACET
   * f.position(v2, l) // ON_FACET
   * f.position(v3, l) // INSIDE_OF_FACET
   *
   * f.position(v1, l2) // ON_FACET
   * f.position(v2, l2) // ON_FACET
   * f.position(v3, l2) // ON_FACET
   *
   * l2.isReal = () => false
   * f.position(v1, l2) // Error
   * f.position(v1, null) // Error
   */
  position (v, lalg) {
    if (!(lalg instanceof LinearAlgebra)) {
      throw Error('`lalg` must be an instance of `LinearAlgebra`.')
    }
    if (!lalg.isReal()) {
      throw Error('`lalg` must be an implementation of real algebra.')
    }
    const ralg = lalg.salg
    const dd = ralg.isub(lalg.dot(this.nvec, v), this.d)
    if (ralg.isZero(dd)) {
      return ON_FACET
    } else if (ralg.isPositive(dd) === this.faceOutside) {
      return OUTSIDE_OF_FACET
    } else {
      return INSIDE_OF_FACET
    }
  }

  /**
   * @desc
   * The Facet#irotate method
   * rotates `this` by a rotation matrix `m` *in place*.
   * A rotation matrix is a orthogonal matrix,
   * i.e. its inverse is equal to its transpose.
   *
   * CAUTION: this method does not check if `m` is an orthogonal matrix.
   *
   * CAUTION: this method does not change the vertices on `this`.
   * Use {@link Facet#replaceVertices} to replace the vertices with
   * the rotated vertices.
   *
   * @param {Matrix} m
   * an instance of {@link Matrix} representing an orthogonal matrix.
   *
   * @param {LinearAlgebra} lalg
   * an instance of {@link LinearAlgebra}.
   *
   * @return {Facet}
   * `this`.
   *
   * @throws {Error}
   * if `lalg` is not an instance of {@link LinearAlgebra}.
   *
   * @throws {Error}
   * if `m` is not a square matrix of the same dimension as `this`.
   *
   * @version 1.1.0
   * @since 1.1.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { Facet } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   *
   * const m = l.$(1, 1)
   * const d = 1
   * const f = new Facet(m, d, true)
   * const f2 = f.irotate(l.$(0, 1, -1, 0).setDim(2), l)
   * f2 instanceof Facet // true
   * f2 === f // true
   * f2.nvec === m // false
   * l.eq(f2.nvec, l.$(1, -1)) // true
   * f2.d // 1
   *
   * f.irotate(l.$(0, 1, -1, 0), l) // Error
   * f.irotate(l.$(0, 1, -1, 0).setDim(2, 2), null) // Error
   * f.irotate(null, l) // Error
   * f.irotate(l.$(0, 1, -1, 0).setDim(1, 4), l) // Error
   * f.irotate(l.$(1).setDim(1), l) // Error
   */
  irotate (m, lalg) {
    if (!(lalg instanceof LinearAlgebra)) {
      throw Error('`lalg` must be an instance of `LinearAlgebra`.')
    }
    m = lalg.cast(m)
    if (!m.isSquare() || m.getDim()[0] !== this.dim) {
      throw Error('`m` must be a square matrix of the same dimension as ' +
          '`this`.')
    }
    this.nvec = lalg.mmul(m, this.nvec)
    return this
  }

  /**
   * @desc
   * The Facet#itranslate method translates `this` by `v` *in place*.
   *
   * @param {Matrix} v
   * an instance of {@link Matrix} representing a vector.
   *
   * @param {LinearAlgebra} lalg
   * an instance of {@link LinearAlgebra}.
   *
   * @return {Facet}
   * `this`.
   *
   * @throws {Error}
   * if `lalg` is not an instance of {@link LinearAlgebra}.
   *
   * @version 1.1.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { Facet } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   *
   * const m = l.$(1, 1)
   * const d = 1
   * const f = new Facet(m, d, true)
   * const f2 = f.itranslate([1, 1], l)
   * f2 instanceof Facet // true
   * f2 === f // true
   * f2.nvec === m // true
   * f2.d // 3
   *
   * f.itranslate([1, 1], null) // Error
   * f.itranslate(null, l) // Error
   */
  itranslate (v, lalg) {
    if (!(lalg instanceof LinearAlgebra)) {
      throw Error('`lalg` must be an instance of `LinearAlgebra`.')
    }
    const salg = lalg.salg
    this.d = salg.iadd(this.d, lalg.dot(this.nvec, v))
    return this
  }

  /**
   * @desc
   * The Facet#iscale method scales `this` by `s` *in place* around
   * the origin.
   *
   * @param {RealAlgebraicElement} s
   * a {@link RealAlgebraicElement} representing a scale factor.
   *
   * @param {LinearAlgebra} lalg
   * an instance of {@link LinearAlgebra}.
   *
   * @return {Facet}
   * `this`.
   *
   * @throws {Error}
   * if `lalg` is not an instance of {@link LinearAlgebra}.
   *
   * @version 1.1.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { Facet } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   *
   * const m = l.$(1, 1)
   * const d = 1
   * const f = new Facet(m, d, true)
   * const f2 = f.iscale(2, l)
   * f2 instanceof Facet // true
   * f2 === f // true
   * f2.nvec === m // true
   * f2.d // 2
   *
   * f.iscale(2, null) // Error
   */
  iscale (s, lalg) {
    if (!(lalg instanceof LinearAlgebra)) {
      throw Error('`lalg` must be an instance of `LinearAlgebra`.')
    }
    const salg = lalg.salg
    this.d = salg.imul(this.d, s)
    return this
  }
}

/* @license-end */
