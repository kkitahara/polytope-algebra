/**
 * @source: https://www.npmjs.com/package/@kkitahara/polytope-algebra
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

/**
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Polytope } from './polytope.mjs'
import { PolytopeAlgebra } from './polytope-algebra.mjs'

/**
 * @desc
 * The AnotherPolytopeAlgebra class is a class for
 * another set algebra of polytopes.
 * In this algebra, intersection operation is used internally
 * for union and difference operation.
 * A weight is associated with each convex polytope.
 *
 * @version 1.1.1
 * @since 1.0.0
 *
 * @example
 * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
 * // import { RealAlgebra } from '@kkitahara/real-algebra'
 * import { LinearAlgebra } from '@kkitahara/linear-algebra'
 * import { AnotherPolytopeAlgebra } from '@kkitahara/polytope-algebra'
 * const r = new RealAlgebra(1e-8)
 * const l = new LinearAlgebra(r)
 * const dim = 2
 * const p2d = new AnotherPolytopeAlgebra(dim, l)
 *
 * // Generate a new hypercube of edge length `2 * d` centred at the origin.
 * let d = 1
 * let p1 = p2d.hypercube(d)
 *
 * // p1
 * // +-----------+
 * // |\\\\\\\\\\\|
 * // |\\\\\\\\\\\|
 * // |\\\\\o\\\\\|
 * // |\\\\\\\\\\\|
 * // |\\\\\\\\\\\|
 * // +-----------+
 *
 * // Calculate the volume of polytopes
 * const vol = p2d.volume(p1)
 * vol.toString() // '4'
 *
 * // Generate a new facet.
 * // A facet is characterised by its normal vector (nvec),
 * // distance from the origin (d),
 * // and the direction of the normal vector (faceOutside).
 * let nvec = [1, 0]
 * d = 1
 * let faceOutside = true
 * let f = p2d.facet(nvec, d, faceOutside)
 *
 * // \\\\\\\\\\\\|
 * // \\\\\\\\\\\\|
 * // \\\\\\\\\\\\|
 * // \\\\\\o\\\\\| f
 * // \\\\\\\\\\\\|
 * // \\\\\\\\\\\\|
 * // \\\\\\\\\\\\|
 *
 * nvec = [1, 0]
 * d = 1
 * faceOutside = false
 * f = p2d.facet(nvec, d, faceOutside)
 *
 * //             |\\\\\\\\\\\\
 * //             |\\\\\\\\\\\\
 * //             |\\\\\\\\\\\\
 * //       o   f |\\\\\\\\\\\\
 * //             |\\\\\\\\\\\\
 * //             |\\\\\\\\\\\\
 * //             |\\\\\\\\\\\\
 *
 * // No need to normalise the normal vector,
 * // provided that `d` is the dot product of the normal vector and
 * // the position vector of a vertex on the facet.
 * f = p2d.facet([2, 0], 1, true)
 *
 * // \\\\\\\\\|
 * // \\\\\\\\\|
 * // \\\\\\\\\|
 * // \\\\\\o\\| f
 * // \\\\\\\\\|
 * // \\\\\\\\\|
 * // \\\\\\\\\|
 *
 * f = p2d.facet([2, 0], 2, true)
 *
 * // \\\\\\\\\\\\|
 * // \\\\\\\\\\\\|
 * // \\\\\\\\\\\\|
 * // \\\\\\o\\\\\| f
 * // \\\\\\\\\\\\|
 * // \\\\\\\\\\\\|
 * // \\\\\\\\\\\\|
 *
 * // Add a facet to polytopes (in place)
 * p1 = p2d.hypercube(d)
 *
 * // p1
 * // +-----------+
 * // |\\\\\\\\\\\|
 * // |\\\\\\\\\\\|
 * // |\\\\\o\\\\\|
 * // |\\\\\\\\\\\|
 * // |\\\\\\\\\\\|
 * // +-----------+
 *
 * f = p2d.facet([2, -1], 1, true)
 *
 * // \\\\\\\\\\\\/
 * // \\\\\\\\\\\/
 * // \\\\\\\\\\/
 * // \\\\\\o\\/ f
 * // \\\\\\\\/
 * // \\\\\\\/
 * // \\\\\\/
 *
 * p1 = p2d.iaddFacet(p1, f)
 * p2d.volume(p1).toString() // '3'
 *
 * // p1
 * // +-----------+
 * // |\\\\\\\\\\/
 * // |\\\\\\\\\/
 * // |\\\\\o\\/
 * // |\\\\\\\/
 * // |\\\\\\/
 * // +-----+
 *
 * // Copy (generate a new object)
 * let p2 = p2d.copy(p1)
 * p2d.volume(p2).toString() // '3'
 *
 * // p2
 * // +-----------+
 * // |\\\\\\\\\\/
 * // |\\\\\\\\\/
 * // |\\\\\o\\/
 * // |\\\\\\\/
 * // |\\\\\\/
 * // +-----+
 *
 * // Rotation (new object is genrated, since v1.1.0)
 * const c4 = l.$(0, -1, 1, 0).setDim(2)
 * p2 = p2d.rotate(p1, c4)
 * p1 !== p2 // true
 * p2d.volume(p2).toString() // '3'
 *
 * // p2
 * // +-__
 * // |\\\--_
 * // |\\\\\\--__
 * // |\\\\\o\\\\-+
 * // |\\\\\\\\\\\|
 * // |\\\\\\\\\\\|
 * // +-----------+
 *
 * // In-place rotation (new object is not genrated, since v1.1.0)
 * p2 = p2d.irotate(p2, c4)
 * p2d.volume(p2).toString() // '3'
 *
 * // p2
 * //       +-----+
 * //      /\\\\\\|
 * //     /\\\\\\\|
 * //    /\\o\\\\\|
 * //   /\\\\\\\\\|
 * //  /\\\\\\\\\\|
 * // +-----------+
 *
 * // Translation (new object is generated)
 * p2 = p2d.translate(p1, [r.$(1, 2), 0])
 * p1 !== p2 // true
 * p2d.volume(p2).toString() // '3'
 *
 * // p2
 * //    +-----------+
 * //    |\\\\\\\\\\/
 * //    |\\\\\\\\\/
 * //    |\\o\\\\\/
 * //    |\\\\\\\/
 * //    |\\\\\\/
 * //    +-----+
 *
 * // In-place translation (new object is not generated)
 * p1 = p2d.itranslate(p1, [r.$(1, 2), 0])
 * p2d.volume(p1).toString() // '3'
 *
 * // p1
 * //    +-----------+
 * //    |\\\\\\\\\\/
 * //    |\\\\\\\\\/
 * //    |\\o\\\\\/
 * //    |\\\\\\\/
 * //    |\\\\\\/
 * //    +-----+
 *
 * // Scaling (new object is generated)
 * p2 = p2d.scale(p1, 2)
 * p1 !== p2 // true
 * p2d.volume(p2).toString() // '12'
 *
 * // p2
 * // +-----------------------+
 * // |\\\\\\\\\\\\\\\\\\\\\\/
 * // |\\\\\\\\\\\\\\\\\\\\\/
 * // |\\\\\\\\\\\\\\\\\\\\/
 * // |\\\\\\\\\\\\\\\\\\\/
 * // |\\\\\\\\\\\\\\\\\\/
 * // |\\\\\o\\\\\\\\\\\/
 * // |\\\\\\\\\\\\\\\\/
 * // |\\\\\\\\\\\\\\\/
 * // |\\\\\\\\\\\\\\/
 * // |\\\\\\\\\\\\\/
 * // |\\\\\\\\\\\\/
 * // +-----------+
 *
 * // In-place scaling (new object is not generated)
 * p1 = p2d.iscale(p1, r.$(1, 3))
 * p2d.volume(p1).toString() // '1 / 3'
 *
 * //      p1
 * //      +---+
 * //      |o\/
 * //      +-+
 *
 * // Multiplication (intersection, new object is generated)
 * p1 = p2d.hypercube(1)
 * p2 = p2d.itranslate(p2d.hypercube(1), [1, 1])
 * let p3 = p2d.mul(p1, p2)
 * p3 !== p1 // true
 * p3 !== p2 // true
 * p2d.volume(p3).toString() // '1'
 *
 * //
 * //
 * //
 * // p1
 * // +-----------+
 * // |\\\\\\\\\\\|
 * // |\\\\\\\\\\\|
 * // |\\\\\o\\\\\|
 * // |\\\\\\\\\\\|
 * // |\\\\\\\\\\\|
 * // +-----------+
 *
 * //       p2
 * //       +-----------+
 * //       |\\\\\\\\\\\|
 * //       |\\\\\\\\\\\|
 * //       |\\\\\\\\\\\|
 * //       |\\\\\\\\\\\|
 * //       |\\\\\\\\\\\|
 * //       o-----------+
 * //
 * //
 * //
 *
 * //
 * //
 * //
 * //       p3
 * //       +-----+
 * //       |\\\\\|
 * //       |\\\\\|
 * //       o-----+
 * //
 * //
 * //
 *
 * // Subtraction (set difference, new object is generated)
 * p1 = p2d.hypercube(1)
 * p2 = p2d.itranslate(p2d.hypercube(1), [1, 1])
 * p3 = p2d.sub(p1, p2)
 * p3 !== p1 // true
 * p3 !== p2 // true
 * p2d.volume(p3).toString() // '3'
 *
 * //
 * //
 * //
 * // p1
 * // +-----------+
 * // |\\\\\\\\\\\|
 * // |\\\\\\\\\\\|
 * // |\\\\\o\\\\\|
 * // |\\\\\\\\\\\|
 * // |\\\\\\\\\\\|
 * // +-----------+
 *
 * //       p2
 * //       +-----------+
 * //       |\\\\\\\\\\\|
 * //       |\\\\\\\\\\\|
 * //       |\\\\\\\\\\\|
 * //       |\\\\\\\\\\\|
 * //       |\\\\\\\\\\\|
 * //       o-----------+
 * //
 * //
 * //
 *
 * //
 * //
 * //
 * // p3
 * // +-----+
 * // |\\\\\|
 * // |\\\\\|
 * // |\\\\\o-----+
 * // |\\\\\\\\\\\|
 * // |\\\\\\\\\\\|
 * // +-----------+
 *
 * // Addition (union, new object is generated)
 * p1 = p2d.hypercube(1)
 * p2 = p2d.itranslate(p2d.hypercube(1), [1, 1])
 * p3 = p2d.add(p1, p2)
 * p3 !== p1 // true
 * p3 !== p2 // true
 * p2d.volume(p3).toString() // '7'
 *
 * //
 * //
 * //
 * // p1
 * // +-----------+
 * // |\\\\\\\\\\\|
 * // |\\\\\\\\\\\|
 * // |\\\\\o\\\\\|
 * // |\\\\\\\\\\\|
 * // |\\\\\\\\\\\|
 * // +-----------+
 *
 * //       p2
 * //       +-----------+
 * //       |\\\\\\\\\\\|
 * //       |\\\\\\\\\\\|
 * //       |\\\\\\\\\\\|
 * //       |\\\\\\\\\\\|
 * //       |\\\\\\\\\\\|
 * //       o-----------+
 * //
 * //
 * //
 *
 * //
 * //       +-----------+
 * //       |\\\\\\\\\\\|
 * // p3    |\\\\\\\\\\\|
 * // +-----+\\\\\\\\\\\|
 * // |\\\\\\\\\\\\\\\\\|
 * // |\\\\\\\\\\\\\\\\\|
 * // |\\\\\o\\\\\+-----+
 * // |\\\\\\\\\\\|
 * // |\\\\\\\\\\\|
 * // +-----------+
 *
 * // JSON (stringify and parse)
 * const str = JSON.stringify(p3)
 * const p4 = JSON.parse(str, p2d.reviver)
 * const p5 = p2d.mul(p3, p4)
 * p2d.volume(p4).toString() // '7'
 * p2d.volume(p5).toString() // '7'
 */
export class AnotherPolytopeAlgebra extends PolytopeAlgebra {
  /**
   * @desc
   * The AnotherPolytopeAlgebra#add method
   * calculates the union of two instances of {@link Polytope}
   * `p1` and `p2`.
   *
   * @param {Polytope} p1
   * an instance of {@link Polytope}.
   *
   * @param {Polytope} p2
   * another instance of {@link Polytope}.
   *
   * @return {Polytope}
   * a new instance of {@link Polytope}.
   *
   * @throws {Error}
   * if `p` is not an instance of {@link Polytope}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { AnotherPolytopeAlgebra, Polytope }
   *   from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   * const p2d = new AnotherPolytopeAlgebra(2, l)
   * const p1d = new AnotherPolytopeAlgebra(1, l)
   *
   * const p = p2d.hypercube(1)
   * const p2 = p2d.translate(p, [1, 1])
   * const p3 = p2d.add(p, p2)
   * p3 instanceof Polytope // true
   * p3.length // 3
   * p3 !== p // true
   * p3 !== p2 // true
   *
   * r.eq(p2d.volume(p), 4) // true
   *
   * r.eq(p2d.volume(p2), 4) // true
   *
   * r.eq(p2d.volume(p3), 7) // true
   *
   * p.nullify()
   * const p4 = p2d.add(p3, p)
   * p4 instanceof Polytope // true
   * p4.length // 3
   *
   * p.nullify()
   * const p5 = p2d.add(p, p3)
   * p5 instanceof Polytope // true
   * p5.length // 3
   *
   * p2d.add(p2, p1d.hypercube(1)) // Error
   * p2d.add(p1d.hypercube(1), p2) // Error
   * p2d.add(p2, null) // Error
   * p2d.add(null, p2) // Error
   */
  add (p1, p2) {
    if (!(p1 instanceof Polytope)) {
      throw Error('`p` must be an instance of `Polytope`.')
    }
    if (!(p2 instanceof Polytope)) {
      throw Error('`p` must be an instance of `Polytope`.')
    }
    if (p2.isNull()) {
      return this.copy(p1)
    } else if (p1.isNull()) {
      return this.copy(p2)
    }
    const dim = this.dim
    if (p1.dim !== dim) {
      throw Error('the dimension of `p` must be the same as `this`.')
    }
    if (p2.dim !== dim) {
      throw Error('the dimension of `p` must be the same as `this`.')
    }
    const p3 = this.copy(p1)
    const p4 = this.sub(p2, p1)
    p3.push(...p4)
    p3.pairAnnihilation(this.lalg)
    return p3
  }

  /**
   * @desc
   * The AnotherPolytopeAlgebra#sub method
   * calculates the set difference of two instances of {@link Polytope}
   * `p1` \ `p2`.
   *
   * @param {Polytope} p1
   * an instance of {@link Polytope}.
   *
   * @param {Polytope} p2
   * another instance of {@link Polytope}.
   *
   * @return {Polytope}
   * a new instance of {@link Polytope}.
   *
   * @throws {Error}
   * if `p` is not an instance of {@link Polytope}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { AnotherPolytopeAlgebra, Polytope }
   *   from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   * const p2d = new AnotherPolytopeAlgebra(2, l)
   * const p1d = new AnotherPolytopeAlgebra(1, l)
   *
   * const p = p2d.hypercube(1)
   * const p2 = p2d.translate(p, [1, 1])
   * const p3 = p2d.sub(p, p2)
   * p3 instanceof Polytope // true
   * p3.length // 2
   * p3 !== p // true
   * p3 !== p2 // true
   *
   * r.eq(p2d.volume(p), 4) // true
   *
   * r.eq(p2d.volume(p2), 4) // true
   *
   * r.eq(p2d.volume(p3), 3) // true
   *
   * p.nullify()
   * const p4 = p2d.sub(p3, p)
   * p4 instanceof Polytope // true
   * p4.length // 2
   *
   * p2d.sub(p2, p1d.hypercube(1)) // Error
   * p2d.sub(p1d.hypercube(1), p2) // Error
   * p2d.sub(p2, null) // Error
   * p2d.sub(null, p2) // Error
   */
  sub (p1, p2) {
    if (!(p1 instanceof Polytope)) {
      throw Error('`p` must be an instance of `Polytope`.')
    }
    if (!(p2 instanceof Polytope)) {
      throw Error('`p` must be an instance of `Polytope`.')
    }
    if (p1.isNull() || p2.isNull()) {
      return this.copy(p1)
    }
    const dim = this.dim
    if (p1.dim !== dim) {
      throw Error('the dimension of `p` must be the same as `this`.')
    }
    if (p2.dim !== dim) {
      throw Error('the dimension of `p` must be the same as `this`.')
    }
    const p3 = this.copy(p1)
    const p4 = this.mul(p1, p2)
    for (let i = p4.length - 1; i >= 0; i -= 1) {
      p4[i].weight = !p4[i].weight
    }
    p3.push(...p4)
    p3.pairAnnihilation(this.lalg)
    return p3
  }

  /**
   * @desc
   * The AnotherPolytopeAlgebra#mul method
   * calculates the intersection of two instances of {@link Polytope}
   * `p1` and `p2`.
   *
   * @param {Polytope} p1
   * an instance of {@link Polytope}.
   *
   * @param {Polytope} p2
   * another instance of {@link Polytope}.
   *
   * @return {Polytope}
   * a new instance of {@link Polytope}.
   *
   * @throws {Error}
   * if `p` is not an instance of {@link Polytope}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { AnotherPolytopeAlgebra, Polytope }
   *   from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   * const p2d = new AnotherPolytopeAlgebra(2, l)
   * const p1d = new AnotherPolytopeAlgebra(1, l)
   *
   * const p = p2d.hypercube(1)
   * const p2 = p2d.translate(p, [1, 1])
   * const p3 = p2d.mul(p, p2)
   * p3 instanceof Polytope // true
   * p3.length // 1
   * p3 !== p // true
   * p3 !== p2 // true
   *
   * r.eq(p2d.volume(p), 4) // true
   *
   * r.eq(p2d.volume(p2), 4) // true
   *
   * r.eq(p2d.volume(p3), 1) // true
   *
   * p3.nullify()
   * const p4 = p2d.mul(p2, p3)
   * p4 instanceof Polytope // true
   * p4.length // 0
   *
   * const p5 = p2d.mul(p, p2d.translate(p2d.hypercube(1), [2, 0]))
   * p5 instanceof Polytope // true
   * p5.length // 0
   *
   * p2d.mul(p2, p1d.hypercube(1)) // Error
   * p2d.mul(p1d.hypercube(1), p2) // Error
   * p2d.mul(p2, null) // Error
   * p2d.mul(null, p2) // Error
   */
  mul (p1, p2) {
    if (!(p1 instanceof Polytope)) {
      throw Error('`p` must be an instance of `Polytope`.')
    }
    if (!(p2 instanceof Polytope)) {
      throw Error('`p` must be an instance of `Polytope`.')
    }
    if (p1.isNull() || p2.isNull()) {
      return new Polytope()
    }
    const dim = this.dim
    if (p1.dim !== dim) {
      throw Error('the dimension of `p` must be the same as `this`.')
    }
    if (p2.dim !== dim) {
      throw Error('the dimension of `p` must be the same as `this`.')
    }
    const lalg = this.lalg
    const p3 = new Polytope()
    for (let i = p1.length - 1; i >= 0; i -= 1) {
      const cpi = p1[i]
      for (let j = p2.length - 1; j >= 0; j -= 1) {
        const cpj = p2[j]
        const cp = cpj.copy(lalg).imul(cpi, lalg)
        if (!cp.isNull()) {
          cp.weight = cpi.weight === cpj.weight
          p3.push(cp)
        }
      }
    }
    p3.pairAnnihilation(lalg)
    return p3
  }
}

/* @license-end */
