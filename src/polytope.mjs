/**
 * @source: https://www.npmjs.com/package/@kkitahara/polytope-algebra
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

/**
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Matrix, LinearAlgebra } from '@kkitahara/linear-algebra'
import { ConvexPolytope } from './convex-polytope.mjs'

/**
 * @desc
 * The Polytope class is a class for polytopes.
 * Polytopes are represented as an array of convex polytopes.
 *
 * @version 1.0.0
 * @since 1.0.0
 */
export class Polytope extends Array {
  /**
   * @desc
   * The constructor function of the {@link Polytope} class.
   * Null convex polytopes are ignored.
   *
   * CAUTION: this constructor function does not check if
   * `args` are valid instances of {@link ConvexPolytope}.
   * It checks only the dimension of the convex polytopes.
   *
   * @param {...ConvexPolytope} args
   * instances of {@link ConvexPolytope}.
   *
   * @throws {Error}
   * if some arguments are not instances of {@link ConvexPolytope}.
   *
   * @throws {Error}
   * if some convex polytopes have different dimensions.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { Facet, ConvexPolytope, Polytope } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   *
   * const x = l.$(1, 0)
   * const y = l.$(0, 1)
   * const facets = [
   *   new Facet(x, 1, true),
   *   new Facet(x, -1, false),
   *   new Facet(y, 1, true),
   *   new Facet(y, -1, false)]
   * const vertices = [
   *   l.$(1, 1),
   *   l.$(-1, 1),
   *   l.$(1, -1),
   *   l.$(-1, -1)]
   * facets[0].vertices = [vertices[0], vertices[2]]
   * facets[1].vertices = [vertices[1], vertices[3]]
   * facets[2].vertices = [vertices[0], vertices[1]]
   * facets[3].vertices = [vertices[2], vertices[3]]
   * const edges = ConvexPolytope.genEdges(2, facets)
   * const cp = new ConvexPolytope(facets, vertices, edges)
   * const cp2 = cp.copy(l)
   *
   * let p = new Polytope(cp)
   * p instanceof Polytope // true
   * p.length // 1
   * p[0] === cp // true
   *
   * p = new Polytope(cp, cp2)
   * p.length // 2
   * p[0] === cp // true
   * p[1] === cp2 // true
   *
   * p = new Polytope()
   * p.length // 0
   *
   * // invalid argument
   * new Polytope(null) // Error
   *
   * // inconsistent dimension
   * cp2.vertices[0] = l.$(1)
   * new Polytope(cp, cp2) // Error
   *
   * // the dimension of null convex polytope is undefined,
   * // and the dimension check fails
   * cp2.nullify()
   * p = new Polytope(cp2)
   * p.length // 0
   */
  constructor (...args) {
    if (args.some((arg) => {
      return !(arg instanceof ConvexPolytope)
    })) {
      throw Error('an argument is not an instance of `ConvexPolytope`.')
    }
    const nonNullPolytopes = []
    for (let i = 0, n = args.length; i < n; i += 1) {
      const arg = args[i]
      if (!arg.isNull()) {
        nonNullPolytopes.push(arg)
      }
    }
    if (nonNullPolytopes.length > 0) {
      const dim = nonNullPolytopes[0].dim
      if (nonNullPolytopes.slice(1).some((cp) => {
        return cp.dim !== dim
      })) {
        throw Error('the dimension of the convex polytopes must be the same')
      }
    }
    super(...nonNullPolytopes)
  }

  /**
   * @desc
   * Thanks to this species, built-in {@link Array} methods,
   * such as the {@link Polytope}#slice method,
   * return an {@link Array} object.
   *
   * @type {function}
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { Facet, ConvexPolytope, Polytope } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   *
   * const x = l.$(1, 0)
   * const y = l.$(0, 1)
   * let facets = [
   *   new Facet(x, 1, true),
   *   new Facet(x, -1, false),
   *   new Facet(y, 1, true),
   *   new Facet(y, -1, false)]
   * let vertices = [
   *   l.$(1, 1),
   *   l.$(-1, 1),
   *   l.$(1, -1),
   *   l.$(-1, -1)]
   * facets[0].vertices = [vertices[0], vertices[2]]
   * facets[1].vertices = [vertices[1], vertices[3]]
   * facets[2].vertices = [vertices[0], vertices[1]]
   * facets[3].vertices = [vertices[2], vertices[3]]
   * let edges = ConvexPolytope.genEdges(2, facets)
   * const cp = new ConvexPolytope(facets, vertices, edges)
   *
   * facets = [
   *   new Facet(x, 3, true),
   *   new Facet(x, 1, false),
   *   new Facet(y, 3, true),
   *   new Facet(y, 1, false)]
   * vertices = [
   *   l.$(3, 3),
   *   l.$(1, 3),
   *   l.$(3, 1),
   *   l.$(1, 1)]
   * facets[0].vertices = [vertices[0], vertices[2]]
   * facets[1].vertices = [vertices[1], vertices[3]]
   * facets[2].vertices = [vertices[0], vertices[1]]
   * facets[3].vertices = [vertices[2], vertices[3]]
   * edges = ConvexPolytope.genEdges(2, facets)
   * const cp2 = new ConvexPolytope(facets, vertices, edges)
   *
   * const p = new Polytope(cp, cp2)
   * const arr = p.slice()
   * arr instanceof Array // true
   * p.constructor === Polytope // true
   * arr.constructor === Polytope // false
   * arr.constructor === Array // true
   */
  static get [Symbol.species] () {
    return Array
  }

  /**
   * @desc
   * The Polytope#dim method returns the dimension of `this`.
   * The dimension of polytopes is equal to that of the first convex polytope.
   *
   * @type {number}
   *
   * @throws {Error}
   * if `this` has no convex polytope.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { Facet, ConvexPolytope, Polytope } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   *
   * const x = l.$(1, 0)
   * const y = l.$(0, 1)
   * const facets = [
   *   new Facet(x, 1, true),
   *   new Facet(x, -1, false),
   *   new Facet(y, 1, true),
   *   new Facet(y, -1, false)]
   * const vertices = [
   *   l.$(1, 1),
   *   l.$(-1, 1),
   *   l.$(1, -1),
   *   l.$(-1, -1)]
   * facets[0].vertices = [vertices[0], vertices[2]]
   * facets[1].vertices = [vertices[1], vertices[3]]
   * facets[2].vertices = [vertices[0], vertices[1]]
   * facets[3].vertices = [vertices[2], vertices[3]]
   * const edges = ConvexPolytope.genEdges(2, facets)
   * const cp = new ConvexPolytope(facets, vertices, edges)
   *
   * const p = new Polytope(cp)
   * p.dim // 2
   *
   * new Polytope().dim // Error
   */
  get dim () {
    if (this.isNull()) {
      throw Error('the dimension of null polytopes cannot be ditermined.')
    }
    return this[0].dim
  }

  /**
   * @desc
   * The Polytope#copy method returns a new instance of {@link Polytope}
   * which has copies of instances of {@link ConvexPolytope}
   * as generated by {@link ConvexPolytope#copy}.
   *
   * @param {LinearAlgebra} lalg
   * an instance of {@link LinearAlgebra}.
   *
   * @return {Polytope}
   * a new instance of {@link Polytope}.
   *
   * @version 1.1.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { Facet, ConvexPolytope, Polytope } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   *
   * const x = l.$(1, 0)
   * const y = l.$(0, 1)
   * const facets = [
   *   new Facet(x, 1, true),
   *   new Facet(x, -1, false),
   *   new Facet(y, 1, true),
   *   new Facet(y, -1, false)]
   * const vertices = [
   *   l.$(1, 1),
   *   l.$(-1, 1),
   *   l.$(1, -1),
   *   l.$(-1, -1)]
   * facets[0].vertices = [vertices[0], vertices[2]]
   * facets[1].vertices = [vertices[1], vertices[3]]
   * facets[2].vertices = [vertices[0], vertices[1]]
   * facets[3].vertices = [vertices[2], vertices[3]]
   * const edges = ConvexPolytope.genEdges(2, facets)
   * const cp = new ConvexPolytope(facets, vertices, edges)
   * const cp2 = cp.copy(l)
   *
   * let p = new Polytope(cp)
   * let p2 = p.copy(l)
   * p2 instanceof Polytope // true
   * p2 !== p // true
   * p2.length // 1
   * p2[0] === cp // false
   *
   * p = new Polytope(cp, cp2)
   * p2 = p.copy(l)
   * p2 instanceof Polytope // true
   * p2 !== p // true
   * p2.length // 2
   * p2[0] !== cp // true
   * p2[1] !== cp2 // true
   *
   * p = new Polytope()
   * p2 = p.copy(l)
   * p2 instanceof Polytope // true
   * p2 !== p // true
   * p2.length // 0
   */
  copy (lalg) {
    const arr = []
    for (let i = 0, n = this.length; i < n; i += 1) {
      arr.push(this[i].copy(lalg))
    }
    return new Polytope(...arr)
  }

  /**
   * @desc
   * The Polytope#isNull method checks if `this` is null.
   * `this` is considered to be null if it has no {@link ConvexPolytope}
   * which is not null.
   *
   * @return {boolean}
   * `true` if `this` is null and `false` otherwise.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { Facet, ConvexPolytope, Polytope } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   *
   * const x = l.$(1, 0)
   * const y = l.$(0, 1)
   * const facets = [
   *   new Facet(x, 1, true),
   *   new Facet(x, -1, false),
   *   new Facet(y, 1, true),
   *   new Facet(y, -1, false)]
   * const vertices = [
   *   l.$(1, 1),
   *   l.$(-1, 1),
   *   l.$(1, -1),
   *   l.$(-1, -1)]
   * facets[0].vertices = [vertices[0], vertices[2]]
   * facets[1].vertices = [vertices[1], vertices[3]]
   * facets[2].vertices = [vertices[0], vertices[1]]
   * facets[3].vertices = [vertices[2], vertices[3]]
   * const edges = ConvexPolytope.genEdges(2, facets)
   * const cp = new ConvexPolytope(facets, vertices, edges)
   *
   * let p = new Polytope()
   * p.isNull() // true
   *
   * p = new Polytope(cp)
   * p.isNull() // false
   */
  isNull () {
    return this.length === 0 || this.every((cp) => {
      return cp.isNull()
    })
  }

  /**
   * @desc
   * The Polytope#nullify method nullifies `this`.
   *
   * @return {Polytope}
   * `this`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { Facet, ConvexPolytope, Polytope } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   *
   * const x = l.$(1, 0)
   * const y = l.$(0, 1)
   * const facets = [
   *   new Facet(x, 1, true),
   *   new Facet(x, -1, false),
   *   new Facet(y, 1, true),
   *   new Facet(y, -1, false)]
   * const vertices = [
   *   l.$(1, 1),
   *   l.$(-1, 1),
   *   l.$(1, -1),
   *   l.$(-1, -1)]
   * facets[0].vertices = [vertices[0], vertices[2]]
   * facets[1].vertices = [vertices[1], vertices[3]]
   * facets[2].vertices = [vertices[0], vertices[1]]
   * facets[3].vertices = [vertices[2], vertices[3]]
   * const edges = ConvexPolytope.genEdges(2, facets)
   * const cp = new ConvexPolytope(facets, vertices, edges)
   *
   * const p = new Polytope(cp)
   * p.isNull() // false
   * p.nullify()
   * p.isNull() // true
   * p.length === 0 // true
   */
  nullify () {
    this.length = 0
    return this
  }

  /**
   * @desc
   * The Polytope#reduce method reduces the number of convex polytopes in
   * `this` by concatenating facing convex polytopes if possible.
   *
   * @param {LinearAlgebra} lalg
   * an instance of {@link LinearAlgebra}.
   *
   * @return {Polytope}
   * `this`.
   *
   * @version 1.1.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { Facet, ConvexPolytope, Polytope } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   *
   * const x = l.$(1, 0)
   * const y = l.$(0, 1)
   * let facets = [
   *   new Facet(x, 1, true),
   *   new Facet(x, -1, false),
   *   new Facet(y, 1, true),
   *   new Facet(y, -1, false)]
   * let vertices = [
   *   l.$(1, 1),
   *   l.$(-1, 1),
   *   l.$(1, -1),
   *   l.$(-1, -1)]
   * facets[0].vertices = [vertices[0], vertices[2]]
   * facets[1].vertices = [vertices[1], vertices[3]]
   * facets[2].vertices = [vertices[0], vertices[1]]
   * facets[3].vertices = [vertices[2], vertices[3]]
   * let edges = ConvexPolytope.genEdges(2, facets)
   * const cp = new ConvexPolytope(facets, vertices, edges)
   *
   * facets = [
   *   new Facet(x, 3, true),
   *   new Facet(x, 1, false),
   *   new Facet(y, 3, true),
   *   new Facet(y, 1, false)]
   * vertices = [
   *   l.$(3, 3),
   *   l.$(1, 3),
   *   l.$(3, 1),
   *   l.$(1, 1)]
   * facets[0].vertices = [vertices[0], vertices[2]]
   * facets[1].vertices = [vertices[1], vertices[3]]
   * facets[2].vertices = [vertices[0], vertices[1]]
   * facets[3].vertices = [vertices[2], vertices[3]]
   * edges = ConvexPolytope.genEdges(2, facets)
   * const cp2 = new ConvexPolytope(facets, vertices, edges)
   *
   * let p = new Polytope(cp, cp2)
   * p.length // 2
   *
   * p.reduce(l)
   * p.length // 2
   *
   * facets = [
   *   new Facet(x, 3, true),
   *   new Facet(x, 1, false),
   *   new Facet(y, 1, true),
   *   new Facet(y, -1, false)]
   * vertices = [
   *   l.$(3, 1),
   *   l.$(1, 1),
   *   l.$(3, -1),
   *   l.$(1, -1)]
   * facets[0].vertices = [vertices[0], vertices[2]]
   * facets[1].vertices = [vertices[1], vertices[3]]
   * facets[2].vertices = [vertices[0], vertices[1]]
   * facets[3].vertices = [vertices[2], vertices[3]]
   * edges = ConvexPolytope.genEdges(2, facets)
   * const cp3 = new ConvexPolytope(facets, vertices, edges)
   *
   * p = new Polytope(cp, cp3)
   * p.length // 2
   *
   * p.reduce(l)
   * p.length // 1
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { PolytopeAlgebra } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra(1e-5)
   * const l = new LinearAlgebra(r)
   * const p2d = new PolytopeAlgebra(2, l)
   *
   * const p = p2d.hypercube(1)
   * let p2 = p2d.hypercube(1)
   * p2 = p2d.iaddFacet(p2, p2d.facet([-1, -1], 0, false))
   * p2 = p2d.itranslate(p2, [2, 0])
   *
   * p.push(p2[0])
   * p.reduce(l)
   * p.length // 1
   *
   * const p3 = p2d.translate(p2d.hypercube(1), [0, 2])
   * p.push(p3[0])
   * p.reduce(l)
   * p.length // 2
   *
   * let p4 = p2d.hypercube(1)
   * p4 = p2d.iaddFacet(p4, p2d.facet([1, 1], 0, false))
   * p4 = p2d.itranslate(p4, [2, 0])
   * p.push(p4[0])
   * p.reduce(l)
   * p.length // 2
   *
   * p.push(p4[0])
   * p[1].nullify()
   * p.reduce(l)
   * p.length // 2
   *
   * p.nullify()
   * p.reduce(l)
   * p.length // 0
   *
   * l.isReal = () => false
   * p.reduce(l) // Error
   *
   * p.reduce(null) // Error
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { PolytopeAlgebra } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra(1e-5)
   * const l = new LinearAlgebra(r)
   * const p3d = new PolytopeAlgebra(3, l)
   *
   * const p = p3d.hypercube(1)
   * let p2 = p3d.hypercube(1)
   * p2 = p3d.iaddFacet(p2, p3d.facet([-1, -1, -1], 1, true))
   * p2 = p3d.itranslate(p2, [0, 0, 2])
   * p.push(p2[0])
   * p.reduce(l)
   * p.length // 2
   *
   * p[1].nullify()
   * p.reduce(l)
   * p.length // 1
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { PolytopeAlgebra } from '@kkitahara/polytope-algebra'
   *
   * // test for issue #2
   *
   * const r = new RealAlgebra(1e-10)
   * const l = new LinearAlgebra(r)
   * const p2d = new PolytopeAlgebra(2, l)
   *
   * let p1 = p2d.hypercube(1)
   * const n1 = [-1, 1]
   * const f1 = p2d.facet(n1, l.dot(n1, [-1, 0]), true)
   * p1 = p2d.itranslate(p2d.iaddFacet(p1, f1), [2, 0])
   * let p2 = p2d.hypercube(1)
   * const n2 = [-0.1, 0.01]
   * const f2 = p2d.facet(n2, l.dot(n2, [1, 0]), true)
   * p2 = p2d.iaddFacet(p2, f2)
   * p1.length // 1
   * p2.length // 1
   *
   * const p3 = p2d.add(p1, p2)
   * // shoud be 1
   * p3.length // 1
   */
  reduce (lalg) {
    if (!(lalg instanceof LinearAlgebra)) {
      throw Error('`lalg` must be an instance of `LinearAlgebra`.')
    }
    if (!lalg.isReal()) {
      throw Error('`lalg` must be an implementation of real algebra.')
    }
    if (this.isNull()) {
      return this
    }
    const ralg = lalg.salg
    const dim = this.dim
    for (let i = this.length - 2; i >= 0; i -= 1) {
      const cpi = this[i]
      if (cpi.isNull()) {
        this.splice(i, 1)
        continue
      }
      const facetsi = cpi.facets
      const verticesi = cpi.vertices
      for (let j = this.length - 1; j > i; j -= 1) {
        const cpj = this[j]
        if (cpj.isNull()) {
          this.splice(j, 1)
          continue
        }
        const facetsj = cpj.facets
        const verticesj = cpj.vertices
        // find a pair of facing facets
        let fi = null
        let fj = null
        for (let k = facetsi.length - 1; k >= 0; k -= 1) {
          const fk = facetsi[k]
          for (let l = facetsj.length - 1; l >= 0; l -= 1) {
            const fl = facetsj[l]
            if (fk.nvec === fl.nvec && fk.faceOutside !== fl.faceOutside) {
              if (ralg.eq(fk.d, fl.d)) {
                fi = fk
                fj = fl
              }
              break
            }
          }
          if (fi !== null) {
            break
          }
        }
        if (fi === null) {
          continue
        }
        // check if the two facets are exactly the same
        const verticesfi = fi.vertices
        const verticesfj = fj.vertices
        if (verticesfi.length !== verticesfj.length) {
          continue
        }
        const vPairs = []
        for (let k = verticesfi.length - 1; k >= 0; k -= 1) {
          const vk = verticesfi[k]
          for (let l = verticesfj.length - 1; l >= 0; l -= 1) {
            const vl = verticesfj[l]
            if (lalg.eq(vk, vl)) {
              vPairs.push([vk, vl])
              break
            }
            if (l === 0) {
              vPairs.length = 0
            }
          }
          if (vPairs.length === 0) {
            break
          }
        }
        if (vPairs.length === 0) {
          continue
        }
        // check if the concatenated polytope is convex
        const ridgesi = []
        for (let k = facetsi.length - 1; k >= 0; k -= 1) {
          const fk = facetsi[k]
          if (fk === fi) {
            continue
          }
          const verticesk = fk.vertices
          const onRidgeVertices = []
          for (let l = verticesfi.length - 1; l >= 0; l -= 1) {
            const vl = verticesfi[l]
            for (let m = verticesk.length - 1; m >= 0; m -= 1) {
              const vm = verticesk[m]
              if (vl === vm) {
                onRidgeVertices.push(vl)
                break
              }
            }
          }
          if (onRidgeVertices.length >= dim - 1) {
            ridgesi.push({ facet: fk, vertices: onRidgeVertices })
          }
        }
        const ridgesj = []
        for (let k = facetsj.length - 1; k >= 0; k -= 1) {
          const fk = facetsj[k]
          if (fk === fj) {
            continue
          }
          const verticesk = fk.vertices
          const onRidgeVertices = []
          for (let l = verticesfj.length - 1; l >= 0; l -= 1) {
            const vl = verticesfj[l]
            for (let m = verticesk.length - 1; m >= 0; m -= 1) {
              const vm = verticesk[m]
              if (vl === vm) {
                onRidgeVertices.push(vl)
                break
              }
            }
          }
          if (onRidgeVertices.length >= dim - 1) {
            ridgesj.push({ facet: fk, vertices: onRidgeVertices })
          }
        }
        const ridgeConditions = []
        let isConvex = true
        for (let l = 0, n = ridgesj.length; l < n; l += 1) {
          const ridgel = ridgesj[l]
          const verticesl = ridgel.vertices
          for (let k = ridgesi.length - 1; k >= 0; k -= 1) {
            const ridgek = ridgesi[k]
            const verticesk = ridgek.vertices
            // find the same ridge
            if (verticesl.every((v) => {
              let vPair
              for (let m = vPairs.length - 1; m >= 0; m -= 1) {
                if (v === vPairs[m][1]) {
                  vPair = vPairs[m][0]
                  break
                }
              }
              for (let m = verticesk.length - 1; m >= 0; m -= 1) {
                if (verticesk[m] === vPair) {
                  return true
                }
              }
              return false
            })) {
              let isParallel
              if (ridgek.facet.nvec === ridgel.facet.nvec) {
                isParallel = true
              } else {
                isParallel = false
                let nvec0 = fi.nvec
                if (!fi.faceOutside) {
                  nvec0 = lalg.neg(nvec0)
                }
                let nvec1 = ridgek.facet.nvec
                if (!ridgek.facet.faceOutside) {
                  nvec1 = lalg.neg(nvec1)
                }
                let nvec2 = ridgel.facet.nvec
                if (!ridgel.facet.faceOutside) {
                  nvec2 = lalg.neg(nvec2)
                }
                const dot01 = lalg.dot(nvec0, nvec1)
                const dot02 = lalg.dot(nvec0, nvec2)
                const dot01IsPositive = ralg.isPositive(dot01)
                const dot02IsPositive = ralg.isPositive(dot02)
                if (dot01IsPositive !== dot02IsPositive) {
                  isConvex = dot02IsPositive
                } else {
                  const dot11 = lalg.abs2(nvec1)
                  const dot22 = lalg.abs2(nvec2)
                  const tmp = ralg.isub(
                    ralg.idiv(ralg.mul(dot02, dot02), dot22),
                    ralg.idiv(ralg.mul(dot01, dot01), dot11))
                  isConvex = dot01IsPositive === ralg.isPositive(tmp)
                }
              }
              ridgeConditions.push({ ridgesiIndex: k, isParallel: isParallel })
              break
            }
          }
          if (!isConvex) {
            break
          }
        }
        if (!isConvex) {
          continue
        }
        // remove facing facets
        facetsi.splice(facetsi.indexOf(fi), 1)
        facetsj.splice(facetsj.indexOf(fj), 1)
        // add new vertices
        for (let k = verticesj.length - 1; k >= 0; k -= 1) {
          const vk = verticesj[k]
          if (vPairs.every((vPair) => {
            return vk !== vPair[1]
          })) {
            verticesi.push(vk)
          }
        }
        // add new facets
        for (let k = facetsj.length - 1; k >= 0; k -= 1) {
          const fk = facetsj[k]
          const verticesk = fk.vertices
          for (let l = ridgesj.length - 1; l >= 0; l -= 1) {
            const ridgel = ridgesj[l]
            if (fk === ridgel.facet && ridgeConditions[l].isParallel) {
              const ridgei = ridgesi[ridgeConditions[l].ridgesiIndex]
              for (let m = verticesk.length - 1; m >= 0; m -= 1) {
                const vm = verticesk[m]
                if (vPairs.every((vPair) => {
                  return vm !== vPair[1]
                })) {
                  ridgei.facet.vertices.push(vm)
                }
              }
              break
            }
            if (l === 0) {
              for (let m = vPairs.length - 1; m >= 0; m -= 1) {
                for (let n = verticesk.length - 1; n >= 0; n -= 1) {
                  if (verticesk[n] === vPairs[m][1]) {
                    verticesk.splice(n, 1, vPairs[m][0])
                    break
                  }
                }
              }
              facetsi.push(fk)
            }
          }
        }
        // remove vertices which are not needed
        for (let k = vPairs.length - 1; k >= 0; k -= 1) {
          const vk = vPairs[k][0]
          let fCount = 0
          for (let l = facetsi.length - 1; l >= 0; l -= 1) {
            if (facetsi[l].vertices.includes(vk)) {
              fCount += 1
            }
            if (fCount === dim) {
              break
            }
            if (l === 0) {
              for (let m = verticesi.length - 1; m >= 0; m -= 1) {
                if (verticesi[m] === vk) {
                  verticesi.splice(m, 1)
                  break
                }
              }
              for (let m = facetsi.length - 1; m >= 0; m -= 1) {
                const verticesm = facetsi[m].vertices
                for (let n = verticesm.length - 1; n >= 0; n -= 1) {
                  if (verticesm[n] === vk) {
                    verticesm.splice(n, 1)
                    break
                  }
                }
              }
            }
          }
        }
        cpi.edges = ConvexPolytope.genEdges(dim, cpi.facets)
        this.splice(j, 1)
        j = this.length
      }
    }
    return this
  }

  /**
   * @desc
   * The Polytope#pairAnnihilation method reduces
   * the number of convex polytopes in `this`
   * by removing pairs of convex polytopes which have different
   * weights if possible.
   *
   * @param {LinearAlgebra} lalg
   * an instance of {@link LinearAlgebra}.
   *
   * @return {Polytope}
   * `this`.
   *
   * @version 1.1.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { PolytopeAlgebra } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra(1e-5)
   * const l = new LinearAlgebra(r)
   * const p2d = new PolytopeAlgebra(2, l)
   *
   * let p = p2d.hypercube(1)
   * const a = Math.sqrt(2) / 2
   * const p2 = p2d.translate(p2d.rotate(p, l.$(a, a, a, -a).setDim(2)), [3, 3])
   * p = p2d.add(p, p2)
   * p[1].weight = false
   * p.pairAnnihilation(l)
   * p.length // 2
   *
   * p[1].nullify()
   * p.pairAnnihilation(l)
   * p.length // 1
   *
   * p = p2d.hypercube(1)
   * p = p2d.add(p, p2)
   * p[0].nullify()
   * p.pairAnnihilation(l)
   * p.length // 1
   *
   * p.pairAnnihilation(null) // Error
   */
  pairAnnihilation (lalg) {
    if (!(lalg instanceof LinearAlgebra)) {
      throw Error('`lalg` must be an instance of `LinearAlgebra`.')
    }
    if (this.isNull()) {
      return this
    }
    const salg = lalg.salg
    for (let i = this.length - 2; i >= 0; i -= 1) {
      const cpi = this[i]
      if (cpi.isNull()) {
        this.splice(i, 1)
        continue
      }
      const facetsi = cpi.facets
      for (let j = this.length - 1; j > i; j -= 1) {
        const cpj = this[j]
        if (cpj.isNull()) {
          this.splice(j, 1)
          continue
        }
        const facetsj = cpj.facets
        if (cpi.weight !== cpj.weight && facetsi.length === facetsj.length) {
          if (facetsi.every((fi) => {
            for (let k = facetsj.length - 1; k >= 0; k -= 1) {
              const fj = facetsj[k]
              if (fi.nvec === fj.nvec && fi.faceOutside === fj.faceOutside) {
                if (salg.eq(fi.d, fj.d)) {
                  return true
                } else {
                  return false
                }
              }
            }
            return false
          })) {
            this.splice(j, 1)
            this.splice(i, 1)
            break
          }
        }
      }
    }
    return this
  }

  /**
   * @desc
   * The Polytope#genSimplexes method
   * generates simplexes from `this`.
   * Each simplex is an {@link Array} containing the vertices of the simplex
   * with weight property.
   *
   * @return {Array}
   * an {@link Array} containing the generated simplexes.
   *
   * @throws {Error}
   * if `this.dim` is `0`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { Facet, ConvexPolytope, Polytope } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   *
   * const x = l.$(1, 0)
   * const y = l.$(0, 1)
   * const facets = [
   *   new Facet(x, 1, true),
   *   new Facet(x, -1, false),
   *   new Facet(y, 1, true),
   *   new Facet(y, -1, false)]
   * const vertices = [
   *   l.$(1, 1),
   *   l.$(-1, 1),
   *   l.$(1, -1),
   *   l.$(-1, -1)]
   * facets[0].vertices = [vertices[0], vertices[2]]
   * facets[1].vertices = [vertices[1], vertices[3]]
   * facets[2].vertices = [vertices[0], vertices[1]]
   * facets[3].vertices = [vertices[2], vertices[3]]
   * const edges = ConvexPolytope.genEdges(2, facets)
   * const cp = new ConvexPolytope(facets, vertices, edges)
   * const cp2 = cp.copy(l)
   * // ANTI-PATTERN!
   * // this is just for a test
   * let p = new Polytope(cp, cp2)
   * let simplexes = p.genSimplexes()
   * simplexes instanceof Array // true
   * simplexes.length // 4
   * simplexes[0].weight // true
   *
   * p = new Polytope()
   * simplexes = p.genSimplexes()
   * simplexes instanceof Array // true
   * simplexes.length // 0
   */
  genSimplexes () {
    if (this.isNull()) {
      return []
    }
    const dim = this.dim
    const simplexes = []
    for (let i = this.length - 1; i >= 0; i -= 1) {
      const cp = this[i]
      const arr = ConvexPolytope.genSimplexes(dim, cp.facets)
      for (const simplex of arr) {
        simplex.weight = cp.weight
        simplexes.push(simplex)
      }
    }
    return simplexes
  }

  /**
   * @desc
   * The Polytope#toJSON method converts
   * `this` to an object serialisable by `JSON.stringify`.
   *
   * @return {object}
   * a serialisable object for `this`.
   *
   * @version 1.1.4
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { Facet, ConvexPolytope, Polytope } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   *
   * const x = l.$(1, 0)
   * const y = l.$(0, 1)
   * const facets = [
   *   new Facet(x, 1, true),
   *   new Facet(x, -1, false),
   *   new Facet(y, 1, true),
   *   new Facet(y, -1, false)]
   * const vertices = [
   *   l.$(1, 1),
   *   l.$(-1, 1),
   *   l.$(1, -1),
   *   l.$(-1, -1)]
   * facets[0].vertices = [vertices[0], vertices[2]]
   * facets[1].vertices = [vertices[1], vertices[3]]
   * facets[2].vertices = [vertices[0], vertices[1]]
   * facets[3].vertices = [vertices[2], vertices[3]]
   * const edges = ConvexPolytope.genEdges(2, facets)
   * const cp = new ConvexPolytope(facets, vertices, edges)
   * const cp2 = cp.copy(l)
   *
   * const p = new Polytope(cp, cp2)
   *
   * // toJSON method is called by JSON.stringify
   * const s = JSON.stringify(p)
   * typeof s // 'string'
   *
   * p.push(new ConvexPolytope())
   * const s2 = JSON.stringify(p)
   * typeof s2 // 'string'
   * s === s2 // true
   */
  toJSON () {
    const obj = {}
    obj.reviver = 'Polytope'
    obj.version = '1.1.4'
    obj.nvecs = []
    obj.convexPolytopes = []
    for (let i = this.length - 1; i >= 0; i -= 1) {
      const cp = this[i]
      if (cp.isNull()) {
        this.splice(i, 1)
        continue
      }
      const facets = cp.facets
      for (let j = facets.length - 1; j >= 0; j -= 1) {
        const nv = facets[j].nvec
        if (!obj.nvecs.includes(nv)) {
          obj.nvecs.push(nv)
        }
      }
    }
    for (let i = this.length - 1; i >= 0; i -= 1) {
      const cp = this[i]
      const facets = []
      const edges = []
      for (let j = cp.facets.length - 1; j >= 0; j -= 1) {
        const fj = cp.facets[j]
        const fvID = []
        for (let k = fj.vertices.length - 1; k >= 0; k -= 1) {
          fvID.push(cp.vertices.indexOf(fj.vertices[k]))
        }
        facets.push([obj.nvecs.indexOf(fj.nvec), fj.d, fj.faceOutside, fvID])
      }
      for (let j = cp.edges.length - 1; j >= 0; j -= 1) {
        const ej = cp.edges[j]
        edges.push([cp.vertices.indexOf(ej.v0), cp.vertices.indexOf(ej.v1)])
      }
      obj.convexPolytopes.push({
        facets: facets,
        vertices: cp.vertices,
        edges: edges,
        weight: cp.weight
      })
    }
    return obj
  }

  /**
   * @desc
   * The Polytope.volume function
   * calculates the sum of the volumes of simplexes.
   * A simplex is an array of vertices.
   * To generate the simplexes for an instance of {@link Polytope},
   * use Polytope#genSimplexes method.
   *
   * @param {Array} simplexes
   * an {@link Array} containing simplexes.
   *
   * @param {LinearAlgebra} lalg
   * an instance of {@link LinearAlgebra}.
   *
   * @return {RealAlgebraicElement}
   * the volume of the given simplexes.
   *
   * @throws {Error}
   * if `lalg` is not an instance of `LinearAlgebra`.
   *
   * @throws {Error}
   * if `lalg` is not an instance of real algebra.
   *
   * @throws {Error}
   * if `simplexes` is not an {@link Array}.
   *
   * @throws {Error}
   * if a simplex does not have the same dimension.
   *
   * @throws {Error}
   * if a vertex is not an adaptive matrix representing a vector of the same
   * dimension.
   *
   * @version 1.1.4
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { Facet, ConvexPolytope, Polytope } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   *
   * const x = l.$(1, 0)
   * const y = l.$(0, 1)
   * const facets = [
   *   new Facet(x, 1, true),
   *   new Facet(x, -1, false),
   *   new Facet(y, 1, true),
   *   new Facet(y, -1, false)]
   * const vertices = [
   *   l.$(1, 1),
   *   l.$(-1, 1),
   *   l.$(1, -1),
   *   l.$(-1, -1)]
   * facets[0].vertices = [vertices[0], vertices[2]]
   * facets[1].vertices = [vertices[1], vertices[3]]
   * facets[2].vertices = [vertices[0], vertices[1]]
   * facets[3].vertices = [vertices[2], vertices[3]]
   * const edges = ConvexPolytope.genEdges(2, facets)
   * const cp = new ConvexPolytope(facets, vertices, edges)
   * let p = new Polytope(cp)
   * let simplexes = p.genSimplexes()
   * Polytope.volume(simplexes, l) // 4
   *
   * let cp2 = cp.copy(l)
   * cp2.iaddFacet(new Facet(x, 1, true), l)
   * p = new Polytope(cp2)
   * simplexes = p.genSimplexes()
   * Polytope.volume(simplexes, l) // 4
   *
   * cp2 = cp.copy(l)
   * cp2.iaddFacet(new Facet(x, 1, false), l)
   * p = new Polytope(cp2)
   * simplexes = p.genSimplexes()
   * Polytope.volume(simplexes, l) // 0
   *
   * cp2 = cp.copy(l)
   * cp2.iaddFacet(new Facet(x, -1, true), l)
   * p = new Polytope(cp2)
   * simplexes = p.genSimplexes()
   * Polytope.volume(simplexes, l) // 0
   *
   * cp2 = cp.copy(l)
   * cp2.iaddFacet(new Facet(x, -1, false), l)
   * p = new Polytope(cp2)
   * simplexes = p.genSimplexes()
   * Polytope.volume(simplexes, l) // 4
   *
   * cp2 = cp.copy(l)
   * cp2.iaddFacet(new Facet(l.$(1, 1), 2, true), l)
   * p = new Polytope(cp2)
   * simplexes = p.genSimplexes()
   * Polytope.volume(simplexes, l) // 4
   *
   * cp2 = cp.copy(l)
   * cp2.iaddFacet(new Facet(l.$(1, 1), 1, true), l)
   * p = new Polytope(cp2)
   * simplexes = p.genSimplexes()
   * Polytope.volume(simplexes, l) // 3.5
   *
   * cp2 = cp.copy(l)
   * cp2.iaddFacet(new Facet(l.$(1, 1), 0, true), l)
   * p = new Polytope(cp2)
   * simplexes = p.genSimplexes()
   * Polytope.volume(simplexes, l) // 2
   *
   * cp2 = cp.copy(l)
   * cp2.iaddFacet(new Facet(l.$(1, 1), -1, true), l)
   * p = new Polytope(cp2)
   * simplexes = p.genSimplexes()
   * Polytope.volume(simplexes, l) // 0.5
   *
   * cp2 = cp.copy(l)
   * cp2.iaddFacet(new Facet(l.$(1, 1), -2, true), l)
   * p = new Polytope(cp2)
   * simplexes = p.genSimplexes()
   * Polytope.volume(simplexes, l) // 0
   *
   * cp2 = cp.copy(l)
   * // ANTI-PATTERN!
   * // this is just for a test
   * p = new Polytope(cp, cp2)
   * simplexes = p.genSimplexes()
   * Polytope.volume(simplexes, l) // 8
   *
   * // 0-dim
   * simplexes = [[l.$()]]
   * simplexes[0].weight = true
   * Polytope.volume(simplexes, l) // 1
   *
   * cp2 = cp.copy(l)
   * p = new Polytope(cp2)
   * simplexes = p.genSimplexes()
   * Polytope.volume(simplexes, null) // Error
   * Polytope.volume(null, l) // Error
   * simplexes.push(null)
   * Polytope.volume(simplexes, l) // Error
   * simplexes.pop()
   * simplexes[0].push(l.$(1, 1))
   * Polytope.volume(simplexes, l) // Error
   * simplexes[0].pop()
   * simplexes[0].pop()
   * simplexes[0].push(l.$(1, 1, 1))
   * Polytope.volume(simplexes, l) // Error
   *
   * l.isReal = () => false
   * Polytope.volume(simplexes, l) // Error
   */
  static volume (simplexes, lalg) {
    if (!(lalg instanceof LinearAlgebra)) {
      throw Error('`lalg` must be an instance of `LinearAlgebra`.')
    }
    if (!lalg.isReal()) {
      throw Error('`lalg` must be an implementation of real algebra.')
    }
    if (!(simplexes instanceof Array)) {
      throw Error('`simplexes` must be an array.')
    }
    const ralg = lalg.salg
    if (simplexes.length === 0) {
      return ralg.num(0)
    } else {
      const dim = simplexes[0].length - 1
      if (simplexes.slice(1).some((simplex) => {
        return !(simplex instanceof Array) || simplex.length !== dim + 1
      })) {
        throw Error('all simplexes must be the same dimension.')
      }
      if (simplexes.some((simplex) => {
        return simplex.some((v) => {
          return !(v instanceof Matrix) ||
            !(v.isAdaptive() || v.length === 0) || v.length !== dim
        })
      })) {
        throw Error('all vertices must be the same dimension.')
      }
      return simplexes.reduce((vol, simplex) => {
        const arr = []
        const v0 = simplex[dim]
        for (let i = dim - 1; i >= 0; i -= 1) {
          arr.push(...lalg.sub(simplex[i], v0))
        }
        let volSimplex = lalg.det(lalg.ilup(lalg.cast(arr).setDim(dim, dim)))
        volSimplex = ralg.iabs(volSimplex)
        if (!simplex.weight) {
          volSimplex = ralg.ineg(volSimplex)
        }
        for (let i = dim; i >= 2; i -= 1) {
          volSimplex = ralg.idiv(volSimplex, i)
        }
        return ralg.iadd(vol, volSimplex)
      }, ralg.num(0))
    }
  }
}

/* @license-end */
