/**
 * @source: https://www.npmjs.com/package/@kkitahara/polytope-algebra
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

/**
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { Facet } from './facet.mjs'
import { Edge } from './edge.mjs'
import { ConvexPolytope } from './convex-polytope.mjs'
import { Polytope } from './polytope.mjs'

/**
 * @desc
 * The PolytopeAlgebra class is a class for set algebra of polytopes.
 *
 * @version 1.1.1
 * @since 1.0.0
 *
 * @example
 * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
 * // import { RealAlgebra } from '@kkitahara/real-algebra'
 * import { LinearAlgebra } from '@kkitahara/linear-algebra'
 * import { PolytopeAlgebra } from '@kkitahara/polytope-algebra'
 * const r = new RealAlgebra(1e-8)
 * const l = new LinearAlgebra(r)
 * const dim = 2
 * const p2d = new PolytopeAlgebra(dim, l)
 *
 * // Generate a new hypercube of edge length `2 * d` centred at the origin.
 * let d = 1
 * let p1 = p2d.hypercube(d)
 *
 * // p1
 * // +-----------+
 * // |\\\\\\\\\\\|
 * // |\\\\\\\\\\\|
 * // |\\\\\o\\\\\|
 * // |\\\\\\\\\\\|
 * // |\\\\\\\\\\\|
 * // +-----------+
 *
 * // Calculate the volume of polytopes
 * const vol = p2d.volume(p1)
 * vol.toString() // '4'
 *
 * // Generate a new facet.
 * // A facet is characterised by its normal vector (nvec),
 * // distance from the origin (d),
 * // and the direction of the normal vector (faceOutside).
 * let nvec = [1, 0]
 * d = 1
 * let faceOutside = true
 * let f = p2d.facet(nvec, d, faceOutside)
 *
 * // \\\\\\\\\\\\|
 * // \\\\\\\\\\\\|
 * // \\\\\\\\\\\\|
 * // \\\\\\o\\\\\| f
 * // \\\\\\\\\\\\|
 * // \\\\\\\\\\\\|
 * // \\\\\\\\\\\\|
 *
 * nvec = [1, 0]
 * d = 1
 * faceOutside = false
 * f = p2d.facet(nvec, d, faceOutside)
 *
 * //             |\\\\\\\\\\\\
 * //             |\\\\\\\\\\\\
 * //             |\\\\\\\\\\\\
 * //       o   f |\\\\\\\\\\\\
 * //             |\\\\\\\\\\\\
 * //             |\\\\\\\\\\\\
 * //             |\\\\\\\\\\\\
 *
 * // No need to normalise the normal vector,
 * // provided that `d` is the dot product of the normal vector and
 * // the position vector of a vertex on the facet.
 * f = p2d.facet([2, 0], 1, true)
 *
 * // \\\\\\\\\|
 * // \\\\\\\\\|
 * // \\\\\\\\\|
 * // \\\\\\o\\| f
 * // \\\\\\\\\|
 * // \\\\\\\\\|
 * // \\\\\\\\\|
 *
 * f = p2d.facet([2, 0], 2, true)
 *
 * // \\\\\\\\\\\\|
 * // \\\\\\\\\\\\|
 * // \\\\\\\\\\\\|
 * // \\\\\\o\\\\\| f
 * // \\\\\\\\\\\\|
 * // \\\\\\\\\\\\|
 * // \\\\\\\\\\\\|
 *
 * // Add a facet to polytopes (in place)
 * p1 = p2d.hypercube(d)
 *
 * // p1
 * // +-----------+
 * // |\\\\\\\\\\\|
 * // |\\\\\\\\\\\|
 * // |\\\\\o\\\\\|
 * // |\\\\\\\\\\\|
 * // |\\\\\\\\\\\|
 * // +-----------+
 *
 * f = p2d.facet([2, -1], 1, true)
 *
 * // \\\\\\\\\\\\/
 * // \\\\\\\\\\\/
 * // \\\\\\\\\\/
 * // \\\\\\o\\/ f
 * // \\\\\\\\/
 * // \\\\\\\/
 * // \\\\\\/
 *
 * p1 = p2d.iaddFacet(p1, f)
 * p2d.volume(p1).toString() // '3'
 *
 * // p1
 * // +-----------+
 * // |\\\\\\\\\\/
 * // |\\\\\\\\\/
 * // |\\\\\o\\/
 * // |\\\\\\\/
 * // |\\\\\\/
 * // +-----+
 *
 * // Copy (generate a new object)
 * let p2 = p2d.copy(p1)
 * p2d.volume(p2).toString() // '3'
 *
 * // p2
 * // +-----------+
 * // |\\\\\\\\\\/
 * // |\\\\\\\\\/
 * // |\\\\\o\\/
 * // |\\\\\\\/
 * // |\\\\\\/
 * // +-----+
 *
 * // Rotation (new object is genrated, since v1.1.0)
 * const c4 = l.$(0, -1, 1, 0).setDim(2)
 * p2 = p2d.rotate(p1, c4)
 * p1 !== p2 // true
 * p2d.volume(p2).toString() // '3'
 *
 * // p2
 * // +-__
 * // |\\\--_
 * // |\\\\\\--__
 * // |\\\\\o\\\\-+
 * // |\\\\\\\\\\\|
 * // |\\\\\\\\\\\|
 * // +-----------+
 *
 * // In-place rotation (new object is not genrated, since v1.1.0)
 * p2 = p2d.irotate(p2, c4)
 * p2d.volume(p2).toString() // '3'
 *
 * // p2
 * //       +-----+
 * //      /\\\\\\|
 * //     /\\\\\\\|
 * //    /\\o\\\\\|
 * //   /\\\\\\\\\|
 * //  /\\\\\\\\\\|
 * // +-----------+
 *
 * // Translation (new object is generated)
 * p2 = p2d.translate(p1, [r.$(1, 2), 0])
 * p1 !== p2 // true
 * p2d.volume(p2).toString() // '3'
 *
 * // p2
 * //    +-----------+
 * //    |\\\\\\\\\\/
 * //    |\\\\\\\\\/
 * //    |\\o\\\\\/
 * //    |\\\\\\\/
 * //    |\\\\\\/
 * //    +-----+
 *
 * // In-place translation (new object is not generated)
 * p1 = p2d.itranslate(p1, [r.$(1, 2), 0])
 * p2d.volume(p1).toString() // '3'
 *
 * // p1
 * //    +-----------+
 * //    |\\\\\\\\\\/
 * //    |\\\\\\\\\/
 * //    |\\o\\\\\/
 * //    |\\\\\\\/
 * //    |\\\\\\/
 * //    +-----+
 *
 * // Scaling (new object is generated)
 * p2 = p2d.scale(p1, 2)
 * p1 !== p2 // true
 * p2d.volume(p2).toString() // '12'
 *
 * // p2
 * // +-----------------------+
 * // |\\\\\\\\\\\\\\\\\\\\\\/
 * // |\\\\\\\\\\\\\\\\\\\\\/
 * // |\\\\\\\\\\\\\\\\\\\\/
 * // |\\\\\\\\\\\\\\\\\\\/
 * // |\\\\\\\\\\\\\\\\\\/
 * // |\\\\\o\\\\\\\\\\\/
 * // |\\\\\\\\\\\\\\\\/
 * // |\\\\\\\\\\\\\\\/
 * // |\\\\\\\\\\\\\\/
 * // |\\\\\\\\\\\\\/
 * // |\\\\\\\\\\\\/
 * // +-----------+
 *
 * // In-place scaling (new object is not generated)
 * p1 = p2d.iscale(p1, r.$(1, 3))
 * p2d.volume(p1).toString() // '1 / 3'
 *
 * //      p1
 * //      +---+
 * //      |o\/
 * //      +-+
 *
 * // Multiplication (intersection, new object is generated)
 * p1 = p2d.hypercube(1)
 * p2 = p2d.itranslate(p2d.hypercube(1), [1, 1])
 * let p3 = p2d.mul(p1, p2)
 * p3 !== p1 // true
 * p3 !== p2 // true
 * p2d.volume(p3).toString() // '1'
 *
 * //
 * //
 * //
 * // p1
 * // +-----------+
 * // |\\\\\\\\\\\|
 * // |\\\\\\\\\\\|
 * // |\\\\\o\\\\\|
 * // |\\\\\\\\\\\|
 * // |\\\\\\\\\\\|
 * // +-----------+
 *
 * //       p2
 * //       +-----------+
 * //       |\\\\\\\\\\\|
 * //       |\\\\\\\\\\\|
 * //       |\\\\\\\\\\\|
 * //       |\\\\\\\\\\\|
 * //       |\\\\\\\\\\\|
 * //       o-----------+
 * //
 * //
 * //
 *
 * //
 * //
 * //
 * //       p3
 * //       +-----+
 * //       |\\\\\|
 * //       |\\\\\|
 * //       o-----+
 * //
 * //
 * //
 *
 * // Subtraction (set difference, new object is generated)
 * p1 = p2d.hypercube(1)
 * p2 = p2d.itranslate(p2d.hypercube(1), [1, 1])
 * p3 = p2d.sub(p1, p2)
 * p3 !== p1 // true
 * p3 !== p2 // true
 * p2d.volume(p3).toString() // '3'
 *
 * //
 * //
 * //
 * // p1
 * // +-----------+
 * // |\\\\\\\\\\\|
 * // |\\\\\\\\\\\|
 * // |\\\\\o\\\\\|
 * // |\\\\\\\\\\\|
 * // |\\\\\\\\\\\|
 * // +-----------+
 *
 * //       p2
 * //       +-----------+
 * //       |\\\\\\\\\\\|
 * //       |\\\\\\\\\\\|
 * //       |\\\\\\\\\\\|
 * //       |\\\\\\\\\\\|
 * //       |\\\\\\\\\\\|
 * //       o-----------+
 * //
 * //
 * //
 *
 * //
 * //
 * //
 * // p3
 * // +-----+
 * // |\\\\\|
 * // |\\\\\|
 * // |\\\\\o-----+
 * // |\\\\\\\\\\\|
 * // |\\\\\\\\\\\|
 * // +-----------+
 *
 * // Addition (union, new object is generated)
 * p1 = p2d.hypercube(1)
 * p2 = p2d.itranslate(p2d.hypercube(1), [1, 1])
 * p3 = p2d.add(p1, p2)
 * p3 !== p1 // true
 * p3 !== p2 // true
 * p2d.volume(p3).toString() // '7'
 *
 * //
 * //
 * //
 * // p1
 * // +-----------+
 * // |\\\\\\\\\\\|
 * // |\\\\\\\\\\\|
 * // |\\\\\o\\\\\|
 * // |\\\\\\\\\\\|
 * // |\\\\\\\\\\\|
 * // +-----------+
 *
 * //       p2
 * //       +-----------+
 * //       |\\\\\\\\\\\|
 * //       |\\\\\\\\\\\|
 * //       |\\\\\\\\\\\|
 * //       |\\\\\\\\\\\|
 * //       |\\\\\\\\\\\|
 * //       o-----------+
 * //
 * //
 * //
 *
 * //
 * //       +-----------+
 * //       |\\\\\\\\\\\|
 * // p3    |\\\\\\\\\\\|
 * // +-----+\\\\\\\\\\\|
 * // |\\\\\\\\\\\\\\\\\|
 * // |\\\\\\\\\\\\\\\\\|
 * // |\\\\\o\\\\\+-----+
 * // |\\\\\\\\\\\|
 * // |\\\\\\\\\\\|
 * // +-----------+
 *
 * // JSON (stringify and parse)
 * const str = JSON.stringify(p3)
 * const p4 = JSON.parse(str, p2d.reviver)
 * const p5 = p2d.mul(p3, p4)
 * p2d.volume(p4).toString() // '7'
 * p2d.volume(p5).toString() // '7'
 */
export class PolytopeAlgebra {
  /**
   * @desc
   * The constructor function of the {@link PolytopeAlgebra} class.
   *
   * @param {number} dim
   * the dimension to be treated.
   *
   * @param {LinearAlgebra} lalg
   * an instance of {@link LinearAlgebra}.
   *
   * @throws {Error}
   * if `lalg` is not an instance of `LinearAlgebra`.
   *
   * @throws {Error}
   * if `lalg` is not an instance of real algebra.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { PolytopeAlgebra } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   * const p3d = new PolytopeAlgebra(3, l)
   *
   * p3d instanceof PolytopeAlgebra // true
   *
   * new PolytopeAlgebra(3, null) // Error
   * new PolytopeAlgebra(null, l) // Error
   *
   * l.isReal = () => false
   * new PolytopeAlgebra(3, l) // Error
   */
  constructor (dim, lalg) {
    if (typeof dim !== 'number' || !Number.isInteger(dim) || dim < 0) {
      throw Error('`dim` must be an positive integer or zero')
    }
    if (!(lalg instanceof LinearAlgebra)) {
      throw Error('`lalg` must be an instance of `LinearAlgebra`.')
    }
    if (!lalg.isReal()) {
      throw Error('`lalg` must be an implementation of real algebra.')
    }
    /**
     * @desc
     * The PolytopeAlgebra#dim is the dimension
     * treated in `this` polytope algebra.
     *
     * @type {LinearAlgebra}
     *
     * @version 1.0.0
     * @since 1.0.0
     */
    this.dim = dim
    /**
     * @desc
     * The PolytopeAlgebra#lalg is used to manipurate vectors an matrices.
     *
     * @type {LinearAlgebra}
     *
     * @version 1.0.0
     * @since 1.0.0
     */
    this.lalg = lalg
    /**
     * @desc
     * The PolytopeAlgebra#nvecCache stores all the normal vectors.
     *
     * @type {Array}
     *
     * @version 1.0.0
     * @since 1.0.0
     */
    this.nvecCache = []
    /**
     * @desc
     * The PolytopeAlgebra#nvecCacheAbs2 stores
     * the square of the abolute value of normal vectors.
     *
     * @type {WeakMap}
     *
     * @version 1.0.0
     * @since 1.0.0
     */
    this.nvecCacheAbs2 = new WeakMap()
  }

  /**
   * @desc
   * The PolytopeAlgebra#copy method returns a new instance of
   * {@link Polytope}
   * which has copies of instances of {@link ConvexPolytope}
   * as generated by {@link ConvexPolytope#copy}.
   *
   * @param {Polytope} p
   * an instance of {@link Polytope}.
   *
   * @return {Polytope}
   * a new instance of {@link Polytope}.
   *
   * @throws {Error}
   * if `p` is not an instance of {@link Polytope}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { Polytope, PolytopeAlgebra } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   * const p2d = new PolytopeAlgebra(2, l)
   *
   * const p = p2d.hypercube(1)
   * p.length // 1
   * p[0].facets.length // 4
   * p[0].vertices.length // 4
   * p[0].edges.length // 4
   *
   * const p2 = p2d.copy(p)
   * p2 instanceof Polytope // true
   * p2 !== p // true
   * p2.length // 1
   * p2[0].facets.length // 4
   * p2[0].vertices.length // 4
   * p2[0].edges.length // 4
   *
   * const p3 = p2d.copy(new Polytope())
   * p3 instanceof Polytope // true
   * p3.isNull() // true
   *
   * p2d.copy(null) // Error
   * p[0].facets[0].nvec.push(1)
   */
  copy (p) {
    if (!(p instanceof Polytope)) {
      throw Error('`p` must be an instance of `Polytope`.')
    }
    return p.copy(this.lalg)
  }

  /**
   * @desc
   * The PolytopeAlgebra#facet method generates
   * a new instance of {@link Facet}.
   * If a normal vector parallel to the given `nvec` exists
   * in `this.nvecCache`,
   * then the `nvec` is replaced by the cheched one,
   * and the other parameters are automatically calculated.
   * If no normal vector parallel to `nvec` has not been cached,
   * then `nvec` is cached.
   *
   * @param {Matrix} nvec
   * a {@link Matrix} representing the normal vector of the new facet.
   *
   * @param {RealAlgebraicElement} d
   * a {@link RealAlgebraicElement} which is equal to
   * the dot product of `nvec` and a vertex on the new facet.
   *
   * @param {boolean} faceOutside
   * `nvec` is considered to face toward outside/inside
   * of the new facet if `faceOutside` is `true`/`false`.
   *
   * @return {Facet}
   * a new instance of {@link Facet}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { Facet, PolytopeAlgebra } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   * const p2d = new PolytopeAlgebra(5, l)
   *
   * const nvec = l.$(1, 1, 1, 1, 1)
   * let f = p2d.facet(nvec, 3, true)
   * f instanceof Facet // true
   * f.nvec === nvec // true
   * r.eq(f.d, 3) // true
   * f.faceOutside // true
   *
   * f = p2d.facet(nvec, 3, true)
   * f instanceof Facet // true
   * f.nvec === nvec // true
   * r.eq(f.d, 3) // true
   * f.faceOutside // true
   *
   * const nvec2 = l.$(2, 2, 2, 2, 2)
   * f = p2d.facet(nvec2, 3, true)
   * f instanceof Facet // true
   * f.nvec === nvec2 // false
   * f.nvec === nvec // true
   * r.ne(f.d, 3) // true
   * r.eq(f.d, r.$(3, 2)) // true
   * f.faceOutside // true
   *
   * const nvec3 = l.$(-2, -2, -2, -2, -2)
   * f = p2d.facet(nvec3, 3)
   * f instanceof Facet // true
   * f.nvec === nvec3// false
   * f.nvec === nvec // true
   * r.ne(f.d, 3) // true
   * r.eq(f.d, r.$(-3, 2)) // true
   * f.faceOutside // false
   *
   * p2d.facet([0, 0, 0, 0, 0], 1, true) // Error
   * p2d.facet([0, 0, 0, 0], 1, true) // Error
   */
  facet (nvec, d, faceOutside = true) {
    const lalg = this.lalg
    const ralg = lalg.salg
    const f = new Facet(lalg.cast(nvec), ralg.copy(ralg.cast(d)), faceOutside)
    if (f.dim !== this.dim) {
      throw Error('facet must be the same dimension as `this`.')
    }
    nvec = f.nvec
    if (!(this.nvecCache.includes(nvec))) {
      const lalg = this.lalg
      const ralg = lalg.salg
      // find a parallel normal vector
      const nvAbs2 = lalg.abs2(nvec)
      if (ralg.isZero(nvAbs2)) {
        throw Error('`nvec` must be a non-zero vector.')
      }
      let found = false
      for (let i = this.nvecCache.length - 1; i >= 0; i -= 1) {
        const nv2 = this.nvecCache[i]
        const nv2Abs2 = this.nvecCacheAbs2.get(nv2)
        const dot = lalg.dot(nvec, nv2)
        if (ralg.eq(ralg.mul(dot, dot), ralg.mul(nvAbs2, nv2Abs2))) {
          // a parallel normal vector already exist
          f.nvec = nv2
          f.d = ralg.imul(f.d, ralg.div(dot, nvAbs2))
          if (ralg.isNegative(dot)) {
            f.faceOutside = !f.faceOutside
          }
          found = true
          break
        }
      }
      if (!found) {
        // new normal vector
        this.nvecCache.push(nvec)
        this.nvecCacheAbs2.set(nvec, nvAbs2)
      }
    }
    return f
  }

  /**
   * @desc
   * The PolytopeAlgebra#hypercube method generates
   * an instance of {@link Polytope} representing
   * a hypercube of edge length `2 * d`.
   *
   * @param {RealAlgebraicElement} [d = Number.MAX_SAFE_INTEGER]
   * half the edge length.
   *
   * @return {Polytope}
   * a new instance of {@link Polytope} representing a hypercube.
   *
   * @version 1.1.0
   * @since 1.1.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { PolytopeAlgebra } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   * const p4d = new PolytopeAlgebra(4, l)
   *
   * let p = p4d.hypercube()
   * p.length // 1
   * p[0].facets.length // 8
   * p[0].vertices.length // 16
   * p[0].edges.length // 32
   *
   * p = p4d.hypercube(r.$(1, 2))
   * r.eq(p4d.volume(p), 1) // true
   *
   * p = p4d.hypercube(1)
   * r.eq(p4d.volume(p), 16) // true
   *
   * p = p4d.hypercube(r.$(0, 2))
   * r.eq(p4d.volume(p), 0) // true
   *
   * p = p4d.hypercube(r.$(-1, 2))
   * r.eq(p4d.volume(p), 0) // true
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { PolytopeAlgebra } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   * const p0d = new PolytopeAlgebra(0, l)
   *
   * // 0-dim test
   * const p = p0d.hypercube()
   * p.length // 1
   * p[0].facets.length // 0
   * p[0].vertices.length // 1
   * p[0].edges.length // 0
   * p[0].dim // 0
   */
  hypercube (d = Number.MAX_SAFE_INTEGER) {
    const lalg = this.lalg
    const ralg = lalg.salg
    const dim = this.dim
    if (dim === 0) {
      const cp = new ConvexPolytope([], [lalg.$()], [])
      return new Polytope(cp)
    }
    d = ralg.cast(d)
    if (ralg.isZero(d)) {
      return new Polytope()
    } else if (ralg.isNegative(d)) {
      return new Polytope()
    }
    const facets = []
    const vertices = []
    for (let i = 0; i < dim; i += 1) {
      const arr = []
      for (let j = 0; j < dim; j += 1) {
        if (j === i) {
          arr.push(1)
        } else {
          arr.push(0)
        }
      }
      const nvec = lalg.cast(arr)
      facets.push(this.facet(nvec, d, true))
      facets.push(this.facet(nvec, ralg.neg(d), false))
    }
    const arr = [-1]
    for (let i = 1; i < dim; i += 1) {
      arr.push(1)
    }
    let i = 0
    while (i < dim) {
      for (let j = i; j >= 0; j -= 1) {
        arr[j] *= -1
      }
      const vertex = lalg.ismul(lalg.cast(arr), d)
      vertices.push(vertex)
      for (let j = 0; j < dim; j += 1) {
        if (arr[j] === 1) {
          facets[2 * j].vertices.push(vertex)
        } else {
          facets[2 * j + 1].vertices.push(vertex)
        }
      }
      for (i = 0; i < dim; i += 1) {
        if (arr[i] === 1) {
          break
        }
      }
    }
    const edges = ConvexPolytope.genEdges(dim, facets)
    const cp = new ConvexPolytope(facets, vertices, edges)
    return new Polytope(cp)
  }

  /**
   * @desc
   * The PolytopeAlgebra#iaddFacet method adds a facet
   * to an instance of {@link Polytope} *in place*.
   * The result is the intersection of the polytopes and the facet.
   *
   * @param {Polytope} p
   * an instance of {@link Polytope}.
   *
   * @param {Facet} f
   * an instance of {@link Facet}.
   *
   * @return {Polytope}
   * `p`.
   *
   * @throws {Error}
   * if `p` is not an instance of {@link Polytope} of which
   * dimension is equal to `this.dim`.
   *
   * @throws {Error}
   * if `f` is not an instance of {@link Facet} of which
   * dimension is equal to `this.dim`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { Polytope, PolytopeAlgebra } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   * const p2d = new PolytopeAlgebra(2, l)
   *
   * let p = p2d.hypercube(1)
   * p.length // 1
   * p[0].facets.length // 4
   * p[0].vertices.length // 4
   * p[0].edges.length // 4
   *
   * const f = p2d.facet([1, 1], 0, true)
   * p = p2d.iaddFacet(p, f)
   * p.length // 1
   * p[0].facets.length // 3
   * p[0].vertices.length // 3
   * p[0].edges.length // 3
   *
   * let p2 = new Polytope()
   * p2 = p2d.iaddFacet(p2, f)
   * p2 instanceof Polytope // true
   * p2.isNull() // true
   *
   * let p3 = p2d.hypercube(1)
   * p3 = p2d.iaddFacet(p3, p2d.facet([0, 1], 1, true))
   * p3 = p2d.iaddFacet(p3, p2d.facet([0, 1], r.$(1, 2), true))
   * p3 = p2d.iaddFacet(p3, p2d.facet([0, 1], -1, true))
   * p3 instanceof Polytope // true
   * p3.isNull() // true
   *
   * p2d.iaddFacet(null, f) // Error
   * p2d.iaddFacet(p, null) // Error
   * p[0].vertices[0].push(1)
   * // invalid dimension of `p`
   * p2d.iaddFacet(p, f) // Error
   * p[0].vertices[0].pop()
   * // invalid dimension of `f`
   * f.nvec.push(1)
   * p2d.iaddFacet(p, f) // Error
   */
  iaddFacet (p, f) {
    const dim = this.dim
    const lalg = this.lalg
    const ralg = lalg.salg
    if (!(p instanceof Polytope)) {
      throw Error('`p` must be an instance of `Polytope`.')
    }
    if (p.isNull()) {
      return p
    }
    if (p.dim !== dim) {
      throw Error('the dimension of `p` must be the same as `this`.')
    }
    if (!(f instanceof Facet)) {
      throw Error('`f` must be an instance of `Facet`.')
    }
    if (f.dim !== dim) {
      throw Error('the dimension of `f` must be the same as `this`.')
    }
    for (let i = p.length - 1; i >= 0; i -= 1) {
      const cpi = p[i]
      // fast check
      for (let j = cpi.facets.length - 1; j >= 0; j -= 1) {
        const fj = cpi.facets[j]
        if (f.nvec === fj.nvec) {
          const dd = ralg.sub(f.d, fj.d)
          if (f.faceOutside === fj.faceOutside) {
            if (ralg.isZero(dd) || ralg.isPositive(dd) === f.faceOutside) {
              break
            }
          } else {
            if (ralg.isZero(dd) || ralg.isPositive(dd) !== f.faceOutside) {
              cpi.nullify()
              break
            }
          }
        }
        if (j === 0) {
          const newF = new Facet(f.nvec, ralg.copy(f.d), f.faceOutside)
          cpi.iaddFacet(newF, lalg)
        }
      }
      if (cpi.isNull()) {
        p.splice(i, 1)
      }
    }
    return p
  }

  /**
   * @desc
   * The PolytopeAlgebra#add method
   * calculates the union of two instances of {@link Polytope}
   * `p1` and `p2`.
   *
   * @param {Polytope} p1
   * an instance of {@link Polytope}.
   *
   * @param {Polytope} p2
   * another instance of {@link Polytope}.
   *
   * @return {Polytope}
   * a new instance of {@link Polytope}.
   *
   * @throws {Error}
   * if `p` is not an instance of {@link Polytope}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { Polytope, PolytopeAlgebra } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   * const p2d = new PolytopeAlgebra(2, l)
   * const p1d = new PolytopeAlgebra(1, l)
   *
   * const p = p2d.hypercube(1)
   * const p2 = p2d.translate(p, [1, 1])
   * const p3 = p2d.add(p, p2)
   * p3 instanceof Polytope // true
   * p3.length // 3
   * p3 !== p // true
   * p3 !== p2 // true
   *
   * const p4 = p2d.add(p2, p3)
   * p4 instanceof Polytope // true
   * p4.length // 3
   *
   * r.eq(p2d.volume(p), 4) // true
   *
   * r.eq(p2d.volume(p2), 4) // true
   *
   * r.eq(p2d.volume(p3), 7) // true
   *
   * const p5 = p2d.add(p4, new Polytope())
   * p5 instanceof Polytope // true
   * p5.length // 3
   *
   * const p6 = p2d.add(new Polytope(), p4)
   * p6 instanceof Polytope // true
   * p6.length // 3
   *
   * p2d.add(p1d.hypercube(1), p2) // Error
   * p2d.add(p2, p1d.hypercube(1)) // Error
   * p2d.add(null, p2) // Error
   * p2d.add(p2, null) // Error
   */
  add (p1, p2) {
    if (!(p1 instanceof Polytope)) {
      throw Error('`p` must be an instance of `Polytope`.')
    }
    if (!(p2 instanceof Polytope)) {
      throw Error('`p` must be an instance of `Polytope`.')
    }
    if (p2.isNull()) {
      return this.copy(p1)
    } else if (p1.isNull()) {
      return this.copy(p2)
    }
    const dim = this.dim
    if (p1.dim !== dim) {
      throw Error('the dimension of `p` must be the same as `this`.')
    }
    if (p2.dim !== dim) {
      throw Error('the dimension of `p` must be the same as `this`.')
    }
    let p3
    if (p1.length < p2.length) {
      p3 = this.copy(p1)
      p3.push(...this.sub(p2, p1))
    } else {
      p3 = this.copy(p2)
      p3.push(...this.sub(p1, p2))
    }
    p3.reduce(this.lalg)
    return p3
  }

  /**
   * @desc
   * The PolytopeAlgebra#sub method
   * calculates the set difference of two instances of {@link Polytope}
   * `p1` \ `p2`.
   *
   * @param {Polytope} p1
   * an instance of {@link Polytope}.
   *
   * @param {Polytope} p2
   * another instance of {@link Polytope}.
   *
   * @return {Polytope}
   * a new instance of {@link Polytope}.
   *
   * @throws {Error}
   * if `p` is not an instance of {@link Polytope}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { Polytope, PolytopeAlgebra } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   * const p2d = new PolytopeAlgebra(2, l)
   * const p1d = new PolytopeAlgebra(1, l)
   *
   * const p = p2d.hypercube(1)
   * const p2 = p2d.translate(p, [1, 1])
   * const p3 = p2d.sub(p, p2)
   * p3 instanceof Polytope // true
   * p3.length // 2
   * p3 !== p // true
   * p3 !== p2 // true
   *
   * r.eq(p2d.volume(p), 4) // true
   *
   * r.eq(p2d.volume(p2), 4) // true
   *
   * r.eq(p2d.volume(p3), 3) // true
   *
   * p3.nullify()
   * const p4 = p2d.sub(p3, p2)
   * p4 instanceof Polytope // true
   * p4.length // 0
   *
   * p2d.sub(p2, p1d.hypercube(1)) // Error
   * p2d.sub(p1d.hypercube(1), p2) // Error
   * p2d.sub(null, p2) // Error
   * p2d.sub(p2, null) // Error
   */
  sub (p1, p2) {
    if (!(p1 instanceof Polytope)) {
      throw Error('`p` must be an instance of `Polytope`.')
    }
    if (!(p2 instanceof Polytope)) {
      throw Error('`p` must be an instance of `Polytope`.')
    }
    if (p1.isNull() || p2.isNull()) {
      return this.copy(p1)
    }
    const dim = this.dim
    if (p1.dim !== dim) {
      throw Error('the dimension of `p` must be the same as `this`.')
    }
    if (p2.dim !== dim) {
      throw Error('the dimension of `p` must be the same as `this`.')
    }
    const lalg = this.lalg
    let p3 = this.copy(p1)
    for (let j = p2.length - 1; j >= 0; j -= 1) {
      const cpj = p2[j]
      const p4 = new Polytope()
      for (let i = p3.length - 1; i >= 0; i -= 1) {
        p4.push(...p3[i].imulSub(cpj, lalg))
      }
      p3 = p4
      p3.reduce(lalg)
    }
    return p3
  }

  /**
   * @desc
   * The PolytopeAlgebra#mul method
   * calculates the intersection of two instances of {@link Polytope}
   * `p1` and `p2`.
   *
   * @param {Polytope} p1
   * an instance of {@link Polytope}.
   *
   * @param {Polytope} p2
   * another instance of {@link Polytope}.
   *
   * @return {Polytope}
   * a new instance of {@link Polytope}.
   *
   * @throws {Error}
   * if `p` is not an instance of {@link Polytope}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { Polytope, PolytopeAlgebra } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   * const p2d = new PolytopeAlgebra(2, l)
   * const p1d = new PolytopeAlgebra(1, l)
   *
   * const p = p2d.hypercube(1)
   * const p2 = p2d.translate(p, [1, 1])
   * const p3 = p2d.mul(p, p2)
   * p3 instanceof Polytope // true
   * p3.length // 1
   * p3 !== p // true
   * p3 !== p2 // true
   *
   * r.eq(p2d.volume(p), 4) // true
   *
   * r.eq(p2d.volume(p2), 4) // true
   *
   * r.eq(p2d.volume(p3), 1) // true
   *
   * p3.nullify()
   * const p4 = p2d.mul(p3, p2)
   * p4 instanceof Polytope // true
   * p4.length // 0
   *
   * const p1 = p1d.hypercube(1)
   * p2d.mul(p1, p2) // Error
   * p2d.mul(p2, p1) // Error
   * p2d.mul(null, p2) // Error
   * p2d.mul(p2, null) // Error
   */
  mul (p1, p2) {
    if (!(p1 instanceof Polytope)) {
      throw Error('`p` must be an instance of `Polytope`.')
    }
    if (!(p2 instanceof Polytope)) {
      throw Error('`p` must be an instance of `Polytope`.')
    }
    if (p1.isNull() || p2.isNull()) {
      return new Polytope()
    }
    const dim = this.dim
    if (p1.dim !== dim) {
      throw Error('the dimension of `p` must be the same as `this`.')
    }
    if (p2.dim !== dim) {
      throw Error('the dimension of `p` must be the same as `this`.')
    }
    const lalg = this.lalg
    const p3 = new Polytope()
    for (let i = p1.length - 1; i >= 0; i -= 1) {
      const cpi = p1[i]
      for (let j = p2.length - 1; j >= 0; j -= 1) {
        const cp = p2[j].copy(lalg).imul(cpi, lalg)
        if (!cp.isNull()) {
          p3.push(cp)
        }
      }
    }
    p3.reduce(lalg)
    return p3
  }

  /**
   * @desc
   * The PolytopeAlgebra#rotate method
   * returns a copy of `p` rotated by translated by a rotation matrix `m`.
   * A rotation matrix is a orthogonal matrix,
   * i.e. its inverse is equal to its transpose.
   *
   * CAUTION: this method does not check if `m` is an orthogonal matrix.
   *
   * @param {Polytope} p
   * an instance of {@link Polytope}.
   *
   * @param {Matrix} m
   * an instance of {@link Matrix} representing an orthogonal matrix.
   *
   * @return {Polytope}
   * a new instance of {@link Polytope}.
   *
   * @version 1.1.0
   * @since 1.1.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { PolytopeAlgebra } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   * const p2d = new PolytopeAlgebra(2, l)
   *
   * const p = p2d.hypercube(1)
   * const p2 = p2d.rotate(p, l.$(0, -1, 1, 0).setDim(2))
   * p !== p2 // true
   * p2.length // 1
   * l.eq(p2[0].facets[0].nvec, l.$(0, 1)) // true
   * l.eq(p2[0].facets[1].nvec, l.$(0, 1)) // true
   * l.eq(p2[0].facets[2].nvec, l.$(1, 0)) // true
   * l.eq(p2[0].facets[3].nvec, l.$(1, 0)) // true
   * p2[0].facets[0].nvec === p[0].facets[2].nvec // true
   * p2[0].facets[1].nvec === p[0].facets[2].nvec // true
   * p2[0].facets[2].nvec === p[0].facets[0].nvec // true
   * p2[0].facets[3].nvec === p[0].facets[0].nvec // true
   * r.eq(p2[0].facets[0].d, 1) // true
   * r.eq(p2[0].facets[1].d, -1) // true
   * r.eq(p2[0].facets[2].d, -1) // true
   * r.eq(p2[0].facets[3].d, 1) // true
   * l.eq(p2[0].vertices[0], [-1, 1]) // true
   * l.eq(p2[0].vertices[1], [-1, -1]) // true
   * l.eq(p2[0].vertices[2], [1, 1]) // true
   * l.eq(p2[0].vertices[3], [1, -1]) // true
   */
  rotate (p, m) {
    return this.irotate(this.copy(p), m)
  }

  /**
   * @desc
   * The PolytopeAlgebra#irotate method rotates an instance of
   * {@link Polytope} `p` by a rotation matrix `m` *in place*.
   * A rotation matrix is a orthogonal matrix,
   * i.e. its inverse is equal to its transpose.
   *
   * CAUTION: this method does not check if `m` is an orthogonal matrix.
   *
   * @param {Polytope} p
   * an instance of {@link Polytope}.
   *
   * @param {Matrix} m
   * an instance of {@link Matrix} representing an orthogonal matrix.
   *
   * @return {Polytope}
   * `p`.
   *
   * @throws {Error}
   * if `p` is not an instance of {@link Polytope}.
   *
   * @version 1.1.0
   * @since 1.1.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { PolytopeAlgebra } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   * const p2d = new PolytopeAlgebra(2, l)
   *
   * const p = p2d.hypercube(1)
   * const p2 = p2d.irotate(p, l.$(0, -1, 1, 0).setDim(2))
   * p === p2 // true
   * p.length // 1
   * l.eq(p[0].facets[0].nvec, l.$(0, 1)) // true
   * l.eq(p[0].facets[1].nvec, l.$(0, 1)) // true
   * l.eq(p[0].facets[2].nvec, l.$(1, 0)) // true
   * l.eq(p[0].facets[3].nvec, l.$(1, 0)) // true
   * r.eq(p[0].facets[0].d, 1) // true
   * r.eq(p[0].facets[1].d, -1) // true
   * r.eq(p[0].facets[2].d, -1) // true
   * r.eq(p[0].facets[3].d, 1) // true
   * l.eq(p[0].vertices[0], [-1, 1]) // true
   * l.eq(p[0].vertices[1], [-1, -1]) // true
   * l.eq(p[0].vertices[2], [1, 1]) // true
   * l.eq(p[0].vertices[3], [1, -1]) // true
   *
   * p2d.irotate(null, l.cast([0, -1, 1, 0]).setDim(2)) // Error
   */
  irotate (p, m) {
    if (!(p instanceof Polytope)) {
      throw Error('`p` must be an instance of `Polytope`.')
    }
    const lalg = this.lalg
    for (let i = p.length - 1; i >= 0; i -= 1) {
      p[i].irotate(m, lalg)
      for (let j = p[i].facets.length - 1; j >= 0; j -= 1) {
        const fj = p[i].facets[j]
        p[i].facets[j] = this.facet(fj.nvec, fj.d, fj.faceOutside)
        p[i].facets[j].vertices = fj.vertices
      }
    }
    return p
  }

  /**
   * @desc
   * The PolytopeAlgebra#translate method
   * returns a copy of `p` translated by `v`.
   *
   * @param {Polytope} p
   * an instance of {@link Polytope}.
   *
   * @param {Matrix} v
   * an instance of {@link Matrix} representing a vector.
   *
   * @return {Polytope}
   * a new instance of {@link Polytope}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { PolytopeAlgebra } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   * const p2d = new PolytopeAlgebra(2, l)
   *
   * const p = p2d.hypercube(1)
   * const p2 = p2d.translate(p, [1, 1])
   * p !== p2 // true
   * p.length // 1
   * p2.length // 1
   * r.eq(p[0].facets[0].d, 1) // true
   * r.eq(p[0].facets[1].d, -1) // true
   * r.eq(p[0].facets[2].d, 1) // true
   * r.eq(p[0].facets[3].d, -1) // true
   * r.eq(p2[0].facets[0].d, 2) // true
   * r.eq(p2[0].facets[1].d, 0) // true
   * r.eq(p2[0].facets[2].d, 2) // true
   * r.eq(p2[0].facets[3].d, 0) // true
   * l.eq(p[0].vertices[0], [1, 1]) // true
   * l.eq(p[0].vertices[1], [-1, 1]) // true
   * l.eq(p[0].vertices[2], [1, -1]) // true
   * l.eq(p[0].vertices[3], [-1, -1]) // true
   * l.eq(p2[0].vertices[0], [2, 2]) // true
   * l.eq(p2[0].vertices[1], [0, 2]) // true
   * l.eq(p2[0].vertices[2], [2, 0]) // true
   * l.eq(p2[0].vertices[3], [0, 0]) // true
   *
   * p2d.translate(null, [1, 1]) // Error
   */
  translate (p, v) {
    return this.itranslate(this.copy(p), v)
  }

  /**
   * @desc
   * The PolytopeAlgebra#itranslate method
   * translates an instance of {@link Polytope} `p` by `v` *in place*.
   *
   * @param {Polytope} p
   * an instance of {@link Polytope}.
   *
   * @param {Matrix} v
   * an instance of {@link Matrix} representing a vector.
   *
   * @return {Polytope}
   * `p`.
   *
   * @throws {Error}
   * if `p` is not an instance of {@link Polytope}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { PolytopeAlgebra } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   * const p2d = new PolytopeAlgebra(2, l)
   *
   * const p = p2d.hypercube(1)
   * const p2 = p2d.itranslate(p, [1, 1])
   * p === p2 // true
   * p.length // 1
   * r.eq(p[0].facets[0].d, 2) // true
   * r.eq(p[0].facets[1].d, 0) // true
   * r.eq(p[0].facets[2].d, 2) // true
   * r.eq(p[0].facets[3].d, 0) // true
   * l.eq(p[0].vertices[0], [2, 2]) // true
   * l.eq(p[0].vertices[1], [0, 2]) // true
   * l.eq(p[0].vertices[2], [2, 0]) // true
   * l.eq(p[0].vertices[3], [0, 0]) // true
   *
   * p2d.itranslate(null, [1, 1]) // Error
   */
  itranslate (p, v) {
    if (!(p instanceof Polytope)) {
      throw Error('`p` must be an instance of `Polytope`.')
    }
    const lalg = this.lalg
    for (let i = p.length - 1; i >= 0; i -= 1) {
      p[i].itranslate(v, lalg)
    }
    return p
  }

  /**
   * @desc
   * The PolytopeAlgebra#scale method returns a copy of `p`
   * scaled by `s` around the origin.
   *
   * @param {Polytope} p
   * an instance of {@link Polytope}.
   *
   * @param {RealAlgebraicElement} s
   * a {@link RealAlgebraicElement} representing a scale factor.
   *
   * @return {Polytope}
   * a new instance of {@link Polytope}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { PolytopeAlgebra } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   * const p2d = new PolytopeAlgebra(2, l)
   *
   * const p = p2d.hypercube(1)
   * const p2 = p2d.scale(p, 2)
   * p === p2 // false
   * p.length // 1
   * p2.length // 1
   * r.eq(p[0].facets[0].d, 1) // true
   * r.eq(p[0].facets[1].d, -1) // true
   * r.eq(p[0].facets[2].d, 1) // true
   * r.eq(p[0].facets[3].d, -1) // true
   * r.eq(p2[0].facets[0].d, 2) // true
   * r.eq(p2[0].facets[1].d, -2) // true
   * r.eq(p2[0].facets[2].d, 2) // true
   * r.eq(p2[0].facets[3].d, -2) // true
   * l.eq(p[0].vertices[0], [1, 1]) // true
   * l.eq(p[0].vertices[1], [-1, 1]) // true
   * l.eq(p[0].vertices[2], [1, -1]) // true
   * l.eq(p[0].vertices[3], [-1, -1]) // true
   * l.eq(p2[0].vertices[0], [2, 2]) // true
   * l.eq(p2[0].vertices[1], [-2, 2]) // true
   * l.eq(p2[0].vertices[2], [2, -2]) // true
   * l.eq(p2[0].vertices[3], [-2, -2]) // true
   *
   * p2d.scale(null, 2) // Error
   */
  scale (p, s) {
    return this.iscale(this.copy(p), s)
  }

  /**
   * @desc
   * The PolytopeAlgebra#iscale method scales an instance of
   * {@link Polytope} `p` by `s` *in place* around the origin.
   *
   * @param {Polytope} p
   * an instance of {@link Polytope}.
   *
   * @param {RealAlgebraicElement} s
   * a {@link RealAlgebraicElement} representing a scale factor.
   *
   * @return {Polytope}
   * `p`.
   *
   * @throws {Error}
   * if `p` is not an instance of {@link Polytope}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { PolytopeAlgebra } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   * const p2d = new PolytopeAlgebra(2, l)
   *
   * const p = p2d.hypercube(1)
   * const p2 = p2d.iscale(p, 2)
   * p === p2 // true
   * p.length // 1
   * r.eq(p[0].facets[0].d, 2) // true
   * r.eq(p[0].facets[1].d, -2) // true
   * r.eq(p[0].facets[2].d, 2) // true
   * r.eq(p[0].facets[3].d, -2) // true
   * l.eq(p[0].vertices[0], [2, 2]) // true
   * l.eq(p[0].vertices[1], [-2, 2]) // true
   * l.eq(p[0].vertices[2], [2, -2]) // true
   * l.eq(p[0].vertices[3], [-2, -2]) // true
   *
   * p2d.iscale(null, 2) // Error
   */
  iscale (p, s) {
    if (!(p instanceof Polytope)) {
      throw Error('`p` must be an instance of `Polytope`.')
    }
    const lalg = this.lalg
    for (let i = p.length - 1; i >= 0; i -= 1) {
      p[i].iscale(s, lalg)
    }
    return p
  }

  /**
   * @desc
   * The PolytopeAlgebra#genSimplexes method
   * generates simplexes from  an instance of {@link Polytope}.
   * Each simplex is an {@link Array} containing the vertices of the simplex.
   *
   * @param {Polytope} p
   * an instance of {@link Polytope}.
   *
   * @return {Array}
   * an {@link Array} containing the generated simplexes.
   *
   * @throws {Error}
   * if `p` is not an instance of {@link Polytope}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { Polytope, PolytopeAlgebra } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   * const p3d = new PolytopeAlgebra(3, l)
   *
   * const p = p3d.hypercube(1)
   * let simplexes = p3d.genSimplexes(p)
   * simplexes instanceof Array // true
   * simplexes.length // 6
   * simplexes[0].length // 4
   *
   * simplexes = p3d.genSimplexes(new Polytope())
   * simplexes instanceof Array // true
   * simplexes.length // 0
   *
   * // invalid type
   * p3d.genSimplexes(null) // Error
   * p[0].vertices[0].push(1)
   * p.dim // 4
   * // inconsistent dimension
   * p3d.genSimplexes(p) // Error
   */
  genSimplexes (p) {
    if (!(p instanceof Polytope)) {
      throw Error('`p` must be an instance of `Polytope`.')
    }
    return p.genSimplexes()
  }

  /**
   * @desc
   * The PolytopeAlgebra#volume function calculates the volume of simplexes.
   *
   * @param {Polytope} p
   * an instance of {@link Polytope}.
   *
   * @return {RealAlgebraicElement}
   * the volume of the given simplexes.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { PolytopeAlgebra } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   * const p2d = new PolytopeAlgebra(2, l)
   *
   * const p = p2d.hypercube(1)
   * r.eq(p2d.volume(p), 4) // true
   */
  volume (p) {
    return Polytope.volume(this.genSimplexes(p), this.lalg)
  }

  /**
   * @desc
   * The reviver function for the {@link Polytope}.
   *
   * @type {Function}
   *
   * @version 1.1.4
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import { Polytope, PolytopeAlgebra } from '@kkitahara/polytope-algebra'
   * const r = new RealAlgebra()
   * const l = new LinearAlgebra(r)
   * const p2d = new PolytopeAlgebra(2, l)
   *
   * const p = p2d.hypercube(1)
   * const p2 = p2d.translate(p, [1, 1])
   * const p3 = p2d.add(p, p2)
   *
   * const s = JSON.stringify(p3)
   * typeof s // 'string'
   *
   * const p4 = JSON.parse(s, p2d.reviver)
   * p4 instanceof Polytope // true
   * r.eq(p2d.volume(p4), 7) // true
   *
   * const s2 = s.replace('1.1.4', '0.0.0')
   * JSON.parse(s2, p2d.reviver) // Error
   */
  get reviver () {
    return (key, value) => {
      const lalg = this.lalg
      if (value !== null && typeof value === 'object' &&
          value.reviver === 'Polytope') {
        if (value.version === '1.1.4') {
          const p = new Polytope()
          const nvecs = value.nvecs
          for (let i = value.convexPolytopes.length - 1; i >= 0; i -= 1) {
            const cpData = value.convexPolytopes[i]
            const vertices = cpData.vertices
            const facets = []
            const edges = []
            for (let j = cpData.facets.length - 1; j >= 0; j -= 1) {
              const fData = cpData.facets[j]
              const f = this.facet(nvecs[fData[0]], fData[1], fData[2])
              const fvID = fData[3]
              for (let k = fvID.length - 1; k >= 0; k -= 1) {
                f.vertices.push(vertices[fvID[k]])
              }
              facets.push(f)
            }
            for (let j = cpData.edges.length - 1; j >= 0; j -= 1) {
              const eData = cpData.edges[j]
              edges.push(new Edge(vertices[eData[0]], vertices[eData[1]]))
            }
            p.push(new ConvexPolytope(facets, vertices, edges, cpData.weight))
          }
          return p
        } else {
          throw Error('invalid version.')
        }
      } else {
        value = lalg.reviver(key, value)
        return value
      }
    }
  }
}

/* @license-end */
