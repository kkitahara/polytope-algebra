import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { AnotherPolytopeAlgebra, Polytope }
  from '../../src/index.mjs'
const r = new RealAlgebra()
const l = new LinearAlgebra(r)
const p2d = new AnotherPolytopeAlgebra(2, l)
const p1d = new AnotherPolytopeAlgebra(1, l)

const p = p2d.hypercube(1)
const p2 = p2d.translate(p, [1, 1])
const p3 = p2d.add(p, p2)
testDriver.test(() => { return p3 instanceof Polytope }, true, 'src/another-polytope-algebra.mjs~AnotherPolytopeAlgebra#add-example0_0', false)
testDriver.test(() => { return p3.length }, 3, 'src/another-polytope-algebra.mjs~AnotherPolytopeAlgebra#add-example0_1', false)
testDriver.test(() => { return p3 !== p }, true, 'src/another-polytope-algebra.mjs~AnotherPolytopeAlgebra#add-example0_2', false)
testDriver.test(() => { return p3 !== p2 }, true, 'src/another-polytope-algebra.mjs~AnotherPolytopeAlgebra#add-example0_3', false)

testDriver.test(() => { return r.eq(p2d.volume(p), 4) }, true, 'src/another-polytope-algebra.mjs~AnotherPolytopeAlgebra#add-example0_4', false)

testDriver.test(() => { return r.eq(p2d.volume(p2), 4) }, true, 'src/another-polytope-algebra.mjs~AnotherPolytopeAlgebra#add-example0_5', false)

testDriver.test(() => { return r.eq(p2d.volume(p3), 7) }, true, 'src/another-polytope-algebra.mjs~AnotherPolytopeAlgebra#add-example0_6', false)

p.nullify()
const p4 = p2d.add(p3, p)
testDriver.test(() => { return p4 instanceof Polytope }, true, 'src/another-polytope-algebra.mjs~AnotherPolytopeAlgebra#add-example0_7', false)
testDriver.test(() => { return p4.length }, 3, 'src/another-polytope-algebra.mjs~AnotherPolytopeAlgebra#add-example0_8', false)

p.nullify()
const p5 = p2d.add(p, p3)
testDriver.test(() => { return p5 instanceof Polytope }, true, 'src/another-polytope-algebra.mjs~AnotherPolytopeAlgebra#add-example0_9', false)
testDriver.test(() => { return p5.length }, 3, 'src/another-polytope-algebra.mjs~AnotherPolytopeAlgebra#add-example0_10', false)

testDriver.test(() => { return p2d.add(p2, p1d.hypercube(1)) }, Error, 'src/another-polytope-algebra.mjs~AnotherPolytopeAlgebra#add-example0_11', false)
testDriver.test(() => { return p2d.add(p1d.hypercube(1), p2) }, Error, 'src/another-polytope-algebra.mjs~AnotherPolytopeAlgebra#add-example0_12', false)
testDriver.test(() => { return p2d.add(p2, null) }, Error, 'src/another-polytope-algebra.mjs~AnotherPolytopeAlgebra#add-example0_13', false)
testDriver.test(() => { return p2d.add(null, p2) }, Error, 'src/another-polytope-algebra.mjs~AnotherPolytopeAlgebra#add-example0_14', false)
