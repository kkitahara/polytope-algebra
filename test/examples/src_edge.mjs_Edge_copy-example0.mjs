import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { Edge } from '../../src/index.mjs'
const r = new RealAlgebra()
const l = new LinearAlgebra(r)

const m1 = l.$(1, 1, 1)
const m2 = l.$(2, 1, 1)
const e = new Edge(m1, m2)
const e2 = e.copy()

testDriver.test(() => { return e2 instanceof Edge }, true, 'src/edge.mjs~Edge#copy-example0_0', false)
testDriver.test(() => { return e2.v0 === m1 }, true, 'src/edge.mjs~Edge#copy-example0_1', false)
testDriver.test(() => { return e2.v1 === m2 }, true, 'src/edge.mjs~Edge#copy-example0_2', false)
