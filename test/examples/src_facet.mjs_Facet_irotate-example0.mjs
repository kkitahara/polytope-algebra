import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { Facet } from '../../src/index.mjs'
const r = new RealAlgebra()
const l = new LinearAlgebra(r)

const m = l.$(1, 1)
const d = 1
const f = new Facet(m, d, true)
const f2 = f.irotate(l.$(0, 1, -1, 0).setDim(2), l)
testDriver.test(() => { return f2 instanceof Facet }, true, 'src/facet.mjs~Facet#irotate-example0_0', false)
testDriver.test(() => { return f2 === f }, true, 'src/facet.mjs~Facet#irotate-example0_1', false)
testDriver.test(() => { return f2.nvec === m }, false, 'src/facet.mjs~Facet#irotate-example0_2', false)
testDriver.test(() => { return l.eq(f2.nvec, l.$(1, -1)) }, true, 'src/facet.mjs~Facet#irotate-example0_3', false)
testDriver.test(() => { return f2.d }, 1, 'src/facet.mjs~Facet#irotate-example0_4', false)

testDriver.test(() => { return f.irotate(l.$(0, 1, -1, 0), l) }, Error, 'src/facet.mjs~Facet#irotate-example0_5', false)
testDriver.test(() => { return f.irotate(l.$(0, 1, -1, 0).setDim(2, 2), null) }, Error, 'src/facet.mjs~Facet#irotate-example0_6', false)
testDriver.test(() => { return f.irotate(null, l) }, Error, 'src/facet.mjs~Facet#irotate-example0_7', false)
testDriver.test(() => { return f.irotate(l.$(0, 1, -1, 0).setDim(1, 4), l) }, Error, 'src/facet.mjs~Facet#irotate-example0_8', false)
testDriver.test(() => { return f.irotate(l.$(1).setDim(1), l) }, Error, 'src/facet.mjs~Facet#irotate-example0_9', false)
