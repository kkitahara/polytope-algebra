import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { PolytopeAlgebra } from '../../src/index.mjs'
const r = new RealAlgebra(1e-5)
const l = new LinearAlgebra(r)
const p3d = new PolytopeAlgebra(3, l)

const p = p3d.hypercube(1)
let p2 = p3d.hypercube(1)
p2 = p3d.iaddFacet(p2, p3d.facet([-1, -1, -1], 1, true))
p2 = p3d.itranslate(p2, [0, 0, 2])
p.push(p2[0])
p.reduce(l)
testDriver.test(() => { return p.length }, 2, 'src/polytope.mjs~Polytope#reduce-example2_0', false)

p[1].nullify()
p.reduce(l)
testDriver.test(() => { return p.length }, 1, 'src/polytope.mjs~Polytope#reduce-example2_1', false)
