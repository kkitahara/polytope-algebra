import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { PolytopeAlgebra } from '../../src/index.mjs'
const r = new RealAlgebra(1e-5)
const l = new LinearAlgebra(r)
const p2d = new PolytopeAlgebra(2, l)

const p = p2d.hypercube(1)
p[0].nullify()

const cp = p[0].iaddFacet(p2d.facet([0, 1], 1), l)
testDriver.test(() => { return cp.isNull() }, true, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example1_0', false)

l.isReal = () => false
testDriver.test(() => { return p[0].iaddFacet(p2d.facet([0, 1], 1), l) }, Error, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example1_1', false)
