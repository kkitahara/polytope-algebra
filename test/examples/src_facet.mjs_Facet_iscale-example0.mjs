import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { Facet } from '../../src/index.mjs'
const r = new RealAlgebra()
const l = new LinearAlgebra(r)

const m = l.$(1, 1)
const d = 1
const f = new Facet(m, d, true)
const f2 = f.iscale(2, l)
testDriver.test(() => { return f2 instanceof Facet }, true, 'src/facet.mjs~Facet#iscale-example0_0', false)
testDriver.test(() => { return f2 === f }, true, 'src/facet.mjs~Facet#iscale-example0_1', false)
testDriver.test(() => { return f2.nvec === m }, true, 'src/facet.mjs~Facet#iscale-example0_2', false)
testDriver.test(() => { return f2.d }, 2, 'src/facet.mjs~Facet#iscale-example0_3', false)

testDriver.test(() => { return f.iscale(2, null) }, Error, 'src/facet.mjs~Facet#iscale-example0_4', false)
