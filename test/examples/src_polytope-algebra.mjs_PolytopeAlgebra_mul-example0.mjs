import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { Polytope, PolytopeAlgebra } from '../../src/index.mjs'
const r = new RealAlgebra()
const l = new LinearAlgebra(r)
const p2d = new PolytopeAlgebra(2, l)
const p1d = new PolytopeAlgebra(1, l)

const p = p2d.hypercube(1)
const p2 = p2d.translate(p, [1, 1])
const p3 = p2d.mul(p, p2)
testDriver.test(() => { return p3 instanceof Polytope }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#mul-example0_0', false)
testDriver.test(() => { return p3.length }, 1, 'src/polytope-algebra.mjs~PolytopeAlgebra#mul-example0_1', false)
testDriver.test(() => { return p3 !== p }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#mul-example0_2', false)
testDriver.test(() => { return p3 !== p2 }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#mul-example0_3', false)

testDriver.test(() => { return r.eq(p2d.volume(p), 4) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#mul-example0_4', false)

testDriver.test(() => { return r.eq(p2d.volume(p2), 4) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#mul-example0_5', false)

testDriver.test(() => { return r.eq(p2d.volume(p3), 1) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#mul-example0_6', false)

p3.nullify()
const p4 = p2d.mul(p3, p2)
testDriver.test(() => { return p4 instanceof Polytope }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#mul-example0_7', false)
testDriver.test(() => { return p4.length }, 0, 'src/polytope-algebra.mjs~PolytopeAlgebra#mul-example0_8', false)

const p1 = p1d.hypercube(1)
testDriver.test(() => { return p2d.mul(p1, p2) }, Error, 'src/polytope-algebra.mjs~PolytopeAlgebra#mul-example0_9', false)
testDriver.test(() => { return p2d.mul(p2, p1) }, Error, 'src/polytope-algebra.mjs~PolytopeAlgebra#mul-example0_10', false)
testDriver.test(() => { return p2d.mul(null, p2) }, Error, 'src/polytope-algebra.mjs~PolytopeAlgebra#mul-example0_11', false)
testDriver.test(() => { return p2d.mul(p2, null) }, Error, 'src/polytope-algebra.mjs~PolytopeAlgebra#mul-example0_12', false)
