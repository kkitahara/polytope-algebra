import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { Facet, ConvexPolytope } from '../../src/index.mjs'
const r = new RealAlgebra()
const l = new LinearAlgebra(r)

const x = l.$(1, 0)
const y = l.$(0, 1)
let facets = [
  new Facet(x, 1, true),
  new Facet(x, -1, false),
  new Facet(y, 1, true),
  new Facet(y, -1, false)]
let vertices = [
  l.$(1, 1),
  l.$(-1, 1),
  l.$(1, -1),
  l.$(-1, -1)]
facets[0].vertices = [vertices[0], vertices[2]]
facets[1].vertices = [vertices[1], vertices[3]]
facets[2].vertices = [vertices[0], vertices[1]]
facets[3].vertices = [vertices[2], vertices[3]]
let edges = ConvexPolytope.genEdges(2, facets)
const cp = new ConvexPolytope(facets, vertices, edges)

facets = [
  new Facet(x, 2, true),
  new Facet(x, 0, false),
  new Facet(y, 2, true),
  new Facet(y, 0, false)]
vertices = [
  l.$(2, 2),
  l.$(0, 2),
  l.$(2, 0),
  l.$(0, 0)]
facets[0].vertices = [vertices[0], vertices[2]]
facets[1].vertices = [vertices[1], vertices[3]]
facets[2].vertices = [vertices[0], vertices[1]]
facets[3].vertices = [vertices[2], vertices[3]]
edges = ConvexPolytope.genEdges(2, facets)
const cp2 = new ConvexPolytope(facets, vertices, edges)

const cp3 = cp.imul(cp2, l)
testDriver.test(() => { return cp3.facets.length }, 4, 'src/convex-polytope.mjs~ConvexPolytope#imul-example0_0', false)
testDriver.test(() => { return cp3.vertices.length }, 4, 'src/convex-polytope.mjs~ConvexPolytope#imul-example0_1', false)
testDriver.test(() => { return cp3.edges.length }, 4, 'src/convex-polytope.mjs~ConvexPolytope#imul-example0_2', false)

testDriver.test(() => { return cp.imul(cp2, null) }, Error, 'src/convex-polytope.mjs~ConvexPolytope#imul-example0_3', false)
testDriver.test(() => { return cp.imul(null, l) }, Error, 'src/convex-polytope.mjs~ConvexPolytope#imul-example0_4', false)
cp2.vertices[0] = l.$(1, 1, 1)
testDriver.test(() => { return cp.imul(cp2, l) }, Error, 'src/convex-polytope.mjs~ConvexPolytope#imul-example0_5', false)

cp2.nullify()
const cp4 = cp.imul(cp2, l)
testDriver.test(() => { return cp4.facets.length }, 0, 'src/convex-polytope.mjs~ConvexPolytope#imul-example0_6', false)
testDriver.test(() => { return cp4.vertices.length }, 0, 'src/convex-polytope.mjs~ConvexPolytope#imul-example0_7', false)
testDriver.test(() => { return cp4.edges.length }, 0, 'src/convex-polytope.mjs~ConvexPolytope#imul-example0_8', false)
