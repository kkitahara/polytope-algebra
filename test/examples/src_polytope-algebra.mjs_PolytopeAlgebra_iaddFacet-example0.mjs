import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { Polytope, PolytopeAlgebra } from '../../src/index.mjs'
const r = new RealAlgebra()
const l = new LinearAlgebra(r)
const p2d = new PolytopeAlgebra(2, l)

let p = p2d.hypercube(1)
testDriver.test(() => { return p.length }, 1, 'src/polytope-algebra.mjs~PolytopeAlgebra#iaddFacet-example0_0', false)
testDriver.test(() => { return p[0].facets.length }, 4, 'src/polytope-algebra.mjs~PolytopeAlgebra#iaddFacet-example0_1', false)
testDriver.test(() => { return p[0].vertices.length }, 4, 'src/polytope-algebra.mjs~PolytopeAlgebra#iaddFacet-example0_2', false)
testDriver.test(() => { return p[0].edges.length }, 4, 'src/polytope-algebra.mjs~PolytopeAlgebra#iaddFacet-example0_3', false)

const f = p2d.facet([1, 1], 0, true)
p = p2d.iaddFacet(p, f)
testDriver.test(() => { return p.length }, 1, 'src/polytope-algebra.mjs~PolytopeAlgebra#iaddFacet-example0_4', false)
testDriver.test(() => { return p[0].facets.length }, 3, 'src/polytope-algebra.mjs~PolytopeAlgebra#iaddFacet-example0_5', false)
testDriver.test(() => { return p[0].vertices.length }, 3, 'src/polytope-algebra.mjs~PolytopeAlgebra#iaddFacet-example0_6', false)
testDriver.test(() => { return p[0].edges.length }, 3, 'src/polytope-algebra.mjs~PolytopeAlgebra#iaddFacet-example0_7', false)

let p2 = new Polytope()
p2 = p2d.iaddFacet(p2, f)
testDriver.test(() => { return p2 instanceof Polytope }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#iaddFacet-example0_8', false)
testDriver.test(() => { return p2.isNull() }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#iaddFacet-example0_9', false)

let p3 = p2d.hypercube(1)
p3 = p2d.iaddFacet(p3, p2d.facet([0, 1], 1, true))
p3 = p2d.iaddFacet(p3, p2d.facet([0, 1], r.$(1, 2), true))
p3 = p2d.iaddFacet(p3, p2d.facet([0, 1], -1, true))
testDriver.test(() => { return p3 instanceof Polytope }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#iaddFacet-example0_10', false)
testDriver.test(() => { return p3.isNull() }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#iaddFacet-example0_11', false)

testDriver.test(() => { return p2d.iaddFacet(null, f) }, Error, 'src/polytope-algebra.mjs~PolytopeAlgebra#iaddFacet-example0_12', false)
testDriver.test(() => { return p2d.iaddFacet(p, null) }, Error, 'src/polytope-algebra.mjs~PolytopeAlgebra#iaddFacet-example0_13', false)
p[0].vertices[0].push(1)
// invalid dimension of `p`
testDriver.test(() => { return p2d.iaddFacet(p, f) }, Error, 'src/polytope-algebra.mjs~PolytopeAlgebra#iaddFacet-example0_14', false)
p[0].vertices[0].pop()
// invalid dimension of `f`
f.nvec.push(1)
testDriver.test(() => { return p2d.iaddFacet(p, f) }, Error, 'src/polytope-algebra.mjs~PolytopeAlgebra#iaddFacet-example0_15', false)
