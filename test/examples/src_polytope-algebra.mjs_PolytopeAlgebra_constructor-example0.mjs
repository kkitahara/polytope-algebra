import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { PolytopeAlgebra } from '../../src/index.mjs'
const r = new RealAlgebra()
const l = new LinearAlgebra(r)
const p3d = new PolytopeAlgebra(3, l)

testDriver.test(() => { return p3d instanceof PolytopeAlgebra }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#constructor-example0_0', false)

testDriver.test(() => { return new PolytopeAlgebra(3, null) }, Error, 'src/polytope-algebra.mjs~PolytopeAlgebra#constructor-example0_1', false)
testDriver.test(() => { return new PolytopeAlgebra(null, l) }, Error, 'src/polytope-algebra.mjs~PolytopeAlgebra#constructor-example0_2', false)

l.isReal = () => false
testDriver.test(() => { return new PolytopeAlgebra(3, l) }, Error, 'src/polytope-algebra.mjs~PolytopeAlgebra#constructor-example0_3', false)
