import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { Facet, ConvexPolytope } from '../../src/index.mjs'
const r = new RealAlgebra()
const l = new LinearAlgebra(r)

const x = l.$(1, 0)
const y = l.$(0, 1)
let facets = [
  new Facet(x, 1, true),
  new Facet(x, -1, false),
  new Facet(y, 1, true),
  new Facet(y, -1, false)]
const vertices = [
  l.$(1, 1),
  l.$(-1, 1),
  l.$(1, -1),
  l.$(-1, -1)]
facets[0].vertices = [vertices[0], vertices[2]]
facets[1].vertices = [vertices[1], vertices[3]]
facets[2].vertices = [vertices[0], vertices[1]]
facets[3].vertices = [vertices[2], vertices[3]]
let simplexes = ConvexPolytope.genSimplexes(2, facets)
testDriver.test(() => { return simplexes instanceof Array }, true, 'src/convex-polytope.mjs~ConvexPolytope.genSimplexes-example0_0', false)
testDriver.test(() => { return simplexes.length }, 2, 'src/convex-polytope.mjs~ConvexPolytope.genSimplexes-example0_1', false)
testDriver.test(() => { return simplexes[0].length }, 3, 'src/convex-polytope.mjs~ConvexPolytope.genSimplexes-example0_2', false)
testDriver.test(() => { return simplexes[1].length }, 3, 'src/convex-polytope.mjs~ConvexPolytope.genSimplexes-example0_3', false)
const vCount = [0, 0, 0, 0]
for (let i = simplexes.length - 1; i >= 0; i -= 1) {
  for (let j = vertices.length - 1; j >= 0; j -= 1) {
    if (simplexes[i][0] === vertices[j] ||
      simplexes[i][1] === vertices[j] ||
      simplexes[i][2] === vertices[j]
    ) {
      vCount[j] += 1
    }
  }
}
vCount.sort()
testDriver.test(() => { return vCount[0] }, 1, 'src/convex-polytope.mjs~ConvexPolytope.genSimplexes-example0_4', false)
testDriver.test(() => { return vCount[1] }, 1, 'src/convex-polytope.mjs~ConvexPolytope.genSimplexes-example0_5', false)
testDriver.test(() => { return vCount[2] }, 2, 'src/convex-polytope.mjs~ConvexPolytope.genSimplexes-example0_6', false)
testDriver.test(() => { return vCount[3] }, 2, 'src/convex-polytope.mjs~ConvexPolytope.genSimplexes-example0_7', false)

// invalid parameters
testDriver.test(() => { return ConvexPolytope.genSimplexes(3, facets) }, Error, 'src/convex-polytope.mjs~ConvexPolytope.genSimplexes-example0_8', false)
testDriver.test(() => { return ConvexPolytope.genSimplexes(null, facets) }, Error, 'src/convex-polytope.mjs~ConvexPolytope.genSimplexes-example0_9', false)
testDriver.test(() => { return ConvexPolytope.genSimplexes(2.1, facets) }, Error, 'src/convex-polytope.mjs~ConvexPolytope.genSimplexes-example0_10', false)
testDriver.test(() => { return ConvexPolytope.genSimplexes(2, null) }, Error, 'src/convex-polytope.mjs~ConvexPolytope.genSimplexes-example0_11', false)
facets.push(null)
testDriver.test(() => { return ConvexPolytope.genSimplexes(2, facets) }, Error, 'src/convex-polytope.mjs~ConvexPolytope.genSimplexes-example0_12', false)
facets.pop()

testDriver.test(() => { return ConvexPolytope.genSimplexes(1, []) }, Error, 'src/convex-polytope.mjs~ConvexPolytope.genSimplexes-example0_13', false)
facets = [{ vertices: [] }, { vertices: [] }]
testDriver.test(() => { return ConvexPolytope.genSimplexes(1, facets) }, Error, 'src/convex-polytope.mjs~ConvexPolytope.genSimplexes-example0_14', false)

// 0-dim
simplexes = ConvexPolytope.genSimplexes(0, [])
testDriver.test(() => { return simplexes.length }, 1, 'src/convex-polytope.mjs~ConvexPolytope.genSimplexes-example0_15', false)
testDriver.test(() => { return simplexes[0].length }, 1, 'src/convex-polytope.mjs~ConvexPolytope.genSimplexes-example0_16', false)
testDriver.test(() => { return ConvexPolytope.genSimplexes(0, [{ vertices: [] }]) }, Error, 'src/convex-polytope.mjs~ConvexPolytope.genSimplexes-example0_17', false)
