import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Matrix as M } from '@kkitahara/linear-algebra'
import { Edge } from '../../src/index.mjs'

const m1 = new M(1, 1, 1)
const m2 = new M(2, 1, 1)
const e = new Edge(m1, m2)

testDriver.test(() => { return e instanceof Edge }, true, 'src/edge.mjs~Edge#constructor-example0_0', false)
testDriver.test(() => { return e.v0 === m1 }, true, 'src/edge.mjs~Edge#constructor-example0_1', false)
testDriver.test(() => { return e.v1 === m2 }, true, 'src/edge.mjs~Edge#constructor-example0_2', false)
