import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Matrix as M } from '@kkitahara/linear-algebra'
import { Facet } from '../../src/index.mjs'

// no facet in 0-dim
testDriver.test(() => { return new Facet(new M(), 1, true) }, Error, 'src/facet.mjs~Facet#constructor-example2_0', false)
