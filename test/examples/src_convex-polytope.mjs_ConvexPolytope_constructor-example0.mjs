import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ConvexPolytope } from '../../src/index.mjs'

const facets = []
const vertices = []
const edges = []
const a = new ConvexPolytope(facets, vertices, edges)

testDriver.test(() => { return a instanceof ConvexPolytope }, true, 'src/convex-polytope.mjs~ConvexPolytope#constructor-example0_0', false)
testDriver.test(() => { return a.facets === facets }, true, 'src/convex-polytope.mjs~ConvexPolytope#constructor-example0_1', false)
testDriver.test(() => { return a.vertices === vertices }, true, 'src/convex-polytope.mjs~ConvexPolytope#constructor-example0_2', false)
testDriver.test(() => { return a.edges === edges }, true, 'src/convex-polytope.mjs~ConvexPolytope#constructor-example0_3', false)

const b = new ConvexPolytope()
testDriver.test(() => { return b instanceof ConvexPolytope }, true, 'src/convex-polytope.mjs~ConvexPolytope#constructor-example0_4', false)
testDriver.test(() => { return b.facets instanceof Array }, true, 'src/convex-polytope.mjs~ConvexPolytope#constructor-example0_5', false)
testDriver.test(() => { return b.facets.length === 0 }, true, 'src/convex-polytope.mjs~ConvexPolytope#constructor-example0_6', false)
testDriver.test(() => { return b.vertices instanceof Array }, true, 'src/convex-polytope.mjs~ConvexPolytope#constructor-example0_7', false)
testDriver.test(() => { return b.vertices.length === 0 }, true, 'src/convex-polytope.mjs~ConvexPolytope#constructor-example0_8', false)
testDriver.test(() => { return b.edges instanceof Array }, true, 'src/convex-polytope.mjs~ConvexPolytope#constructor-example0_9', false)
testDriver.test(() => { return b.edges.length === 0 }, true, 'src/convex-polytope.mjs~ConvexPolytope#constructor-example0_10', false)
testDriver.test(() => { return b.weight }, true, 'src/convex-polytope.mjs~ConvexPolytope#constructor-example0_11', false)

testDriver.test(() => { return new ConvexPolytope(null, vertices, edges) }, Error, 'src/convex-polytope.mjs~ConvexPolytope#constructor-example0_12', false)
testDriver.test(() => { return new ConvexPolytope(facets, null, edges) }, Error, 'src/convex-polytope.mjs~ConvexPolytope#constructor-example0_13', false)
testDriver.test(() => { return new ConvexPolytope(facets, vertices, null) }, Error, 'src/convex-polytope.mjs~ConvexPolytope#constructor-example0_14', false)
testDriver.test(() => { return new ConvexPolytope(facets, vertices, edges, null) }, Error, 'src/convex-polytope.mjs~ConvexPolytope#constructor-example0_15', false)
