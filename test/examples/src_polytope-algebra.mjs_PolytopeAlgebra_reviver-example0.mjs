import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { Polytope, PolytopeAlgebra } from '../../src/index.mjs'
const r = new RealAlgebra()
const l = new LinearAlgebra(r)
const p2d = new PolytopeAlgebra(2, l)

const p = p2d.hypercube(1)
const p2 = p2d.translate(p, [1, 1])
const p3 = p2d.add(p, p2)

const s = JSON.stringify(p3)
testDriver.test(() => { return typeof s }, 'string', 'src/polytope-algebra.mjs~PolytopeAlgebra#reviver-example0_0', false)

const p4 = JSON.parse(s, p2d.reviver)
testDriver.test(() => { return p4 instanceof Polytope }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#reviver-example0_1', false)
testDriver.test(() => { return r.eq(p2d.volume(p4), 7) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#reviver-example0_2', false)

const s2 = s.replace('1.1.4', '0.0.0')
testDriver.test(() => { return JSON.parse(s2, p2d.reviver) }, Error, 'src/polytope-algebra.mjs~PolytopeAlgebra#reviver-example0_3', false)
