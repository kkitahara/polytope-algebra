import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { Polytope, PolytopeAlgebra } from '../../src/index.mjs'
const r = new RealAlgebra()
const l = new LinearAlgebra(r)
const p3d = new PolytopeAlgebra(3, l)

const p = p3d.hypercube(1)
let simplexes = p3d.genSimplexes(p)
testDriver.test(() => { return simplexes instanceof Array }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#genSimplexes-example0_0', false)
testDriver.test(() => { return simplexes.length }, 6, 'src/polytope-algebra.mjs~PolytopeAlgebra#genSimplexes-example0_1', false)
testDriver.test(() => { return simplexes[0].length }, 4, 'src/polytope-algebra.mjs~PolytopeAlgebra#genSimplexes-example0_2', false)

simplexes = p3d.genSimplexes(new Polytope())
testDriver.test(() => { return simplexes instanceof Array }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#genSimplexes-example0_3', false)
testDriver.test(() => { return simplexes.length }, 0, 'src/polytope-algebra.mjs~PolytopeAlgebra#genSimplexes-example0_4', false)

// invalid type
testDriver.test(() => { return p3d.genSimplexes(null) }, Error, 'src/polytope-algebra.mjs~PolytopeAlgebra#genSimplexes-example0_5', false)
p[0].vertices[0].push(1)
testDriver.test(() => { return p.dim }, 4, 'src/polytope-algebra.mjs~PolytopeAlgebra#genSimplexes-example0_6', false)
// inconsistent dimension
testDriver.test(() => { return p3d.genSimplexes(p) }, Error, 'src/polytope-algebra.mjs~PolytopeAlgebra#genSimplexes-example0_7', false)
