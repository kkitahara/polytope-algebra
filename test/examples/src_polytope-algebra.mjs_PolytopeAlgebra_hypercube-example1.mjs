import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { PolytopeAlgebra } from '../../src/index.mjs'
const r = new RealAlgebra()
const l = new LinearAlgebra(r)
const p0d = new PolytopeAlgebra(0, l)

// 0-dim test
const p = p0d.hypercube()
testDriver.test(() => { return p.length }, 1, 'src/polytope-algebra.mjs~PolytopeAlgebra#hypercube-example1_0', false)
testDriver.test(() => { return p[0].facets.length }, 0, 'src/polytope-algebra.mjs~PolytopeAlgebra#hypercube-example1_1', false)
testDriver.test(() => { return p[0].vertices.length }, 1, 'src/polytope-algebra.mjs~PolytopeAlgebra#hypercube-example1_2', false)
testDriver.test(() => { return p[0].edges.length }, 0, 'src/polytope-algebra.mjs~PolytopeAlgebra#hypercube-example1_3', false)
testDriver.test(() => { return p[0].dim }, 0, 'src/polytope-algebra.mjs~PolytopeAlgebra#hypercube-example1_4', false)
