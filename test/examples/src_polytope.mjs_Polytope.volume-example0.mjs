import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { Facet, ConvexPolytope, Polytope } from '../../src/index.mjs'
const r = new RealAlgebra()
const l = new LinearAlgebra(r)

const x = l.$(1, 0)
const y = l.$(0, 1)
const facets = [
  new Facet(x, 1, true),
  new Facet(x, -1, false),
  new Facet(y, 1, true),
  new Facet(y, -1, false)]
const vertices = [
  l.$(1, 1),
  l.$(-1, 1),
  l.$(1, -1),
  l.$(-1, -1)]
facets[0].vertices = [vertices[0], vertices[2]]
facets[1].vertices = [vertices[1], vertices[3]]
facets[2].vertices = [vertices[0], vertices[1]]
facets[3].vertices = [vertices[2], vertices[3]]
const edges = ConvexPolytope.genEdges(2, facets)
const cp = new ConvexPolytope(facets, vertices, edges)
let p = new Polytope(cp)
let simplexes = p.genSimplexes()
testDriver.test(() => { return Polytope.volume(simplexes, l) }, 4, 'src/polytope.mjs~Polytope.volume-example0_0', false)

let cp2 = cp.copy(l)
cp2.iaddFacet(new Facet(x, 1, true), l)
p = new Polytope(cp2)
simplexes = p.genSimplexes()
testDriver.test(() => { return Polytope.volume(simplexes, l) }, 4, 'src/polytope.mjs~Polytope.volume-example0_1', false)

cp2 = cp.copy(l)
cp2.iaddFacet(new Facet(x, 1, false), l)
p = new Polytope(cp2)
simplexes = p.genSimplexes()
testDriver.test(() => { return Polytope.volume(simplexes, l) }, 0, 'src/polytope.mjs~Polytope.volume-example0_2', false)

cp2 = cp.copy(l)
cp2.iaddFacet(new Facet(x, -1, true), l)
p = new Polytope(cp2)
simplexes = p.genSimplexes()
testDriver.test(() => { return Polytope.volume(simplexes, l) }, 0, 'src/polytope.mjs~Polytope.volume-example0_3', false)

cp2 = cp.copy(l)
cp2.iaddFacet(new Facet(x, -1, false), l)
p = new Polytope(cp2)
simplexes = p.genSimplexes()
testDriver.test(() => { return Polytope.volume(simplexes, l) }, 4, 'src/polytope.mjs~Polytope.volume-example0_4', false)

cp2 = cp.copy(l)
cp2.iaddFacet(new Facet(l.$(1, 1), 2, true), l)
p = new Polytope(cp2)
simplexes = p.genSimplexes()
testDriver.test(() => { return Polytope.volume(simplexes, l) }, 4, 'src/polytope.mjs~Polytope.volume-example0_5', false)

cp2 = cp.copy(l)
cp2.iaddFacet(new Facet(l.$(1, 1), 1, true), l)
p = new Polytope(cp2)
simplexes = p.genSimplexes()
testDriver.test(() => { return Polytope.volume(simplexes, l) }, 3.5, 'src/polytope.mjs~Polytope.volume-example0_6', false)

cp2 = cp.copy(l)
cp2.iaddFacet(new Facet(l.$(1, 1), 0, true), l)
p = new Polytope(cp2)
simplexes = p.genSimplexes()
testDriver.test(() => { return Polytope.volume(simplexes, l) }, 2, 'src/polytope.mjs~Polytope.volume-example0_7', false)

cp2 = cp.copy(l)
cp2.iaddFacet(new Facet(l.$(1, 1), -1, true), l)
p = new Polytope(cp2)
simplexes = p.genSimplexes()
testDriver.test(() => { return Polytope.volume(simplexes, l) }, 0.5, 'src/polytope.mjs~Polytope.volume-example0_8', false)

cp2 = cp.copy(l)
cp2.iaddFacet(new Facet(l.$(1, 1), -2, true), l)
p = new Polytope(cp2)
simplexes = p.genSimplexes()
testDriver.test(() => { return Polytope.volume(simplexes, l) }, 0, 'src/polytope.mjs~Polytope.volume-example0_9', false)

cp2 = cp.copy(l)
// ANTI-PATTERN!
// this is just for a test
p = new Polytope(cp, cp2)
simplexes = p.genSimplexes()
testDriver.test(() => { return Polytope.volume(simplexes, l) }, 8, 'src/polytope.mjs~Polytope.volume-example0_10', false)

// 0-dim
simplexes = [[l.$()]]
simplexes[0].weight = true
testDriver.test(() => { return Polytope.volume(simplexes, l) }, 1, 'src/polytope.mjs~Polytope.volume-example0_11', false)

cp2 = cp.copy(l)
p = new Polytope(cp2)
simplexes = p.genSimplexes()
testDriver.test(() => { return Polytope.volume(simplexes, null) }, Error, 'src/polytope.mjs~Polytope.volume-example0_12', false)
testDriver.test(() => { return Polytope.volume(null, l) }, Error, 'src/polytope.mjs~Polytope.volume-example0_13', false)
simplexes.push(null)
testDriver.test(() => { return Polytope.volume(simplexes, l) }, Error, 'src/polytope.mjs~Polytope.volume-example0_14', false)
simplexes.pop()
simplexes[0].push(l.$(1, 1))
testDriver.test(() => { return Polytope.volume(simplexes, l) }, Error, 'src/polytope.mjs~Polytope.volume-example0_15', false)
simplexes[0].pop()
simplexes[0].pop()
simplexes[0].push(l.$(1, 1, 1))
testDriver.test(() => { return Polytope.volume(simplexes, l) }, Error, 'src/polytope.mjs~Polytope.volume-example0_16', false)

l.isReal = () => false
testDriver.test(() => { return Polytope.volume(simplexes, l) }, Error, 'src/polytope.mjs~Polytope.volume-example0_17', false)
