import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { PolytopeAlgebra } from '../../src/index.mjs'
const r = new RealAlgebra(1e-5)
const l = new LinearAlgebra(r)
const p2d = new PolytopeAlgebra(2, l)

let p = p2d.hypercube(1)
let a = Math.sqrt(2) / 2
let p2 = p2d.translate(p2d.irotate(p2d.hypercube(1),
  l.$(a, a, a, -a).setDim(2)), [3, 3])

let arr = p[0].imulSub(p2[0], l)
testDriver.test(() => { return arr.length }, 1, 'src/convex-polytope.mjs~ConvexPolytope#imulSub-example1_0', false)

p = p2d.hypercube(1)
a = Math.sqrt(2) / 2
p2 = p2d.iaddFacet(p2d.hypercube(1), p2d.facet([1, 1], 1))
p2 = p2d.itranslate(p2, [1, 1])

arr = p[0].imulSub(p2[0], l)
testDriver.test(() => { return arr.length }, 2, 'src/convex-polytope.mjs~ConvexPolytope#imulSub-example1_1', false)

p = p2d.hypercube(1)
p[0].nullify()
p2 = p2d.hypercube(1)

arr = p[0].imulSub(p2[0], l)
testDriver.test(() => { return arr.length }, 0, 'src/convex-polytope.mjs~ConvexPolytope#imulSub-example1_2', false)

l.isReal = () => false
testDriver.test(() => { return p[0].imulSub(p2[0], l) }, Error, 'src/convex-polytope.mjs~ConvexPolytope#imulSub-example1_3', false)
