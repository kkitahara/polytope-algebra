import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Matrix as M } from '@kkitahara/linear-algebra'
import { Edge } from '../../src/index.mjs'

const m1 = new M(1, 1, 1)
const m2 = new M(2, 1, 1)

testDriver.test(() => { return new Edge(new M(1, 1, 1).setDim(1, 3), m2) }, Error, 'src/edge.mjs~Edge#constructor-example1_0', false)
testDriver.test(() => { return new Edge(m1, new M(2, 1, 1).setDim(1, 3)) }, Error, 'src/edge.mjs~Edge#constructor-example1_1', false)

testDriver.test(() => { return new Edge(new M(1, 1, 1, 1), m2) }, Error, 'src/edge.mjs~Edge#constructor-example1_2', false)
testDriver.test(() => { return new Edge(m1, new M(2, 1, 1, 1)) }, Error, 'src/edge.mjs~Edge#constructor-example1_3', false)

testDriver.test(() => { return new Edge(new M(), m2) }, Error, 'src/edge.mjs~Edge#constructor-example1_4', false)
testDriver.test(() => { return new Edge(m1, new M()) }, Error, 'src/edge.mjs~Edge#constructor-example1_5', false)
