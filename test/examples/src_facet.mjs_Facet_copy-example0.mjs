import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { Facet } from '../../src/index.mjs'
const r = new RealAlgebra()
const l = new LinearAlgebra(r)

const m = l.$(1, 2, 3)
const d = 2
const f = new Facet(m, d, true)
// ANTI-PATTERN
// this is just for a test, vertices must be on the facet
const v = l.$(0, 0, 0)
f.vertices.push(v)
const f2 = f.copy(l)

testDriver.test(() => { return f2 instanceof Facet }, true, 'src/facet.mjs~Facet#copy-example0_0', false)
testDriver.test(() => { return f2.nvec === m }, true, 'src/facet.mjs~Facet#copy-example0_1', false)
testDriver.test(() => { return f2.faceOutside }, true, 'src/facet.mjs~Facet#copy-example0_2', false)
testDriver.test(() => { return f2.d === d }, false, 'src/facet.mjs~Facet#copy-example0_3', false)
testDriver.test(() => { return r.eq(f2.d, d) }, true, 'src/facet.mjs~Facet#copy-example0_4', false)
testDriver.test(() => { return f2.vertices instanceof Array }, true, 'src/facet.mjs~Facet#copy-example0_5', false)
testDriver.test(() => { return f2.vertices.length === 1 }, true, 'src/facet.mjs~Facet#copy-example0_6', false)
testDriver.test(() => { return f2.vertices[0] === v }, true, 'src/facet.mjs~Facet#copy-example0_7', false)

// invalid parameter
testDriver.test(() => { return f.copy(null) }, Error, 'src/facet.mjs~Facet#copy-example0_8', false)
