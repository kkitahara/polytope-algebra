import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
import { Facet, OUTSIDE_OF_FACET, ON_FACET, INSIDE_OF_FACET }
  from '../../src/index.mjs'
const r = new RealAlgebra()
const r2 = new RealAlgebra(0.1)
const l = new LinearAlgebra(r)
const l2 = new LinearAlgebra(r2)

const m = new M(1, 1, 1)
const d = 3
const f = new Facet(m, d, true)
const v1 = new M(0.99, 1, 1)
const v2 = new M(1, 1, 1)
const v3 = new M(1, 1, 1.01)

testDriver.test(() => { return f.position(v1, l) }, INSIDE_OF_FACET, 'src/facet.mjs~Facet#position-example0_0', false)
testDriver.test(() => { return f.position(v2, l) }, ON_FACET, 'src/facet.mjs~Facet#position-example0_1', false)
testDriver.test(() => { return f.position(v3, l) }, OUTSIDE_OF_FACET, 'src/facet.mjs~Facet#position-example0_2', false)

testDriver.test(() => { return f.position(v1, l2) }, ON_FACET, 'src/facet.mjs~Facet#position-example0_3', false)
testDriver.test(() => { return f.position(v2, l2) }, ON_FACET, 'src/facet.mjs~Facet#position-example0_4', false)
testDriver.test(() => { return f.position(v3, l2) }, ON_FACET, 'src/facet.mjs~Facet#position-example0_5', false)
