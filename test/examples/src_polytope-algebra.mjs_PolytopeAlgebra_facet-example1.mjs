import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { Facet, PolytopeAlgebra } from '../../src/index.mjs'
const r = new RealAlgebra()
const l = new LinearAlgebra(r)
const p2d = new PolytopeAlgebra(5, l)

const nvec = l.$(1, 1, 1, 1, 1)
let f = p2d.facet(nvec, 3, true)
testDriver.test(() => { return f instanceof Facet }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#facet-example1_0', false)
testDriver.test(() => { return f.nvec === nvec }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#facet-example1_1', false)
testDriver.test(() => { return r.eq(f.d, 3) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#facet-example1_2', false)
testDriver.test(() => { return f.faceOutside }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#facet-example1_3', false)

f = p2d.facet(nvec, 3, true)
testDriver.test(() => { return f instanceof Facet }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#facet-example1_4', false)
testDriver.test(() => { return f.nvec === nvec }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#facet-example1_5', false)
testDriver.test(() => { return r.eq(f.d, 3) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#facet-example1_6', false)
testDriver.test(() => { return f.faceOutside }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#facet-example1_7', false)

const nvec2 = l.$(2, 2, 2, 2, 2)
f = p2d.facet(nvec2, 3, true)
testDriver.test(() => { return f instanceof Facet }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#facet-example1_8', false)
testDriver.test(() => { return f.nvec === nvec2 }, false, 'src/polytope-algebra.mjs~PolytopeAlgebra#facet-example1_9', false)
testDriver.test(() => { return f.nvec === nvec }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#facet-example1_10', false)
testDriver.test(() => { return r.ne(f.d, 3) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#facet-example1_11', false)
testDriver.test(() => { return r.eq(f.d, r.$(3, 2)) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#facet-example1_12', false)
testDriver.test(() => { return f.faceOutside }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#facet-example1_13', false)

const nvec3 = l.$(-2, -2, -2, -2, -2)
f = p2d.facet(nvec3, 3)
testDriver.test(() => { return f instanceof Facet }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#facet-example1_14', false)
testDriver.test(() => { return f.nvec === nvec3 }, false, 'src/polytope-algebra.mjs~PolytopeAlgebra#facet-example1_15', false)
testDriver.test(() => { return f.nvec === nvec }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#facet-example1_16', false)
testDriver.test(() => { return r.ne(f.d, 3) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#facet-example1_17', false)
testDriver.test(() => { return r.eq(f.d, r.$(-3, 2)) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#facet-example1_18', false)
testDriver.test(() => { return f.faceOutside }, false, 'src/polytope-algebra.mjs~PolytopeAlgebra#facet-example1_19', false)

testDriver.test(() => { return p2d.facet([0, 0, 0, 0, 0], 1, true) }, Error, 'src/polytope-algebra.mjs~PolytopeAlgebra#facet-example1_20', false)
testDriver.test(() => { return p2d.facet([0, 0, 0, 0], 1, true) }, Error, 'src/polytope-algebra.mjs~PolytopeAlgebra#facet-example1_21', false)
