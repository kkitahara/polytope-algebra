import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { PolytopeAlgebra } from '../../src/index.mjs'
const r = new RealAlgebra()
const l = new LinearAlgebra(r)
const p2d = new PolytopeAlgebra(2, l)

const p = p2d.hypercube(1)
const p2 = p2d.translate(p, [1, 1])
testDriver.test(() => { return p !== p2 }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#translate-example0_0', false)
testDriver.test(() => { return p.length }, 1, 'src/polytope-algebra.mjs~PolytopeAlgebra#translate-example0_1', false)
testDriver.test(() => { return p2.length }, 1, 'src/polytope-algebra.mjs~PolytopeAlgebra#translate-example0_2', false)
testDriver.test(() => { return r.eq(p[0].facets[0].d, 1) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#translate-example0_3', false)
testDriver.test(() => { return r.eq(p[0].facets[1].d, -1) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#translate-example0_4', false)
testDriver.test(() => { return r.eq(p[0].facets[2].d, 1) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#translate-example0_5', false)
testDriver.test(() => { return r.eq(p[0].facets[3].d, -1) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#translate-example0_6', false)
testDriver.test(() => { return r.eq(p2[0].facets[0].d, 2) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#translate-example0_7', false)
testDriver.test(() => { return r.eq(p2[0].facets[1].d, 0) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#translate-example0_8', false)
testDriver.test(() => { return r.eq(p2[0].facets[2].d, 2) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#translate-example0_9', false)
testDriver.test(() => { return r.eq(p2[0].facets[3].d, 0) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#translate-example0_10', false)
testDriver.test(() => { return l.eq(p[0].vertices[0], [1, 1]) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#translate-example0_11', false)
testDriver.test(() => { return l.eq(p[0].vertices[1], [-1, 1]) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#translate-example0_12', false)
testDriver.test(() => { return l.eq(p[0].vertices[2], [1, -1]) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#translate-example0_13', false)
testDriver.test(() => { return l.eq(p[0].vertices[3], [-1, -1]) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#translate-example0_14', false)
testDriver.test(() => { return l.eq(p2[0].vertices[0], [2, 2]) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#translate-example0_15', false)
testDriver.test(() => { return l.eq(p2[0].vertices[1], [0, 2]) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#translate-example0_16', false)
testDriver.test(() => { return l.eq(p2[0].vertices[2], [2, 0]) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#translate-example0_17', false)
testDriver.test(() => { return l.eq(p2[0].vertices[3], [0, 0]) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#translate-example0_18', false)

testDriver.test(() => { return p2d.translate(null, [1, 1]) }, Error, 'src/polytope-algebra.mjs~PolytopeAlgebra#translate-example0_19', false)
