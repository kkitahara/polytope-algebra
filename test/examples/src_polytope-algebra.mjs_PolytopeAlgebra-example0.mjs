import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
// import { RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { PolytopeAlgebra } from '../../src/index.mjs'
const r = new RealAlgebra(1e-8)
const l = new LinearAlgebra(r)
const dim = 2
const p2d = new PolytopeAlgebra(dim, l)

// Generate a new hypercube of edge length `2 * d` centred at the origin.
let d = 1
let p1 = p2d.hypercube(d)

// p1
// +-----------+
// |\\\\\\\\\\\|
// |\\\\\\\\\\\|
// |\\\\\o\\\\\|
// |\\\\\\\\\\\|
// |\\\\\\\\\\\|
// +-----------+

// Calculate the volume of polytopes
const vol = p2d.volume(p1)
testDriver.test(() => { return vol.toString() }, '4', 'src/polytope-algebra.mjs~PolytopeAlgebra-example0_0', false)

// Generate a new facet.
// A facet is characterised by its normal vector (nvec),
// distance from the origin (d),
// and the direction of the normal vector (faceOutside).
let nvec = [1, 0]
d = 1
let faceOutside = true
let f = p2d.facet(nvec, d, faceOutside)

// \\\\\\\\\\\\|
// \\\\\\\\\\\\|
// \\\\\\\\\\\\|
// \\\\\\o\\\\\| f
// \\\\\\\\\\\\|
// \\\\\\\\\\\\|
// \\\\\\\\\\\\|

nvec = [1, 0]
d = 1
faceOutside = false
f = p2d.facet(nvec, d, faceOutside)

//             |\\\\\\\\\\\\
//             |\\\\\\\\\\\\
//             |\\\\\\\\\\\\
//       o   f |\\\\\\\\\\\\
//             |\\\\\\\\\\\\
//             |\\\\\\\\\\\\
//             |\\\\\\\\\\\\

// No need to normalise the normal vector,
// provided that `d` is the dot product of the normal vector and
// the position vector of a vertex on the facet.
f = p2d.facet([2, 0], 1, true)

// \\\\\\\\\|
// \\\\\\\\\|
// \\\\\\\\\|
// \\\\\\o\\| f
// \\\\\\\\\|
// \\\\\\\\\|
// \\\\\\\\\|

f = p2d.facet([2, 0], 2, true)

// \\\\\\\\\\\\|
// \\\\\\\\\\\\|
// \\\\\\\\\\\\|
// \\\\\\o\\\\\| f
// \\\\\\\\\\\\|
// \\\\\\\\\\\\|
// \\\\\\\\\\\\|

// Add a facet to polytopes (in place)
p1 = p2d.hypercube(d)

// p1
// +-----------+
// |\\\\\\\\\\\|
// |\\\\\\\\\\\|
// |\\\\\o\\\\\|
// |\\\\\\\\\\\|
// |\\\\\\\\\\\|
// +-----------+

f = p2d.facet([2, -1], 1, true)

// \\\\\\\\\\\\/
// \\\\\\\\\\\/
// \\\\\\\\\\/
// \\\\\\o\\/ f
// \\\\\\\\/
// \\\\\\\/
// \\\\\\/

p1 = p2d.iaddFacet(p1, f)
testDriver.test(() => { return p2d.volume(p1).toString() }, '3', 'src/polytope-algebra.mjs~PolytopeAlgebra-example0_1', false)

// p1
// +-----------+
// |\\\\\\\\\\/
// |\\\\\\\\\/
// |\\\\\o\\/
// |\\\\\\\/
// |\\\\\\/
// +-----+

// Copy (generate a new object)
let p2 = p2d.copy(p1)
testDriver.test(() => { return p2d.volume(p2).toString() }, '3', 'src/polytope-algebra.mjs~PolytopeAlgebra-example0_2', false)

// p2
// +-----------+
// |\\\\\\\\\\/
// |\\\\\\\\\/
// |\\\\\o\\/
// |\\\\\\\/
// |\\\\\\/
// +-----+

// Rotation (new object is genrated, since v1.1.0)
const c4 = l.$(0, -1, 1, 0).setDim(2)
p2 = p2d.rotate(p1, c4)
testDriver.test(() => { return p1 !== p2 }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra-example0_3', false)
testDriver.test(() => { return p2d.volume(p2).toString() }, '3', 'src/polytope-algebra.mjs~PolytopeAlgebra-example0_4', false)

// p2
// +-__
// |\\\--_
// |\\\\\\--__
// |\\\\\o\\\\-+
// |\\\\\\\\\\\|
// |\\\\\\\\\\\|
// +-----------+

// In-place rotation (new object is not genrated, since v1.1.0)
p2 = p2d.irotate(p2, c4)
testDriver.test(() => { return p2d.volume(p2).toString() }, '3', 'src/polytope-algebra.mjs~PolytopeAlgebra-example0_5', false)

// p2
//       +-----+
//      /\\\\\\|
//     /\\\\\\\|
//    /\\o\\\\\|
//   /\\\\\\\\\|
//  /\\\\\\\\\\|
// +-----------+

// Translation (new object is generated)
p2 = p2d.translate(p1, [r.$(1, 2), 0])
testDriver.test(() => { return p1 !== p2 }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra-example0_6', false)
testDriver.test(() => { return p2d.volume(p2).toString() }, '3', 'src/polytope-algebra.mjs~PolytopeAlgebra-example0_7', false)

// p2
//    +-----------+
//    |\\\\\\\\\\/
//    |\\\\\\\\\/
//    |\\o\\\\\/
//    |\\\\\\\/
//    |\\\\\\/
//    +-----+

// In-place translation (new object is not generated)
p1 = p2d.itranslate(p1, [r.$(1, 2), 0])
testDriver.test(() => { return p2d.volume(p1).toString() }, '3', 'src/polytope-algebra.mjs~PolytopeAlgebra-example0_8', false)

// p1
//    +-----------+
//    |\\\\\\\\\\/
//    |\\\\\\\\\/
//    |\\o\\\\\/
//    |\\\\\\\/
//    |\\\\\\/
//    +-----+

// Scaling (new object is generated)
p2 = p2d.scale(p1, 2)
testDriver.test(() => { return p1 !== p2 }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra-example0_9', false)
testDriver.test(() => { return p2d.volume(p2).toString() }, '12', 'src/polytope-algebra.mjs~PolytopeAlgebra-example0_10', false)

// p2
// +-----------------------+
// |\\\\\\\\\\\\\\\\\\\\\\/
// |\\\\\\\\\\\\\\\\\\\\\/
// |\\\\\\\\\\\\\\\\\\\\/
// |\\\\\\\\\\\\\\\\\\\/
// |\\\\\\\\\\\\\\\\\\/
// |\\\\\o\\\\\\\\\\\/
// |\\\\\\\\\\\\\\\\/
// |\\\\\\\\\\\\\\\/
// |\\\\\\\\\\\\\\/
// |\\\\\\\\\\\\\/
// |\\\\\\\\\\\\/
// +-----------+

// In-place scaling (new object is not generated)
p1 = p2d.iscale(p1, r.$(1, 3))
testDriver.test(() => { return p2d.volume(p1).toString() }, '1 / 3', 'src/polytope-algebra.mjs~PolytopeAlgebra-example0_11', false)

//      p1
//      +---+
//      |o\/
//      +-+

// Multiplication (intersection, new object is generated)
p1 = p2d.hypercube(1)
p2 = p2d.itranslate(p2d.hypercube(1), [1, 1])
let p3 = p2d.mul(p1, p2)
testDriver.test(() => { return p3 !== p1 }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra-example0_12', false)
testDriver.test(() => { return p3 !== p2 }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra-example0_13', false)
testDriver.test(() => { return p2d.volume(p3).toString() }, '1', 'src/polytope-algebra.mjs~PolytopeAlgebra-example0_14', false)

//
//
//
// p1
// +-----------+
// |\\\\\\\\\\\|
// |\\\\\\\\\\\|
// |\\\\\o\\\\\|
// |\\\\\\\\\\\|
// |\\\\\\\\\\\|
// +-----------+

//       p2
//       +-----------+
//       |\\\\\\\\\\\|
//       |\\\\\\\\\\\|
//       |\\\\\\\\\\\|
//       |\\\\\\\\\\\|
//       |\\\\\\\\\\\|
//       o-----------+
//
//
//

//
//
//
//       p3
//       +-----+
//       |\\\\\|
//       |\\\\\|
//       o-----+
//
//
//

// Subtraction (set difference, new object is generated)
p1 = p2d.hypercube(1)
p2 = p2d.itranslate(p2d.hypercube(1), [1, 1])
p3 = p2d.sub(p1, p2)
testDriver.test(() => { return p3 !== p1 }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra-example0_15', false)
testDriver.test(() => { return p3 !== p2 }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra-example0_16', false)
testDriver.test(() => { return p2d.volume(p3).toString() }, '3', 'src/polytope-algebra.mjs~PolytopeAlgebra-example0_17', false)

//
//
//
// p1
// +-----------+
// |\\\\\\\\\\\|
// |\\\\\\\\\\\|
// |\\\\\o\\\\\|
// |\\\\\\\\\\\|
// |\\\\\\\\\\\|
// +-----------+

//       p2
//       +-----------+
//       |\\\\\\\\\\\|
//       |\\\\\\\\\\\|
//       |\\\\\\\\\\\|
//       |\\\\\\\\\\\|
//       |\\\\\\\\\\\|
//       o-----------+
//
//
//

//
//
//
// p3
// +-----+
// |\\\\\|
// |\\\\\|
// |\\\\\o-----+
// |\\\\\\\\\\\|
// |\\\\\\\\\\\|
// +-----------+

// Addition (union, new object is generated)
p1 = p2d.hypercube(1)
p2 = p2d.itranslate(p2d.hypercube(1), [1, 1])
p3 = p2d.add(p1, p2)
testDriver.test(() => { return p3 !== p1 }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra-example0_18', false)
testDriver.test(() => { return p3 !== p2 }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra-example0_19', false)
testDriver.test(() => { return p2d.volume(p3).toString() }, '7', 'src/polytope-algebra.mjs~PolytopeAlgebra-example0_20', false)

//
//
//
// p1
// +-----------+
// |\\\\\\\\\\\|
// |\\\\\\\\\\\|
// |\\\\\o\\\\\|
// |\\\\\\\\\\\|
// |\\\\\\\\\\\|
// +-----------+

//       p2
//       +-----------+
//       |\\\\\\\\\\\|
//       |\\\\\\\\\\\|
//       |\\\\\\\\\\\|
//       |\\\\\\\\\\\|
//       |\\\\\\\\\\\|
//       o-----------+
//
//
//

//
//       +-----------+
//       |\\\\\\\\\\\|
// p3    |\\\\\\\\\\\|
// +-----+\\\\\\\\\\\|
// |\\\\\\\\\\\\\\\\\|
// |\\\\\\\\\\\\\\\\\|
// |\\\\\o\\\\\+-----+
// |\\\\\\\\\\\|
// |\\\\\\\\\\\|
// +-----------+

// JSON (stringify and parse)
const str = JSON.stringify(p3)
const p4 = JSON.parse(str, p2d.reviver)
const p5 = p2d.mul(p3, p4)
testDriver.test(() => { return p2d.volume(p4).toString() }, '7', 'src/polytope-algebra.mjs~PolytopeAlgebra-example0_21', false)
testDriver.test(() => { return p2d.volume(p5).toString() }, '7', 'src/polytope-algebra.mjs~PolytopeAlgebra-example0_22', false)
