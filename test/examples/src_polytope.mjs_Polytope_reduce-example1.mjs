import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { PolytopeAlgebra } from '../../src/index.mjs'
const r = new RealAlgebra(1e-5)
const l = new LinearAlgebra(r)
const p2d = new PolytopeAlgebra(2, l)

const p = p2d.hypercube(1)
let p2 = p2d.hypercube(1)
p2 = p2d.iaddFacet(p2, p2d.facet([-1, -1], 0, false))
p2 = p2d.itranslate(p2, [2, 0])

p.push(p2[0])
p.reduce(l)
testDriver.test(() => { return p.length }, 1, 'src/polytope.mjs~Polytope#reduce-example1_0', false)

const p3 = p2d.translate(p2d.hypercube(1), [0, 2])
p.push(p3[0])
p.reduce(l)
testDriver.test(() => { return p.length }, 2, 'src/polytope.mjs~Polytope#reduce-example1_1', false)

let p4 = p2d.hypercube(1)
p4 = p2d.iaddFacet(p4, p2d.facet([1, 1], 0, false))
p4 = p2d.itranslate(p4, [2, 0])
p.push(p4[0])
p.reduce(l)
testDriver.test(() => { return p.length }, 2, 'src/polytope.mjs~Polytope#reduce-example1_2', false)

p.push(p4[0])
p[1].nullify()
p.reduce(l)
testDriver.test(() => { return p.length }, 2, 'src/polytope.mjs~Polytope#reduce-example1_3', false)

p.nullify()
p.reduce(l)
testDriver.test(() => { return p.length }, 0, 'src/polytope.mjs~Polytope#reduce-example1_4', false)

l.isReal = () => false
testDriver.test(() => { return p.reduce(l) }, Error, 'src/polytope.mjs~Polytope#reduce-example1_5', false)

testDriver.test(() => { return p.reduce(null) }, Error, 'src/polytope.mjs~Polytope#reduce-example1_6', false)
