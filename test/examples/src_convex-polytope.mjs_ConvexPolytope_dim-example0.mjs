import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Matrix as M } from '@kkitahara/linear-algebra'
import { ConvexPolytope } from '../../src/index.mjs'

// ANTI-PATTERN
// This is just for a test.
// In practice, facets, vertices and edges must be consistently given.
let a = new ConvexPolytope([], [new M(1, 2), new M(1, 2, 3)])
testDriver.test(() => { return a.dim }, 2, 'src/convex-polytope.mjs~ConvexPolytope#dim-example0_0', false)

a = new ConvexPolytope([], [new M()])
testDriver.test(() => { return a.dim }, 0, 'src/convex-polytope.mjs~ConvexPolytope#dim-example0_1', false)

// throws an Error if there is no vertex
testDriver.test(() => { return new ConvexPolytope().dim }, Error, 'src/convex-polytope.mjs~ConvexPolytope#dim-example0_2', false)
