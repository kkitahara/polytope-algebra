import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { Facet, ConvexPolytope } from '../../src/index.mjs'
const r = new RealAlgebra()
const l = new LinearAlgebra(r)

const x = l.$(1, 0)
const y = l.$(0, 1)
const facets = [
  new Facet(x, 1, true),
  new Facet(x, -1, false),
  new Facet(y, 1, true),
  new Facet(y, -1, false)]
const vertices = [
  l.$(1, 1),
  l.$(-1, 1),
  l.$(1, -1),
  l.$(-1, -1)]
facets[0].vertices = [vertices[0], vertices[2]]
facets[1].vertices = [vertices[1], vertices[3]]
facets[2].vertices = [vertices[0], vertices[1]]
facets[3].vertices = [vertices[2], vertices[3]]
const edges = ConvexPolytope.genEdges(2, facets)
const cp = new ConvexPolytope(facets, vertices, edges)
const cp2 = cp.copy(l)
testDriver.test(() => { return cp2.facets.length }, 4, 'src/convex-polytope.mjs~ConvexPolytope#copy-example0_0', false)
testDriver.test(() => { return cp2.vertices.length }, 4, 'src/convex-polytope.mjs~ConvexPolytope#copy-example0_1', false)
testDriver.test(() => { return cp2.edges.length }, 4, 'src/convex-polytope.mjs~ConvexPolytope#copy-example0_2', false)
testDriver.test(() => { return cp.facets !== cp2.facets }, true, 'src/convex-polytope.mjs~ConvexPolytope#copy-example0_3', false)
testDriver.test(() => { return cp.facets[0] !== cp2.facets[0] }, true, 'src/convex-polytope.mjs~ConvexPolytope#copy-example0_4', false)
testDriver.test(() => { return cp.facets[0].nvec === cp2.facets[0].nvec }, true, 'src/convex-polytope.mjs~ConvexPolytope#copy-example0_5', false)
testDriver.test(() => { return cp.facets[0].d !== cp2.facets[0].d }, true, 'src/convex-polytope.mjs~ConvexPolytope#copy-example0_6', false)
testDriver.test(() => { return cp.facets[0].vertices !== cp2.facets[0].vertices }, true, 'src/convex-polytope.mjs~ConvexPolytope#copy-example0_7', false)
testDriver.test(() => { return cp.facets[0].vertices[0] === cp2.facets[0].vertices[0] }, false, 'src/convex-polytope.mjs~ConvexPolytope#copy-example0_8', false)
testDriver.test(() => { return cp.vertices !== cp2.vertices }, true, 'src/convex-polytope.mjs~ConvexPolytope#copy-example0_9', false)
testDriver.test(() => { return cp.vertices[0] === cp2.vertices[0] }, false, 'src/convex-polytope.mjs~ConvexPolytope#copy-example0_10', false)
testDriver.test(() => { return cp.edges !== cp2.edges }, true, 'src/convex-polytope.mjs~ConvexPolytope#copy-example0_11', false)
testDriver.test(() => { return cp.edges[0] !== cp2.edges[0] }, true, 'src/convex-polytope.mjs~ConvexPolytope#copy-example0_12', false)
testDriver.test(() => { return cp.edges[0].v0 === cp2.edges[0].v0 }, false, 'src/convex-polytope.mjs~ConvexPolytope#copy-example0_13', false)

testDriver.test(() => { return cp.copy(null) }, Error, 'src/convex-polytope.mjs~ConvexPolytope#copy-example0_14', false)
