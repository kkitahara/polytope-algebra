import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { AnotherPolytopeAlgebra, Polytope }
  from '../../src/index.mjs'
const r = new RealAlgebra()
const l = new LinearAlgebra(r)
const p2d = new AnotherPolytopeAlgebra(2, l)
const p1d = new AnotherPolytopeAlgebra(1, l)

const p = p2d.hypercube(1)
const p2 = p2d.translate(p, [1, 1])
const p3 = p2d.mul(p, p2)
testDriver.test(() => { return p3 instanceof Polytope }, true, 'src/another-polytope-algebra.mjs~AnotherPolytopeAlgebra#mul-example0_0', false)
testDriver.test(() => { return p3.length }, 1, 'src/another-polytope-algebra.mjs~AnotherPolytopeAlgebra#mul-example0_1', false)
testDriver.test(() => { return p3 !== p }, true, 'src/another-polytope-algebra.mjs~AnotherPolytopeAlgebra#mul-example0_2', false)
testDriver.test(() => { return p3 !== p2 }, true, 'src/another-polytope-algebra.mjs~AnotherPolytopeAlgebra#mul-example0_3', false)

testDriver.test(() => { return r.eq(p2d.volume(p), 4) }, true, 'src/another-polytope-algebra.mjs~AnotherPolytopeAlgebra#mul-example0_4', false)

testDriver.test(() => { return r.eq(p2d.volume(p2), 4) }, true, 'src/another-polytope-algebra.mjs~AnotherPolytopeAlgebra#mul-example0_5', false)

testDriver.test(() => { return r.eq(p2d.volume(p3), 1) }, true, 'src/another-polytope-algebra.mjs~AnotherPolytopeAlgebra#mul-example0_6', false)

p3.nullify()
const p4 = p2d.mul(p2, p3)
testDriver.test(() => { return p4 instanceof Polytope }, true, 'src/another-polytope-algebra.mjs~AnotherPolytopeAlgebra#mul-example0_7', false)
testDriver.test(() => { return p4.length }, 0, 'src/another-polytope-algebra.mjs~AnotherPolytopeAlgebra#mul-example0_8', false)

const p5 = p2d.mul(p, p2d.translate(p2d.hypercube(1), [2, 0]))
testDriver.test(() => { return p5 instanceof Polytope }, true, 'src/another-polytope-algebra.mjs~AnotherPolytopeAlgebra#mul-example0_9', false)
testDriver.test(() => { return p5.length }, 0, 'src/another-polytope-algebra.mjs~AnotherPolytopeAlgebra#mul-example0_10', false)

testDriver.test(() => { return p2d.mul(p2, p1d.hypercube(1)) }, Error, 'src/another-polytope-algebra.mjs~AnotherPolytopeAlgebra#mul-example0_11', false)
testDriver.test(() => { return p2d.mul(p1d.hypercube(1), p2) }, Error, 'src/another-polytope-algebra.mjs~AnotherPolytopeAlgebra#mul-example0_12', false)
testDriver.test(() => { return p2d.mul(p2, null) }, Error, 'src/another-polytope-algebra.mjs~AnotherPolytopeAlgebra#mul-example0_13', false)
testDriver.test(() => { return p2d.mul(null, p2) }, Error, 'src/another-polytope-algebra.mjs~AnotherPolytopeAlgebra#mul-example0_14', false)
