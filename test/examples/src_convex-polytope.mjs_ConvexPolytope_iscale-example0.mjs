import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { Facet, ConvexPolytope } from '../../src/index.mjs'
const r = new RealAlgebra()
const l = new LinearAlgebra(r)

const x = l.$(1, 0)
const y = l.$(0, 1)
const facets = [
  new Facet(x, 1, true),
  new Facet(x, -1, false),
  new Facet(y, 1, true),
  new Facet(y, -1, false)]
const vertices = [
  l.$(1, 1),
  l.$(-1, 1),
  l.$(1, -1),
  l.$(-1, -1)]
facets[0].vertices = [vertices[0], vertices[2]]
facets[1].vertices = [vertices[1], vertices[3]]
facets[2].vertices = [vertices[0], vertices[1]]
facets[3].vertices = [vertices[2], vertices[3]]
const edges = ConvexPolytope.genEdges(2, facets)
const cp = new ConvexPolytope(facets, vertices, edges)
const cp2 = cp.iscale(2, l)

testDriver.test(() => { return cp === cp2 }, true, 'src/convex-polytope.mjs~ConvexPolytope#iscale-example0_0', false)
testDriver.test(() => { return cp.facets[0].nvec === x }, true, 'src/convex-polytope.mjs~ConvexPolytope#iscale-example0_1', false)
testDriver.test(() => { return cp.facets[1].nvec === x }, true, 'src/convex-polytope.mjs~ConvexPolytope#iscale-example0_2', false)
testDriver.test(() => { return cp.facets[2].nvec === y }, true, 'src/convex-polytope.mjs~ConvexPolytope#iscale-example0_3', false)
testDriver.test(() => { return cp.facets[3].nvec === y }, true, 'src/convex-polytope.mjs~ConvexPolytope#iscale-example0_4', false)

testDriver.test(() => { return cp.facets[0].d }, 2, 'src/convex-polytope.mjs~ConvexPolytope#iscale-example0_5', false)
testDriver.test(() => { return cp.facets[1].d }, -2, 'src/convex-polytope.mjs~ConvexPolytope#iscale-example0_6', false)
testDriver.test(() => { return cp.facets[2].d }, 2, 'src/convex-polytope.mjs~ConvexPolytope#iscale-example0_7', false)
testDriver.test(() => { return cp.facets[3].d }, -2, 'src/convex-polytope.mjs~ConvexPolytope#iscale-example0_8', false)

testDriver.test(() => { return l.eq(cp.vertices[0], [2, 2]) }, true, 'src/convex-polytope.mjs~ConvexPolytope#iscale-example0_9', false)
testDriver.test(() => { return l.eq(cp.vertices[1], [-2, 2]) }, true, 'src/convex-polytope.mjs~ConvexPolytope#iscale-example0_10', false)
testDriver.test(() => { return l.eq(cp.vertices[2], [2, -2]) }, true, 'src/convex-polytope.mjs~ConvexPolytope#iscale-example0_11', false)
testDriver.test(() => { return l.eq(cp.vertices[3], [-2, -2]) }, true, 'src/convex-polytope.mjs~ConvexPolytope#iscale-example0_12', false)

testDriver.test(() => { return cp.iscale(2, null) }, Error, 'src/convex-polytope.mjs~ConvexPolytope#iscale-example0_13', false)
