import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { Facet } from '../../src/index.mjs'
const r = new RealAlgebra()
const l = new LinearAlgebra(r)

const m = l.$(1, 2, 3)
const d = 2
const f = new Facet(m, d, true)
// ANTI-PATTERN
// this is just for a test, vertices must be on the facet
const v = l.$(0, 0, 0)
const v2 = l.$(1, 0, 0)
f.vertices.push(v)
const vMap = new WeakMap()

vMap.set(v, v2)
testDriver.test(() => { return f.vertices[0] === v }, true, 'src/facet.mjs~Facet#replaceVertices-example0_0', false)
f.replaceVertices(vMap)
testDriver.test(() => { return f.vertices[0] === v2 }, true, 'src/facet.mjs~Facet#replaceVertices-example0_1', false)

testDriver.test(() => { return f.replaceVertices(vMap) }, Error, 'src/facet.mjs~Facet#replaceVertices-example0_2', false)
testDriver.test(() => { return f.replaceVertices(null) }, Error, 'src/facet.mjs~Facet#replaceVertices-example0_3', false)
