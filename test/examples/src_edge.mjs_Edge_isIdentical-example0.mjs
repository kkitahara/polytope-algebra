import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Matrix as M } from '@kkitahara/linear-algebra'
import { Edge } from '../../src/index.mjs'

const m1 = new M(1, 1, 1)
const m2 = new M(2, 1, 1)
const e1 = new Edge(m1, m2)
const e2 = new Edge(m1, m2)
const e3 = new Edge(m2, m1)
const e4 = new Edge(m1, new M(2, 1, 1))
const e5 = new Edge(m2, new M(1, 1, 1))

testDriver.test(() => { return e1.isIdentical(e1) }, true, 'src/edge.mjs~Edge#isIdentical-example0_0', false)
testDriver.test(() => { return e1.isIdentical(e2) }, true, 'src/edge.mjs~Edge#isIdentical-example0_1', false)
testDriver.test(() => { return e1.isIdentical(e3) }, true, 'src/edge.mjs~Edge#isIdentical-example0_2', false)
testDriver.test(() => { return e1.isIdentical(e4) }, false, 'src/edge.mjs~Edge#isIdentical-example0_3', false)
testDriver.test(() => { return e1.isIdentical(e5) }, false, 'src/edge.mjs~Edge#isIdentical-example0_4', false)
testDriver.test(() => { return e1.isIdentical(null) }, Error, 'src/edge.mjs~Edge#isIdentical-example0_5', false)
