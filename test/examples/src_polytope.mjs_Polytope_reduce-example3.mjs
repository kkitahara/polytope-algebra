import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { PolytopeAlgebra } from '../../src/index.mjs'

// test for issue #2

const r = new RealAlgebra(1e-10)
const l = new LinearAlgebra(r)
const p2d = new PolytopeAlgebra(2, l)

let p1 = p2d.hypercube(1)
const n1 = [-1, 1]
const f1 = p2d.facet(n1, l.dot(n1, [-1, 0]), true)
p1 = p2d.itranslate(p2d.iaddFacet(p1, f1), [2, 0])
let p2 = p2d.hypercube(1)
const n2 = [-0.1, 0.01]
const f2 = p2d.facet(n2, l.dot(n2, [1, 0]), true)
p2 = p2d.iaddFacet(p2, f2)
testDriver.test(() => { return p1.length }, 1, 'src/polytope.mjs~Polytope#reduce-example3_0', false)
testDriver.test(() => { return p2.length }, 1, 'src/polytope.mjs~Polytope#reduce-example3_1', false)

const p3 = p2d.add(p1, p2)
// shoud be 1
testDriver.test(() => { return p3.length }, 1, 'src/polytope.mjs~Polytope#reduce-example3_2', false)
