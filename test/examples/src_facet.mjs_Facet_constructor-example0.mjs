import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Matrix as M } from '@kkitahara/linear-algebra'
import { Facet } from '../../src/index.mjs'

const m = new M(1, 2, 3)
const d = 2
const f = new Facet(m, d, true)

testDriver.test(() => { return f instanceof Facet }, true, 'src/facet.mjs~Facet#constructor-example0_0', false)
testDriver.test(() => { return f.nvec === m }, true, 'src/facet.mjs~Facet#constructor-example0_1', false)
testDriver.test(() => { return f.faceOutside }, true, 'src/facet.mjs~Facet#constructor-example0_2', false)
testDriver.test(() => { return f.d === d }, true, 'src/facet.mjs~Facet#constructor-example0_3', false)
testDriver.test(() => { return f.vertices instanceof Array }, true, 'src/facet.mjs~Facet#constructor-example0_4', false)
testDriver.test(() => { return f.vertices.length === 0 }, true, 'src/facet.mjs~Facet#constructor-example0_5', false)

const f2 = new Facet(m, d, false)

testDriver.test(() => { return f2 instanceof Facet }, true, 'src/facet.mjs~Facet#constructor-example0_6', false)
testDriver.test(() => { return f2.faceOutside }, false, 'src/facet.mjs~Facet#constructor-example0_7', false)
