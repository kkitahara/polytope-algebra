import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { Facet, ConvexPolytope } from '../../src/index.mjs'
const r = new RealAlgebra()
const l = new LinearAlgebra(r)

const x = l.$(1, 0)
const y = l.$(0, 1)
const facets = [
  new Facet(x, 1, true),
  new Facet(x, -1, false),
  new Facet(y, 1, true),
  new Facet(y, -1, false)]
const vertices = [
  l.$(1, 1),
  l.$(-1, 1),
  l.$(1, -1),
  l.$(-1, -1)]
facets[0].vertices = [vertices[0], vertices[2]]
facets[1].vertices = [vertices[1], vertices[3]]
facets[2].vertices = [vertices[0], vertices[1]]
facets[3].vertices = [vertices[2], vertices[3]]
const edges = ConvexPolytope.genEdges(2, facets)
const cp = new ConvexPolytope(facets, vertices, edges)
testDriver.test(() => { return cp.facets.length }, 4, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_0', false)
testDriver.test(() => { return cp.vertices.length }, 4, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_1', false)
testDriver.test(() => { return cp.edges.length }, 4, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_2', false)

let cp2 = cp.copy(l)
cp2.iaddFacet(new Facet(x, 1, true), l)
testDriver.test(() => { return cp2.facets.length }, 4, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_3', false)
testDriver.test(() => { return cp2.vertices.length }, 4, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_4', false)
testDriver.test(() => { return cp2.edges.length }, 4, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_5', false)

cp2 = cp.copy(l)
cp2.iaddFacet(new Facet(x, 1, false), l)
testDriver.test(() => { return cp2.facets.length }, 0, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_6', false)
testDriver.test(() => { return cp2.vertices.length }, 0, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_7', false)
testDriver.test(() => { return cp2.edges.length }, 0, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_8', false)

cp2 = cp.copy(l)
cp2.iaddFacet(new Facet(x, -1, true), l)
testDriver.test(() => { return cp2.facets.length }, 0, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_9', false)
testDriver.test(() => { return cp2.vertices.length }, 0, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_10', false)
testDriver.test(() => { return cp2.edges.length }, 0, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_11', false)

cp2 = cp.copy(l)
cp2.iaddFacet(new Facet(x, -1, false), l)
testDriver.test(() => { return cp2.facets.length }, 4, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_12', false)
testDriver.test(() => { return cp2.vertices.length }, 4, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_13', false)
testDriver.test(() => { return cp2.edges.length }, 4, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_14', false)

cp2 = cp.copy(l)
cp2.iaddFacet(new Facet(l.$(1, 1), 2, true), l)
testDriver.test(() => { return cp2.facets.length }, 4, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_15', false)
testDriver.test(() => { return cp2.vertices.length }, 4, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_16', false)
testDriver.test(() => { return cp2.edges.length }, 4, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_17', false)

cp2 = cp.copy(l)
cp2.iaddFacet(new Facet(l.$(1, 1), 1, true), l)
testDriver.test(() => { return cp2.facets.length }, 5, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_18', false)
testDriver.test(() => { return cp2.vertices.length }, 5, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_19', false)
testDriver.test(() => { return cp2.edges.length }, 5, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_20', false)

cp2 = cp.copy(l)
cp2.iaddFacet(new Facet(l.$(1, 1), 0, true), l)
testDriver.test(() => { return cp2.facets.length }, 3, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_21', false)
testDriver.test(() => { return cp2.vertices.length }, 3, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_22', false)
testDriver.test(() => { return cp2.edges.length }, 3, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_23', false)

cp2 = cp.copy(l)
cp2.iaddFacet(new Facet(l.$(1, 1), -1, true), l)
testDriver.test(() => { return cp2.facets.length }, 3, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_24', false)
testDriver.test(() => { return cp2.vertices.length }, 3, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_25', false)
testDriver.test(() => { return cp2.edges.length }, 3, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_26', false)

cp2 = cp.copy(l)
cp2.iaddFacet(new Facet(l.$(1, 1), -2, true), l)
testDriver.test(() => { return cp2.facets.length }, 0, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_27', false)
testDriver.test(() => { return cp2.vertices.length }, 0, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_28', false)
testDriver.test(() => { return cp2.edges.length }, 0, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_29', false)

// invalid parameters
cp2 = cp.copy(l)
testDriver.test(() => { return cp2.iaddFacet(new Facet(x, 2, true), null) }, Error, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_30', false)
cp2 = cp.copy(l)
testDriver.test(() => { return cp2.iaddFacet(null, l) }, Error, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_31', false)
cp2 = cp.copy(l)
testDriver.test(() => { return cp2.iaddFacet(new Facet(l.$(1, 1, 1), 1, true), l) }, Error, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example0_32', false)
