import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Matrix as M } from '@kkitahara/linear-algebra'
import { Facet } from '../../src/index.mjs'

const m = new M(1, 2, 3, 4).setDim(1, 4)
const d = 2
// `m` is not a vector
testDriver.test(() => { return new Facet(m, d, true) }, Error, 'src/facet.mjs~Facet#constructor-example1_0', false)
