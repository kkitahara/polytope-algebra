import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { Facet, ConvexPolytope, Polytope } from '../../src/index.mjs'
const r = new RealAlgebra()
const l = new LinearAlgebra(r)

const x = l.$(1, 0)
const y = l.$(0, 1)
let facets = [
  new Facet(x, 1, true),
  new Facet(x, -1, false),
  new Facet(y, 1, true),
  new Facet(y, -1, false)]
let vertices = [
  l.$(1, 1),
  l.$(-1, 1),
  l.$(1, -1),
  l.$(-1, -1)]
facets[0].vertices = [vertices[0], vertices[2]]
facets[1].vertices = [vertices[1], vertices[3]]
facets[2].vertices = [vertices[0], vertices[1]]
facets[3].vertices = [vertices[2], vertices[3]]
let edges = ConvexPolytope.genEdges(2, facets)
const cp = new ConvexPolytope(facets, vertices, edges)

facets = [
  new Facet(x, 3, true),
  new Facet(x, 1, false),
  new Facet(y, 3, true),
  new Facet(y, 1, false)]
vertices = [
  l.$(3, 3),
  l.$(1, 3),
  l.$(3, 1),
  l.$(1, 1)]
facets[0].vertices = [vertices[0], vertices[2]]
facets[1].vertices = [vertices[1], vertices[3]]
facets[2].vertices = [vertices[0], vertices[1]]
facets[3].vertices = [vertices[2], vertices[3]]
edges = ConvexPolytope.genEdges(2, facets)
const cp2 = new ConvexPolytope(facets, vertices, edges)

let p = new Polytope(cp, cp2)
testDriver.test(() => { return p.length }, 2, 'src/polytope.mjs~Polytope#reduce-example0_0', false)

p.reduce(l)
testDriver.test(() => { return p.length }, 2, 'src/polytope.mjs~Polytope#reduce-example0_1', false)

facets = [
  new Facet(x, 3, true),
  new Facet(x, 1, false),
  new Facet(y, 1, true),
  new Facet(y, -1, false)]
vertices = [
  l.$(3, 1),
  l.$(1, 1),
  l.$(3, -1),
  l.$(1, -1)]
facets[0].vertices = [vertices[0], vertices[2]]
facets[1].vertices = [vertices[1], vertices[3]]
facets[2].vertices = [vertices[0], vertices[1]]
facets[3].vertices = [vertices[2], vertices[3]]
edges = ConvexPolytope.genEdges(2, facets)
const cp3 = new ConvexPolytope(facets, vertices, edges)

p = new Polytope(cp, cp3)
testDriver.test(() => { return p.length }, 2, 'src/polytope.mjs~Polytope#reduce-example0_2', false)

p.reduce(l)
testDriver.test(() => { return p.length }, 1, 'src/polytope.mjs~Polytope#reduce-example0_3', false)
