import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Matrix as M } from '@kkitahara/linear-algebra'
import { Facet, ConvexPolytope } from '../../src/index.mjs'

const f1 = new Facet(new M(1, 1), 2, true)
const f2 = new Facet(new M(1, 1, 3), 2, true)

// ANTI-PATTERN
// This is just for a test.
// In practice, facets, vertices and edges must be consistently given.
const facets = [f1, f2]
const a = new ConvexPolytope(facets, [1], [2, 3, 4])

testDriver.test(() => { return a.isNull() }, false, 'src/convex-polytope.mjs~ConvexPolytope#nullify-example0_0', false)
testDriver.test(() => { return a.facets.length }, 2, 'src/convex-polytope.mjs~ConvexPolytope#nullify-example0_1', false)
testDriver.test(() => { return a.vertices.length }, 1, 'src/convex-polytope.mjs~ConvexPolytope#nullify-example0_2', false)
testDriver.test(() => { return a.edges.length }, 3, 'src/convex-polytope.mjs~ConvexPolytope#nullify-example0_3', false)

a.nullify()
testDriver.test(() => { return a.isNull() }, true, 'src/convex-polytope.mjs~ConvexPolytope#nullify-example0_4', false)
testDriver.test(() => { return a.facets.length }, 0, 'src/convex-polytope.mjs~ConvexPolytope#nullify-example0_5', false)
testDriver.test(() => { return a.vertices.length }, 0, 'src/convex-polytope.mjs~ConvexPolytope#nullify-example0_6', false)
testDriver.test(() => { return a.edges.length }, 0, 'src/convex-polytope.mjs~ConvexPolytope#nullify-example0_7', false)
