import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { Facet, ConvexPolytope, Polytope } from '../../src/index.mjs'
const r = new RealAlgebra()
const l = new LinearAlgebra(r)

const x = l.$(1, 0)
const y = l.$(0, 1)
const facets = [
  new Facet(x, 1, true),
  new Facet(x, -1, false),
  new Facet(y, 1, true),
  new Facet(y, -1, false)]
const vertices = [
  l.$(1, 1),
  l.$(-1, 1),
  l.$(1, -1),
  l.$(-1, -1)]
facets[0].vertices = [vertices[0], vertices[2]]
facets[1].vertices = [vertices[1], vertices[3]]
facets[2].vertices = [vertices[0], vertices[1]]
facets[3].vertices = [vertices[2], vertices[3]]
const edges = ConvexPolytope.genEdges(2, facets)
const cp = new ConvexPolytope(facets, vertices, edges)
const cp2 = cp.copy(l)

let p = new Polytope(cp)
testDriver.test(() => { return p instanceof Polytope }, true, 'src/polytope.mjs~Polytope#constructor-example0_0', false)
testDriver.test(() => { return p.length }, 1, 'src/polytope.mjs~Polytope#constructor-example0_1', false)
testDriver.test(() => { return p[0] === cp }, true, 'src/polytope.mjs~Polytope#constructor-example0_2', false)

p = new Polytope(cp, cp2)
testDriver.test(() => { return p.length }, 2, 'src/polytope.mjs~Polytope#constructor-example0_3', false)
testDriver.test(() => { return p[0] === cp }, true, 'src/polytope.mjs~Polytope#constructor-example0_4', false)
testDriver.test(() => { return p[1] === cp2 }, true, 'src/polytope.mjs~Polytope#constructor-example0_5', false)

p = new Polytope()
testDriver.test(() => { return p.length }, 0, 'src/polytope.mjs~Polytope#constructor-example0_6', false)

// invalid argument
testDriver.test(() => { return new Polytope(null) }, Error, 'src/polytope.mjs~Polytope#constructor-example0_7', false)

// inconsistent dimension
cp2.vertices[0] = l.$(1)
testDriver.test(() => { return new Polytope(cp, cp2) }, Error, 'src/polytope.mjs~Polytope#constructor-example0_8', false)

// the dimension of null convex polytope is undefined,
// and the dimension check fails
cp2.nullify()
p = new Polytope(cp2)
testDriver.test(() => { return p.length }, 0, 'src/polytope.mjs~Polytope#constructor-example0_9', false)
