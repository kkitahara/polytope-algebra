import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { Polytope, PolytopeAlgebra } from '../../src/index.mjs'
const r = new RealAlgebra()
const l = new LinearAlgebra(r)
const p2d = new PolytopeAlgebra(2, l)

const p = p2d.hypercube(1)
testDriver.test(() => { return p.length }, 1, 'src/polytope-algebra.mjs~PolytopeAlgebra#copy-example0_0', false)
testDriver.test(() => { return p[0].facets.length }, 4, 'src/polytope-algebra.mjs~PolytopeAlgebra#copy-example0_1', false)
testDriver.test(() => { return p[0].vertices.length }, 4, 'src/polytope-algebra.mjs~PolytopeAlgebra#copy-example0_2', false)
testDriver.test(() => { return p[0].edges.length }, 4, 'src/polytope-algebra.mjs~PolytopeAlgebra#copy-example0_3', false)

const p2 = p2d.copy(p)
testDriver.test(() => { return p2 instanceof Polytope }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#copy-example0_4', false)
testDriver.test(() => { return p2 !== p }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#copy-example0_5', false)
testDriver.test(() => { return p2.length }, 1, 'src/polytope-algebra.mjs~PolytopeAlgebra#copy-example0_6', false)
testDriver.test(() => { return p2[0].facets.length }, 4, 'src/polytope-algebra.mjs~PolytopeAlgebra#copy-example0_7', false)
testDriver.test(() => { return p2[0].vertices.length }, 4, 'src/polytope-algebra.mjs~PolytopeAlgebra#copy-example0_8', false)
testDriver.test(() => { return p2[0].edges.length }, 4, 'src/polytope-algebra.mjs~PolytopeAlgebra#copy-example0_9', false)

const p3 = p2d.copy(new Polytope())
testDriver.test(() => { return p3 instanceof Polytope }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#copy-example0_10', false)
testDriver.test(() => { return p3.isNull() }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#copy-example0_11', false)

testDriver.test(() => { return p2d.copy(null) }, Error, 'src/polytope-algebra.mjs~PolytopeAlgebra#copy-example0_12', false)
p[0].facets[0].nvec.push(1)
