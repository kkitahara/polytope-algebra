import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { PolytopeAlgebra } from '../../src/index.mjs'
const r = new RealAlgebra()
const l = new LinearAlgebra(r)
const p4d = new PolytopeAlgebra(4, l)

let p = p4d.hypercube()
testDriver.test(() => { return p.length }, 1, 'src/polytope-algebra.mjs~PolytopeAlgebra#hypercube-example0_0', false)
testDriver.test(() => { return p[0].facets.length }, 8, 'src/polytope-algebra.mjs~PolytopeAlgebra#hypercube-example0_1', false)
testDriver.test(() => { return p[0].vertices.length }, 16, 'src/polytope-algebra.mjs~PolytopeAlgebra#hypercube-example0_2', false)
testDriver.test(() => { return p[0].edges.length }, 32, 'src/polytope-algebra.mjs~PolytopeAlgebra#hypercube-example0_3', false)

p = p4d.hypercube(r.$(1, 2))
testDriver.test(() => { return r.eq(p4d.volume(p), 1) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#hypercube-example0_4', false)

p = p4d.hypercube(1)
testDriver.test(() => { return r.eq(p4d.volume(p), 16) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#hypercube-example0_5', false)

p = p4d.hypercube(r.$(0, 2))
testDriver.test(() => { return r.eq(p4d.volume(p), 0) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#hypercube-example0_6', false)

p = p4d.hypercube(r.$(-1, 2))
testDriver.test(() => { return r.eq(p4d.volume(p), 0) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#hypercube-example0_7', false)
