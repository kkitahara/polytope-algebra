import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { Edge } from '../../src/index.mjs'
const r = new RealAlgebra()
const l = new LinearAlgebra(r)

const m1 = l.$(1, 1, 1)
const m2 = l.$(2, 1, 1)
const m3 = l.$(3, 1, 1)
const m4 = l.$(4, 1, 1)
const e = new Edge(m1, m2)
const vMap = new WeakMap()
vMap.set(m1, m3)
vMap.set(m2, m4)
e.replaceVertices(vMap)
testDriver.test(() => { return e.v0 === m3 }, true, 'src/edge.mjs~Edge#replaceVertices-example0_0', false)
testDriver.test(() => { return e.v1 === m4 }, true, 'src/edge.mjs~Edge#replaceVertices-example0_1', false)

testDriver.test(() => { return e.replaceVertices(vMap) }, Error, 'src/edge.mjs~Edge#replaceVertices-example0_2', false)
