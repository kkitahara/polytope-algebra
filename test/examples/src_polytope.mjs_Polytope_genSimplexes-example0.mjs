import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { Facet, ConvexPolytope, Polytope } from '../../src/index.mjs'
const r = new RealAlgebra()
const l = new LinearAlgebra(r)

const x = l.$(1, 0)
const y = l.$(0, 1)
const facets = [
  new Facet(x, 1, true),
  new Facet(x, -1, false),
  new Facet(y, 1, true),
  new Facet(y, -1, false)]
const vertices = [
  l.$(1, 1),
  l.$(-1, 1),
  l.$(1, -1),
  l.$(-1, -1)]
facets[0].vertices = [vertices[0], vertices[2]]
facets[1].vertices = [vertices[1], vertices[3]]
facets[2].vertices = [vertices[0], vertices[1]]
facets[3].vertices = [vertices[2], vertices[3]]
const edges = ConvexPolytope.genEdges(2, facets)
const cp = new ConvexPolytope(facets, vertices, edges)
const cp2 = cp.copy(l)
// ANTI-PATTERN!
// this is just for a test
let p = new Polytope(cp, cp2)
let simplexes = p.genSimplexes()
testDriver.test(() => { return simplexes instanceof Array }, true, 'src/polytope.mjs~Polytope#genSimplexes-example0_0', false)
testDriver.test(() => { return simplexes.length }, 4, 'src/polytope.mjs~Polytope#genSimplexes-example0_1', false)
testDriver.test(() => { return simplexes[0].weight }, true, 'src/polytope.mjs~Polytope#genSimplexes-example0_2', false)

p = new Polytope()
simplexes = p.genSimplexes()
testDriver.test(() => { return simplexes instanceof Array }, true, 'src/polytope.mjs~Polytope#genSimplexes-example0_3', false)
testDriver.test(() => { return simplexes.length }, 0, 'src/polytope.mjs~Polytope#genSimplexes-example0_4', false)
