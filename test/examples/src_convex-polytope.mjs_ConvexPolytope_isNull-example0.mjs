import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Matrix as M } from '@kkitahara/linear-algebra'
import { ConvexPolytope } from '../../src/index.mjs'

// ANTI-PATTERN
// This is just for a test.
// In practice, facets, vertices and edges must be consistently given.
const a = new ConvexPolytope([], [new M(1)])

testDriver.test(() => { return a.isNull() }, false, 'src/convex-polytope.mjs~ConvexPolytope#isNull-example0_0', false)
testDriver.test(() => { return new ConvexPolytope().isNull() }, true, 'src/convex-polytope.mjs~ConvexPolytope#isNull-example0_1', false)
