import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Matrix as M } from '@kkitahara/linear-algebra'
import { Facet } from '../../src/index.mjs'

const m = new M(1, 2, 3, 4)
const d = 2
const f = new Facet(m, d, true)

testDriver.test(() => { return f.dim }, 4, 'src/facet.mjs~Facet#dim-example0_0', false)
