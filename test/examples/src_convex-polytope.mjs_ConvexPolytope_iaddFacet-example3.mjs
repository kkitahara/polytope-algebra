import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { PolytopeAlgebra } from '../../src/index.mjs'
const r = new RealAlgebra(1e-5)
const l = new LinearAlgebra(r)
const p1d = new PolytopeAlgebra(1, l)

const p = p1d.hypercube(1)

const cp = p[0].iaddFacet(p1d.facet([1], 0), l)
testDriver.test(() => { return cp.facets.length }, 2, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example3_0', false)
