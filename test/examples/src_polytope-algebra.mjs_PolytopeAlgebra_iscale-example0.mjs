import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { PolytopeAlgebra } from '../../src/index.mjs'
const r = new RealAlgebra()
const l = new LinearAlgebra(r)
const p2d = new PolytopeAlgebra(2, l)

const p = p2d.hypercube(1)
const p2 = p2d.iscale(p, 2)
testDriver.test(() => { return p === p2 }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#iscale-example0_0', false)
testDriver.test(() => { return p.length }, 1, 'src/polytope-algebra.mjs~PolytopeAlgebra#iscale-example0_1', false)
testDriver.test(() => { return r.eq(p[0].facets[0].d, 2) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#iscale-example0_2', false)
testDriver.test(() => { return r.eq(p[0].facets[1].d, -2) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#iscale-example0_3', false)
testDriver.test(() => { return r.eq(p[0].facets[2].d, 2) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#iscale-example0_4', false)
testDriver.test(() => { return r.eq(p[0].facets[3].d, -2) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#iscale-example0_5', false)
testDriver.test(() => { return l.eq(p[0].vertices[0], [2, 2]) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#iscale-example0_6', false)
testDriver.test(() => { return l.eq(p[0].vertices[1], [-2, 2]) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#iscale-example0_7', false)
testDriver.test(() => { return l.eq(p[0].vertices[2], [2, -2]) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#iscale-example0_8', false)
testDriver.test(() => { return l.eq(p[0].vertices[3], [-2, -2]) }, true, 'src/polytope-algebra.mjs~PolytopeAlgebra#iscale-example0_9', false)

testDriver.test(() => { return p2d.iscale(null, 2) }, Error, 'src/polytope-algebra.mjs~PolytopeAlgebra#iscale-example0_10', false)
