import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { PolytopeAlgebra } from '../../src/index.mjs'
const r = new RealAlgebra(1e-5)
const l = new LinearAlgebra(r)
const p3d = new PolytopeAlgebra(3, l)

const p = p3d.hypercube(1)

const cp = p[0].iaddFacet(p3d.facet([1, 1, 0], 0), l)
testDriver.test(() => { return cp.facets.length }, 5, 'src/convex-polytope.mjs~ConvexPolytope#iaddFacet-example2_0', false)
