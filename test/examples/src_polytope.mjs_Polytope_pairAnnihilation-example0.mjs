import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { PolytopeAlgebra } from '../../src/index.mjs'
const r = new RealAlgebra(1e-5)
const l = new LinearAlgebra(r)
const p2d = new PolytopeAlgebra(2, l)

let p = p2d.hypercube(1)
const a = Math.sqrt(2) / 2
const p2 = p2d.translate(p2d.rotate(p, l.$(a, a, a, -a).setDim(2)), [3, 3])
p = p2d.add(p, p2)
p[1].weight = false
p.pairAnnihilation(l)
testDriver.test(() => { return p.length }, 2, 'src/polytope.mjs~Polytope#pairAnnihilation-example0_0', false)

p[1].nullify()
p.pairAnnihilation(l)
testDriver.test(() => { return p.length }, 1, 'src/polytope.mjs~Polytope#pairAnnihilation-example0_1', false)

p = p2d.hypercube(1)
p = p2d.add(p, p2)
p[0].nullify()
p.pairAnnihilation(l)
testDriver.test(() => { return p.length }, 1, 'src/polytope.mjs~Polytope#pairAnnihilation-example0_2', false)

testDriver.test(() => { return p.pairAnnihilation(null) }, Error, 'src/polytope.mjs~Polytope#pairAnnihilation-example0_3', false)
