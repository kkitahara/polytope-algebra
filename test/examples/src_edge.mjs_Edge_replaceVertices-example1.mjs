import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '@kkitahara/linear-algebra'
import { Edge } from '../../src/index.mjs'
const r = new RealAlgebra()
const l = new LinearAlgebra(r)

const m1 = l.$(1, 1, 1)
const m2 = l.$(2, 1, 1)
const m3 = l.$(3, 1, 1)
const e = new Edge(m1, m2)
const vMap = new WeakMap()
testDriver.test(() => { return e.replaceVertices(vMap) }, Error, 'src/edge.mjs~Edge#replaceVertices-example1_0', false)
vMap.set(m1, m3)
testDriver.test(() => { return e.replaceVertices(vMap) }, Error, 'src/edge.mjs~Edge#replaceVertices-example1_1', false)
testDriver.test(() => { return e.replaceVertices(null) }, Error, 'src/edge.mjs~Edge#replaceVertices-example1_2', false)
