import resolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'
import { terser } from 'rollup-plugin-terser'

export default {
  input: 'src/index.mjs',
  output: {
    file: 'polytope-algebra.min.mjs',
    format: 'esm'
  },
  plugins: [
    resolve(),
    commonjs(),
    terser({ output: { comments: /^\**!|@preserve|@license|@cc_on/i } })
  ]
}
