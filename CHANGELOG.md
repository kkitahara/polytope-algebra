v1.2.8
- Remove array spread syntax from Polytope.genSimplexes.

v1.2.7
- Fix bugs in ConvexPolytope.genEdges, ConvexPolytope.genSimplexes.

v1.2.6
- Fix the problems found in v1.2.5.

v1.2.5
- Unresolved problems found for dimensions higher than 3. Use of this code for such higher dimensions is experimental.
- Update README.
- Add warning message to PolytopeAlgebra#constructor.

v1.2.4
- Update dependencies

v1.2.3
- Update dependencies

v1.2.2
- Fix README

v1.2.1
- Fix a bug in Polytope#reduce (#2)

v1.2.0
- Add support for 0-dim polytopes

v1.1.8
- Change @kkitahara/linear-algebra from dependency to peerDependency

v1.1.7
- Add CHANGELOG.md
